<!--
SPDX-FileCopyrightText: 2022-2023 Gerry Agbobada <git@gagbo.net>

SPDX-License-Identifier: CC0-1.0
-->

# R7.rs

R7 plans to be a Rust implementation of r7rs-small
