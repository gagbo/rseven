// SPDX-FileCopyrightText: 2023 Gerry Agbobada <git@gagbo.net>
//
// SPDX-License-Identifier: LGPL-3.0-only

use gc::{Finalize, Gc, GcCell, Trace};
use std::{io::Read, io::Write};

#[derive(Trace, Finalize, Debug, Clone)]
pub enum Port {
    BinaryInput(Input),
    BinaryOutput(Output),
    TextualInput(Input),
    TextualOutput(Output),
}

/// A trait for types valid as runtime "input ports" for Scheme.
pub trait InputPort: Read + Trace + Finalize + std::fmt::Debug {}

#[derive(Trace, Finalize, Debug, Clone)]
pub struct Input {
    port: Gc<GcCell<dyn InputPort>>,
}

/// A trait for types valid as runtime "output ports" for Scheme.
pub trait OutputPort: Write + Trace + Finalize + std::fmt::Debug {}

#[derive(Trace, Finalize, Debug, Clone)]
pub struct Output {
    port: Gc<GcCell<dyn OutputPort>>,
}
