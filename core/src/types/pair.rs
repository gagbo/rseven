// SPDX-FileCopyrightText: 2023 Gerry Agbobada <git@gagbo.net>
//
// SPDX-License-Identifier: LGPL-3.0-only

use gc::{Finalize, Gc, GcCell, Trace};

use super::{HeapValue, Value};

#[derive(Trace, Finalize, Debug, Clone)]
pub struct Pair {
    pub(crate) car: HeapValue,
    pub(crate) cdr: HeapValue,
}

impl Pair {
    // Walk to the cdr of the Pair
    pub fn advance_pair(&self) -> Option<Self> {
        match &*self.cdr.borrow() {
            Value::Pair(p) => Some(Self {
                car: p.car.clone(),
                cdr: p.cdr.clone(),
            }),
            Value::Nil => None,
            _ => Some(Self {
                car: self.cdr.clone(),
                cdr: Gc::new(GcCell::new(Value::Nil)),
            }),
        }
    }
}

impl FromIterator<HeapValue> for Value {
    fn from_iter<T: IntoIterator<Item = HeapValue>>(iter: T) -> Self {
        let mut iter = iter.into_iter();
        let first = iter.next();
        if first.is_none() {
            return Value::Nil;
        }

        Value::Pair(Pair {
            car: first.unwrap(),
            cdr: Gc::new(GcCell::new(iter.collect())),
        })
    }
}

impl FromIterator<Value> for Value {
    fn from_iter<T: IntoIterator<Item = Value>>(iter: T) -> Self {
        iter.into_iter().map(|v| Gc::new(GcCell::new(v))).collect()
    }
}

impl IntoIterator for Pair {
    type Item = HeapValue;

    type IntoIter = PairIterator;

    fn into_iter(self) -> Self::IntoIter {
        Self::IntoIter {
            done: false,
            current_pair: self,
        }
    }
}

#[derive(Trace, Finalize, Debug, Clone)]
pub struct PairIterator {
    /// True if we finished the iterator and the "current_pair" is irrelevant
    /// (i.e. stuck on the last one.)
    done: bool,
    current_pair: Pair,
}

impl Iterator for PairIterator {
    type Item = HeapValue;

    fn next(&mut self) -> Option<Self::Item> {
        if self.done {
            return None;
        }

        let val = self.current_pair.car.clone();
        match self.current_pair.advance_pair() {
            Some(p) => self.current_pair = p,
            None => self.done = true,
        }

        Some(val)
    }
}
