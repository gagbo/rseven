// SPDX-FileCopyrightText: 2023 Gerry Agbobada <git@gagbo.net>
//
// SPDX-License-Identifier: LGPL-3.0-only

use gc::{Finalize, Gc, GcCell, Trace};

use crate::{Chunk, Heap, Result, Value};

#[derive(Clone)]
pub struct NativeProc {
    pub(crate) arity: ProcArity,
    pub(crate) name: String,
    pub(crate) function: fn(Vec<Value>) -> Result<Vec<Value>>,
}

impl std::fmt::Debug for NativeProc {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        f.debug_struct("NativeProc")
            .field("arity", &self.arity)
            .field("name", &self.name)
            .field("function", &"pointer".to_string())
            .finish()
    }
}

impl NativeProc {
    pub fn new(
        name: String,
        arity: ProcArity,
        function: fn(Vec<Value>) -> Result<Vec<Value>>,
    ) -> Self {
        Self {
            arity,
            name,
            function,
        }
    }
}

#[derive(Trace, Finalize, Debug, Clone)]
pub struct BytecodeProc {
    // SAFETY: ProcArity is only a tagged union for a single u16
    #[unsafe_ignore_trace]
    pub(crate) arity: ProcArity,
    pub(crate) chunk: Chunk,
    pub(crate) name: String,
}

impl Default for BytecodeProc {
    fn default() -> Self {
        Self {
            arity: Default::default(),
            chunk: Default::default(),
            name: Default::default(),
        }
    }
}

#[derive(Debug, Clone, Copy)]
pub enum ProcArity {
    /// (var1 var2 var3 ...)
    Fixed(u16),
    /// var
    ListVar,
    /// (var1 var2 . rest)
    ///
    /// The variant contains the minimum number of arguments,
    /// that is, the number of variables before the '.'
    FixedAndRest(u16),
}

impl Default for ProcArity {
    fn default() -> Self {
        Self::Fixed(0)
    }
}

impl BytecodeProc {
    pub fn new(name: String, arity: ProcArity) -> Self {
        Self {
            arity,
            chunk: Chunk::new(),
            name,
        }
    }
}

impl std::fmt::Display for BytecodeProc {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        if self.name.is_empty() {
            write!(f, "#<compiled procedure <unnamed lambda>>",)
        } else {
            write!(f, "#<compiled procedure {}>", self.name)
        }
    }
}

#[derive(Trace, Finalize, Debug, Clone)]
pub struct Closure {
    pub(crate) function: Heap<BytecodeProc>,
    pub(crate) upvalues: Vec<Heap<Value>>,
    pub(crate) upvalue_count: usize,
}

impl Closure {
    pub fn new(function: Heap<BytecodeProc>) -> Self {
        Self {
            function,
            upvalues: Vec::new(),
            upvalue_count: 0,
        }
    }
}

impl Default for Closure {
    fn default() -> Self {
        Self {
            function: Default::default(),
            upvalues: Vec::new(),
            upvalue_count: Default::default(),
        }
    }
}

impl From<Closure> for Value {
    fn from(value: Closure) -> Self {
        Self::Closure(Gc::new(GcCell::new(value)))
    }
}

impl std::fmt::Display for Closure {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        if self.function.borrow().name.is_empty() {
            write!(f, "#<closure <unnamed lambda>>",)
        } else {
            write!(f, "#<closure {}>", self.function.borrow().name)
        }
    }
}

/// A CallFrame for the call frame storage (useful for continuations as well)
///
/// TODO: Add a proper constructor that takes only the necessary initial state
///
/// TODO: Add a setter for the vm_stack_start_index
///
/// TODO: Add a field of Option<Vec<Value>> to save the current
///   state of the stack in the CallFrame (useful for restoring continuations)
///
/// TODO: Add an accessor for the vm_stack_start_index (useful for the OP_BECOME OpCode that
///   will clean up the Stack from current CallFrame values basically)
#[derive(Trace, Finalize, Debug, Clone)]
pub struct CallFrame {
    // NOTE: we need to have a Gc<Procedure> instead of
    // having the extra Value enum indirection, but it doesn't matter for now,
    // as only the VM will create CallFrames and it can assert things.
    /// The function to evaluate in this frame.
    ///
    /// The HeapValue is guaranteed to point to a Procedure
    pub(crate) closure: Heap<Closure>,
    /// The IP inside the current function.
    pub(crate) instruction_pointer: usize,
    /// The index in the VM stack where the locals started.
    pub(crate) vm_stack_start_index: usize,
    pub(crate) parent_frame: Option<Heap<CallFrame>>,
}

impl CallFrame {
    pub(crate) fn advance_ip(&mut self, offset: usize) {
        self.instruction_pointer += offset
    }
    pub(crate) fn backwards_ip(&mut self, offset: usize) {
        self.instruction_pointer -= offset
    }
    pub(crate) fn read_constant(&self, index: usize) -> Value {
        self.closure.borrow().function.borrow().chunk.constants[index].clone()
    }
}
