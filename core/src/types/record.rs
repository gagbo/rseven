// SPDX-FileCopyrightText: 2023 Gerry Agbobada <git@gagbo.net>
//
// SPDX-License-Identifier: LGPL-3.0-only

use gc::{Finalize, Trace};

#[derive(Trace, Finalize, Debug, Clone)]
pub struct Record;
