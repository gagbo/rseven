// SPDX-FileCopyrightText: 2022-2023 Gerry Agbobada <git@gagbo.net>
//
// SPDX-License-Identifier: LGPL-3.0-only

//! Error types

use parser::ast::AstNode;

#[derive(Debug, Clone, thiserror::Error)]
pub enum Error {
    #[error("void variable: {0}")]
    VoidVariable(String),
    #[error("uninitialized closure slot")]
    UninitClosureSlot,
    #[error("Continuation wrong argument count: continuation expected {expected} arguments, got {got} arguments")]
    ContinuationArgCount { expected: usize, got: usize },
    #[error("compilation error: {0}")]
    Compilation(String),
    #[error("compilation for the node type is not implemented: {0}")]
    UnsupportedCompilation(String),
    #[error("generic error: {0}")]
    Unknown(String),
    #[error("empty chunk")]
    EmptyChunk,
    #[error("popping from an empty stack")]
    EmptyStack,
    #[error("lexical error: {0}")]
    LexicalError(#[from] parser::LexicalError),
    #[error("VM has no current CallFrame.")]
    NoCurrentFrame,
    #[error("Wrong type: expected {0}, got {1}")]
    WrongType(String, String),
}

pub type Result<T> = std::result::Result<T, Error>;
