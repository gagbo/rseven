// SPDX-FileCopyrightText: 2023 Gerry Agbobada <git@gagbo.net>
//
// SPDX-License-Identifier: LGPL-3.0-only

use super::{
    expression::compile_expression, literal::compile_literal, patch_jump, push_code,
    sequence::compile_sequence, Compiler,
};
use crate::{OpCode, Result};
use parser::ast::{CaseClause, CaseWithRecipientFallback, CaseWithSequenceFallback};
use std::{cell::RefCell, rc::Rc};

pub(super) fn compile_case_sequence_fallback(
    current_compiler: Rc<RefCell<Compiler>>,
    case_expr: CaseWithSequenceFallback,
) -> Result<()> {
    let CaseWithSequenceFallback {
        tested_value,
        clauses,
        fallback,
    } = case_expr;
    compile_expression(current_compiler.clone(), *tested_value, false)?;
    let mut end_jumps = Vec::new();
    let mut next_clause_jump;
    // TODO: Factor the CaseClause compilation code with the section
    // from RecipientFallback compilation code
    for clause in clauses {
        match clause {
            CaseClause::Sequence { values, consequent } => {
                let mut clause_jumps = Vec::new();
                for value in values {
                    compile_literal(current_compiler.clone(), value)?;
                    push_code(current_compiler.clone(), OpCode::Eq);
                    clause_jumps.push(push_code(current_compiler.clone(), OpCode::JumpIfTrue(0)));
                    // Consume:
                    // - The #false, and
                    // - The 2nd OpEq operand
                    push_code(current_compiler.clone(), OpCode::Popn(2));
                }

                // NOTE: We are supposed to jump past this inconditional jump
                // if any case clause has matched
                next_clause_jump = push_code(current_compiler.clone(), OpCode::Jump(0));
                for jump in clause_jumps {
                    patch_jump(current_compiler.clone(), jump)?;
                }
                // Consume:
                // - The #true
                // - the 2nd OpEq operand
                // - the 1st OpEq operand
                push_code(current_compiler.clone(), OpCode::Popn(3));
                compile_sequence(current_compiler.clone(), consequent)?;
            }
            CaseClause::Recipient { values, recipient } => {
                let mut clause_jumps = Vec::new();
                for value in values {
                    compile_literal(current_compiler.clone(), value)?;
                    push_code(current_compiler.clone(), OpCode::Eq);
                    clause_jumps.push(push_code(current_compiler.clone(), OpCode::JumpIfTrue(0)));
                    // Consume:
                    // - The #false, and
                    // - The 2nd OpEq operand
                    push_code(current_compiler.clone(), OpCode::Popn(2));
                }

                // NOTE: We are supposed to jump past this inconditional jump
                // if any case clause has matched
                next_clause_jump = push_code(current_compiler.clone(), OpCode::Jump(0));
                for jump in clause_jumps {
                    patch_jump(current_compiler.clone(), jump)?;
                }
                // Consume:
                // - The #true
                // - the 2nd OpEq operand
                // We leave the 1st operand value on the top of the stack, as it's called by recipient
                push_code(current_compiler.clone(), OpCode::Popn(2));
                compile_expression(current_compiler.clone(), recipient, false)?;
                push_code(current_compiler.clone(), OpCode::Become(1));
            }
        }
        end_jumps.push(push_code(current_compiler.clone(), OpCode::Jump(0)));
        patch_jump(current_compiler.clone(), next_clause_jump)?;
    }

    // Consume:
    // - the 1st OpEq operand
    // NOTE: All other values (#false and the 2nd OpEq operand) have been consumed on the stack by
    // the false case handling in compilation above
    push_code(current_compiler.clone(), OpCode::Pop);
    if let Some(fallback) = fallback {
        compile_sequence(current_compiler.clone(), fallback)?;
    } else {
        // Without fallback, behaviour is unspecified. We choose to make cond return #f
        push_code(current_compiler.clone(), OpCode::False);
    }

    for jump in end_jumps {
        patch_jump(current_compiler.clone(), jump)?;
    }
    Ok(())
}

pub(super) fn compile_case_recipient_fallback(
    current_compiler: Rc<RefCell<Compiler>>,
    case_expr: CaseWithRecipientFallback,
) -> Result<()> {
    let CaseWithRecipientFallback {
        tested_value,
        clauses,
        fallback,
    } = case_expr;
    compile_expression(current_compiler.clone(), *tested_value, false)?;
    let mut end_jumps = Vec::new();
    let mut next_clause_jump;
    // TODO: Factor the CaseClause compilation code with the section
    // from SequenceFallback compilation code
    for clause in clauses {
        match clause {
            CaseClause::Sequence { values, consequent } => {
                let mut clause_jumps = Vec::new();
                for value in values {
                    compile_literal(current_compiler.clone(), value)?;
                    push_code(current_compiler.clone(), OpCode::Eq);
                    clause_jumps.push(push_code(current_compiler.clone(), OpCode::JumpIfTrue(0)));
                    // Consume:
                    // - The #false, and
                    // - The 2nd OpEq operand
                    push_code(current_compiler.clone(), OpCode::Popn(2));
                }

                // NOTE: We are supposed to jump past this inconditional jump
                // if any case clause has matched
                next_clause_jump = push_code(current_compiler.clone(), OpCode::Jump(0));
                for jump in clause_jumps {
                    patch_jump(current_compiler.clone(), jump)?;
                }
                // Consume:
                // - The #true
                // - the 2nd OpEq operand
                // - the 1st OpEq operand
                push_code(current_compiler.clone(), OpCode::Popn(3));
                compile_sequence(current_compiler.clone(), consequent)?;
            }
            CaseClause::Recipient { values, recipient } => {
                let mut clause_jumps = Vec::new();
                for value in values {
                    compile_literal(current_compiler.clone(), value)?;
                    push_code(current_compiler.clone(), OpCode::Eq);
                    clause_jumps.push(push_code(current_compiler.clone(), OpCode::JumpIfTrue(0)));
                    // Consume:
                    // - The #false, and
                    // - The 2nd OpEq operand
                    push_code(current_compiler.clone(), OpCode::Popn(2));
                }

                // NOTE: We are supposed to jump past this inconditional jump
                // if any case clause has matched
                next_clause_jump = push_code(current_compiler.clone(), OpCode::Jump(0));
                for jump in clause_jumps {
                    patch_jump(current_compiler.clone(), jump)?;
                }
                // Consume:
                // - The #true
                // - the 2nd OpEq operand
                // We leave the 1st operand value on the top of the stack, as it's called by recipient
                push_code(current_compiler.clone(), OpCode::Popn(2));
                compile_expression(current_compiler.clone(), recipient, false)?;
                push_code(current_compiler.clone(), OpCode::Become(1));
            }
        }
        end_jumps.push(push_code(current_compiler.clone(), OpCode::Jump(0)));
        patch_jump(current_compiler.clone(), next_clause_jump)?;
    }

    // NOTE: We don't need to do anything, in all "falsey" cases we left the test value on the top
    // of the stack
    compile_expression(current_compiler.clone(), *fallback, false)?;
    push_code(current_compiler.clone(), OpCode::Become(1));

    for jump in end_jumps {
        patch_jump(current_compiler.clone(), jump)?;
    }
    Ok(())
}
