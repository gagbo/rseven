// SPDX-FileCopyrightText: 2023 Gerry Agbobada <git@gagbo.net>
//
// SPDX-License-Identifier: LGPL-3.0-only

use super::{expression::compile_expression, push_code, Compiler};
use crate::{OpCode, Result};
use parser::ast::ProcedureCall;
use std::{cell::RefCell, rc::Rc};

pub(super) fn compile_procedure_call(
    current_compiler: Rc<RefCell<Compiler>>,
    proc_call: ProcedureCall,
    in_tail_position: bool,
) -> Result<()> {
    let ProcedureCall {
        operator,
        mut operands,
    } = proc_call;

    // The calling convention is almost the one from Crafting Interpreters, that comes from Lua.
    //
    // We put all the arguments in order first, then the function to call, then we call.
    //
    // BOTTOM
    // ARG1
    // ARG2
    // ARG3
    // FUNCTION
    // OP_CALL(3)
    // TOP
    //
    //
    // This allows the OP_CALL to freely eat the top of the stack after it got its copy. And as
    // Scheme doesn't have implicit arguments, we can safely remove the need for the extra slot.
    //
    // This also means that all function arguments are eagerly evaluated, from left to right
    let arg_count = operands.len();
    while let Some(expr) = operands.pop_front() {
        compile_expression(current_compiler.clone(), expr, false)?;
    }
    compile_expression(current_compiler.clone(), *operator, false)?;
    push_code(
        current_compiler,
        if in_tail_position {
            OpCode::Become(arg_count)
        } else {
            OpCode::Call(arg_count)
        },
    );
    Ok(())
}
