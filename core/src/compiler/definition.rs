// SPDX-FileCopyrightText: 2023 Gerry Agbobada <git@gagbo.net>
//
// SPDX-License-Identifier: LGPL-3.0-only

use super::{
    add_local, expression::compile_expression, in_global_scope, lambda::compile_lambda, push_code,
    push_constant, Compiler,
};
use crate::{Error, OpCode, Result, Value};
use parser::ast::{
    Definition, Formals, LambdaDefinition, LambdaExpr, RecordDefinition, SingleDefinition,
    SyntaxDefinition, ValuesDefinition,
};
use std::{cell::RefCell, rc::Rc};

pub(crate) fn compile_definition(
    current_compiler: Rc<RefCell<Compiler>>,
    def: Definition,
) -> Result<()> {
    match def {
        Definition::Single(SingleDefinition { identifier, value }) => {
            compile_expression(current_compiler.clone(), value, false)?;

            if in_global_scope(current_compiler.clone()) {
                let constant = push_constant(current_compiler.clone(), Value::String(identifier));
                push_code(current_compiler, OpCode::DefineGlobal(constant));
            } else {
                // In scheme, we allow redefining variables in the same
                // scope.

                // NOTE: maybe produce a warning if a variable with the same
                // name is in the same scope??

                // NOTE: we should not need the whole "declaring vs.
                // defining" variables here since we declare the local
                // variable in scope as the last operation. (This is the
                // advantage of compiling from an AST instead of doing a
                // single-pass compiler)
                //
                // This might change with the LetRec family of expressions
                // though. In that case, Local.depth will need to be an
                // Option<usize>, and an extra "initialize" method will be
                // necessary for locals
                add_local(current_compiler, identifier, true)?;
            }
            Ok(())
        }
        Definition::Lambda(LambdaDefinition {
            identifier,
            formals,
            body,
        }) => {
            let formals: Formals = if formals.last_dotted_ident.is_some() {
                Formals::Dotted {
                    head: formals.idents,
                    last: formals.last_dotted_ident.unwrap(),
                }
            } else {
                Formals::List(formals.idents)
            };

            compile_lambda(
                current_compiler.clone(),
                LambdaExpr { formals, body },
                Some(identifier.clone()),
            )?;

            if in_global_scope(current_compiler.clone()) {
                let constant = push_constant(current_compiler.clone(), Value::String(identifier));
                push_code(current_compiler, OpCode::DefineGlobal(constant));
            } else {
                // In scheme, we allow redefining variables in the same
                // scope.

                // NOTE: maybe produce a warning if a variable with the same
                // name is in the same scope??

                // NOTE: we should not need the whole "declaring vs.
                // defining" variables here since we declare the local
                // variable in scope as the last operation. (This is the
                // advantage of compiling from an AST instead of doing a
                // single-pass compiler)
                //
                // This might change with the LetRec family of expressions
                // though. In that case, Local.depth will need to be an
                // Option<usize>, and an extra "initialize" method will be
                // necessary for locals
                add_local(current_compiler, identifier, true)?;
            }
            Ok(())
        }
        Definition::Syntax(SyntaxDefinition {
            keyword: _,
            spec: _,
        }) => Err(Error::UnsupportedCompilation(
            dbg_pls::pretty(&def).to_string(),
        )),
        Definition::Values(ValuesDefinition {
            formals: _,
            body: _,
        }) => Err(Error::UnsupportedCompilation(
            dbg_pls::pretty(&def).to_string(),
        )),
        Definition::Record(RecordDefinition {
            name: _,
            constructor: _,
            pred: _,
            fields: _,
        }) => Err(Error::UnsupportedCompilation(
            dbg_pls::pretty(&def).to_string(),
        )),
        Definition::Block(_) => Err(Error::UnsupportedCompilation(
            dbg_pls::pretty(&def).to_string(),
        )),
    }
}
