// SPDX-FileCopyrightText: 2023 Gerry Agbobada <git@gagbo.net>
//
// SPDX-License-Identifier: LGPL-3.0-only

use parser::ast::Literal;

use super::{push_code, push_constant, Compiler};
use crate::{Error, OpCode, Result, Value};
use std::{cell::RefCell, rc::Rc};

pub(super) fn compile_literal(current_compiler: Rc<RefCell<Compiler>>, lit: Literal) -> Result<()> {
    match lit {
        Literal::Bool(b) => {
            push_code(
                current_compiler,
                if b { OpCode::True } else { OpCode::False },
            );
            Ok(())
        }
        Literal::Num(_)
        | Literal::Vec(_)
        | Literal::Char(_)
        | Literal::StringLiteral(_)
        | Literal::Bytevector(_)
        | Literal::Symbol(_) => {
            // NOTE: Trading performance for memory efficiency in the chunks
            let lit_as_val = Value::from(lit);
            let constant = current_compiler
                .borrow()
                .target
                .borrow()
                .function
                .borrow()
                .chunk
                .constants
                .iter()
                .position(|val| val == &lit_as_val);

            if let Some(existing_constant) = constant {
                push_code(current_compiler, OpCode::Constant(existing_constant));
                return Ok(());
            }

            let constant = push_constant(current_compiler.clone(), lit_as_val);
            push_code(current_compiler, OpCode::Constant(constant));
            Ok(())
        }
        Literal::Compound(_) => Err(Error::UnsupportedCompilation(
            dbg_pls::pretty(&lit).to_string(),
        )),
        /* NOTE: This is a label assignment in literal, looking like
         * tree> '#0=(a b c . #0#)
         * Expression(
         *     Literal(Assignment {
         *         label: 0,
         *         datum: Compound(List {
         *             head: [Symbol("a"), Symbol("b"), Symbol("c")],
         *             tail: Some(Label(0)),
         *         }),
         *     }),
         * )
         * */
        Literal::Assignment { label: _, datum: _ } => Err(Error::UnsupportedCompilation(
            dbg_pls::pretty(&lit).to_string(),
        )),

        Literal::Label(_) => Err(Error::UnsupportedCompilation(
            dbg_pls::pretty(&lit).to_string(),
        )),
    }
}
