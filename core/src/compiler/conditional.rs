// SPDX-FileCopyrightText: 2023 Gerry Agbobada <git@gagbo.net>
//
// SPDX-License-Identifier: LGPL-3.0-only

use super::{
    expression::compile_expression, patch_jump, push_code, sequence::compile_sequence, Compiler,
};
use crate::{OpCode, Result};
use parser::ast::{IfExpr, UnlessExpr, WhenExpr};
use std::{cell::RefCell, rc::Rc};

pub(super) fn compile_if(
    current_compiler: Rc<RefCell<Compiler>>,
    if_expr: IfExpr,
    in_tail_position: bool,
) -> Result<()> {
    let IfExpr {
        test,
        consequent,
        alternate,
    } = if_expr;

    compile_expression(current_compiler.clone(), *test, false)?;

    let jump_index = push_code(current_compiler.clone(), OpCode::JumpIfFalse(0));

    push_code(current_compiler.clone(), OpCode::Pop);
    compile_expression(current_compiler.clone(), *consequent, in_tail_position)?;
    let jump_else_index = push_code(current_compiler.clone(), OpCode::Jump(0));
    patch_jump(current_compiler.clone(), jump_index)?;

    // We eat the predicate in all cases if the "if" test was falsey
    push_code(current_compiler.clone(), OpCode::Pop);
    if let Some(alternate) = alternate {
        compile_expression(current_compiler.clone(), *alternate, in_tail_position)?;
    } else {
        // In lack of alternate, we make (if) statement return False.
        // So after having eaten the predicate, we replace it with a literal false
        push_code(current_compiler.clone(), OpCode::False);
    }
    patch_jump(current_compiler, jump_else_index)?;

    Ok(())
}

pub(super) fn compile_when(
    current_compiler: Rc<RefCell<Compiler>>,
    when_expr: WhenExpr,
) -> Result<()> {
    let WhenExpr { test, consequent } = when_expr;
    compile_expression(current_compiler.clone(), *test, false)?;

    let jump_index = push_code(current_compiler.clone(), OpCode::JumpIfFalse(0));
    // Get rid of the condition
    push_code(current_compiler.clone(), OpCode::Pop);
    compile_sequence(current_compiler.clone(), consequent)?;
    let else_jump = push_code(current_compiler.clone(), OpCode::Jump(0));

    patch_jump(current_compiler.clone(), jump_index)?;
    // If the condition was false, we remove the conditional and replace it with a #f
    push_code(current_compiler.clone(), OpCode::Pop);
    push_code(current_compiler.clone(), OpCode::False);
    patch_jump(current_compiler, else_jump)?;
    Ok(())
}

pub(super) fn compile_unless(
    current_compiler: Rc<RefCell<Compiler>>,
    unless_expr: UnlessExpr,
) -> Result<()> {
    let UnlessExpr { test, consequent } = unless_expr;
    compile_expression(current_compiler.clone(), *test, false)?;

    let jump_index = push_code(current_compiler.clone(), OpCode::JumpIfTrue(0));
    // Get rid of the condition
    push_code(current_compiler.clone(), OpCode::Pop);
    compile_sequence(current_compiler.clone(), consequent)?;
    let else_jump = push_code(current_compiler.clone(), OpCode::Jump(0));

    patch_jump(current_compiler.clone(), jump_index)?;
    // If the condition was true, we remove the conditional and replace it with a #f
    push_code(current_compiler.clone(), OpCode::Pop);
    push_code(current_compiler.clone(), OpCode::False);
    patch_jump(current_compiler, else_jump)?;
    Ok(())
}
