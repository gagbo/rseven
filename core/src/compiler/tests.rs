// SPDX-FileCopyrightText: 2023 Gerry Agbobada <git@gagbo.net>
//
// SPDX-License-Identifier: LGPL-3.0-only

use super::*;
use crate::Chunk;
use parser::ast::{
    AstNode, Body, Expression, Formals, IfExpr, LambdaDefinition, LambdaExpr, Sequence,
};
use pretty_assertions::assert_eq;

fn compile_to_chunk(ast: Ast) -> Chunk {
    let root_compiler = Compiler::new(CompilationTarget::Script, 0);
    compile_ast(root_compiler.clone(), ast).unwrap();

    root_compiler
        .clone()
        .borrow()
        .target
        .borrow()
        .function
        .borrow()
        .chunk
        .clone()
}

#[test]
fn compile_global_access() {
    let chunk = compile_to_chunk(Ast {
        source: "",
        node: AstNode::Expression(Expression::Ident("global-var".to_string())),
    });

    assert_eq!(chunk.code, vec![OpCode::GetGlobal(0)]);
    assert_eq!(chunk.constants.len(), 1);
    assert!(matches!(&chunk.constants[0], Value::String(name) if name == "global-var"));
}

#[test]
fn compile_if() {
    let chunk = compile_to_chunk(Ast {
        source: "",
        node: AstNode::Expression(Expression::If(IfExpr {
            test: Box::new(Expression::Ident("test".to_string())),
            consequent: Box::new(Expression::Ident("consequent".to_string())),
            alternate: Some(Box::new(Expression::Ident("alternate".to_string()))),
        })),
    });

    assert_eq!(
        chunk.code,
        vec![
            OpCode::GetGlobal(0),   // Getting "test" value
            OpCode::JumpIfFalse(3), // Skipping to the alternate if false
            OpCode::Pop,            // Pop the test
            OpCode::GetGlobal(1),   // Getting the "consequent" value
            OpCode::Jump(2),        // Skipping the alternate branch
            OpCode::Pop,            // Start of the alternate branch
            OpCode::GetGlobal(2)    // Getting "alternate" value
        ]
    );

    let chunk = compile_to_chunk(Ast {
        source: "",
        node: AstNode::Expression(Expression::If(IfExpr {
            test: Box::new(Expression::Ident("test".to_string())),
            consequent: Box::new(Expression::Ident("consequent".to_string())),
            alternate: None,
        })),
    });

    assert_eq!(
        chunk.code,
        vec![
            OpCode::GetGlobal(0),   // Getting "test" value
            OpCode::JumpIfFalse(3), // Skipping if false
            OpCode::Pop,            // Pop the test
            OpCode::GetGlobal(1),   // Getting the "consequent" value
            OpCode::Jump(2),        // Skipping the alternate branch
            OpCode::Pop,            // Start of the alternate branch
            OpCode::False           // Getting "alternate" value
        ]
    );
}

#[test]
fn compile_lambda() {
    // The chunk represents
    // (lambda (arg) arg tail)
    let chunk = compile_to_chunk(Ast {
        source: "",
        node: AstNode::Expression(Expression::Lambda(LambdaExpr {
            formals: Formals::List(vec!["arg".to_string()]),
            body: Body {
                definitions: Vec::new(),
                sequence: Sequence {
                    commands: vec![Expression::Ident("arg".to_string())],
                    tail_expression: Box::new(Expression::Ident("tail".to_string())),
                },
            },
        })),
    });

    if let Value::Closure(f) = &chunk.constants[0] {
        assert_eq!(
            f.borrow().function.borrow().chunk.code,
            vec![OpCode::GetLocal(0), OpCode::GetGlobal(0), OpCode::Return]
        );
    } else {
        panic!()
    }
    assert_eq!(chunk.code, vec![OpCode::Closure(0, Vec::new())]);
}
