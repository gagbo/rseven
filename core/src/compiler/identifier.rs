// SPDX-FileCopyrightText: 2023 Gerry Agbobada <git@gagbo.net>
//
// SPDX-License-Identifier: LGPL-3.0-only

use super::{push_code, push_constant, resolve_local_variable, resolve_upvalue, Compiler};
use crate::{OpCode, Result, Value};
use std::{cell::RefCell, rc::Rc};

pub(super) fn compile_identifier_resolution(
    current_compiler: Rc<RefCell<Compiler>>,
    ident: String,
) -> Result<()> {
    if let Some(local_index) = resolve_local_variable(current_compiler.clone(), &ident)? {
        push_code(current_compiler, OpCode::GetLocal(local_index));
    } else if let Some(upvalue_index) = resolve_upvalue(current_compiler.clone(), &ident)? {
        push_code(current_compiler, OpCode::GetUpvalue(upvalue_index));
    } else {
        let constant = push_constant(current_compiler.clone(), Value::String(ident));
        push_code(current_compiler, OpCode::GetGlobal(constant));
    }
    Ok(())
}
