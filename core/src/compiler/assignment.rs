// SPDX-FileCopyrightText: 2023 Gerry Agbobada <git@gagbo.net>
//
// SPDX-License-Identifier: LGPL-3.0-only

use super::{
    expression::compile_expression, push_code, push_constant, resolve_local_variable,
    resolve_upvalue, Compiler,
};
use crate::{OpCode, Result, Value};
use parser::ast::Assignment;
use std::{cell::RefCell, rc::Rc};

pub(super) fn compile_assignment(
    current_compiler: Rc<RefCell<Compiler>>,
    assignment: Assignment,
) -> Result<()> {
    let Assignment { ident, value } = assignment;
    compile_expression(current_compiler.clone(), *value, false)?;

    if let Some(local_index) = resolve_local_variable(current_compiler.clone(), &ident)? {
        push_code(current_compiler, OpCode::SetLocal(local_index));
    } else if let Some(upvalue_index) = resolve_upvalue(current_compiler.clone(), &ident)? {
        push_code(current_compiler, OpCode::SetUpvalue(upvalue_index));
    } else {
        let constant = push_constant(current_compiler.clone(), Value::String(ident));
        push_code(current_compiler, OpCode::SetGlobal(constant));
    }
    Ok(())
}
