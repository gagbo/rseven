// SPDX-FileCopyrightText: 2023 Gerry Agbobada <git@gagbo.net>
//
// SPDX-License-Identifier: LGPL-3.0-only

use super::{
    add_local, begin_scope, definition::compile_definition, expression::compile_expression,
    push_code, push_constant, Compiler,
};
use crate::{CompilationTarget, OpCode, ProcArity, Result, Value};
use parser::ast::{Formals, LambdaExpr};
use std::{cell::RefCell, rc::Rc};
use tracing::trace;

pub(super) fn compile_lambda(
    current_compiler: Rc<RefCell<Compiler>>,
    lambda: LambdaExpr,
    name: Option<String>,
) -> Result<()> {
    let LambdaExpr { formals, body } = lambda;
    let lambda_compiler = Compiler::child(current_compiler.clone(), CompilationTarget::Function);
    let arity = match &formals {
        Formals::List(list) => ProcArity::Fixed(list.len().try_into().unwrap()),
        Formals::Single(_) => ProcArity::ListVar,
        Formals::Dotted { head, last: _ } => {
            ProcArity::FixedAndRest(head.len().try_into().unwrap())
        }
    };
    lambda_compiler
        .borrow()
        .target
        .borrow()
        .function
        .borrow_mut()
        .arity = arity;
    lambda_compiler
        .borrow()
        .target
        .borrow()
        .function
        .borrow_mut()
        .name = name.unwrap_or_default();

    // No lambda_compiler.end_scope() because we'll drop the temporary instead
    begin_scope(lambda_compiler.clone())?;
    match formals {
        Formals::List(vars) => {
            for param in vars {
                add_local(lambda_compiler.clone(), param, true)?;
            }
        }
        Formals::Single(list_var_name) => {
            add_local(lambda_compiler.clone(), list_var_name, true)?;
        }
        Formals::Dotted { head, last } => {
            for param in head {
                add_local(lambda_compiler.clone(), param, true)?;
            }

            add_local(lambda_compiler.clone(), last, true)?;
        }
    };

    for def in body.definitions {
        compile_definition(lambda_compiler.clone(), def)?;
    }
    for expr in body.sequence.commands {
        compile_expression(lambda_compiler.clone(), expr, false)?;
    }

    compile_expression(
        lambda_compiler.clone(),
        *body.sequence.tail_expression,
        true,
    )?;

    push_code(lambda_compiler.clone(), OpCode::Return);

    let procedure_var: Value = Value::Closure(lambda_compiler.borrow().target.clone());
    let constant = push_constant(current_compiler.clone(), procedure_var);
    push_code(
        current_compiler,
        OpCode::Closure(constant, lambda_compiler.borrow().upvalues.clone()),
    );

    // end_scope(lambda_compiler.clone(), true)?;

    trace!(
        upvalues = ?lambda_compiler.borrow().upvalues,
        "Lambda compilation result:\n{}",
        lambda_compiler
            .borrow()
            .target
            .borrow()
            .function
            .borrow()
            .chunk
            .disassemble(&lambda_compiler.borrow().target_name())
    );

    Ok(())
}
