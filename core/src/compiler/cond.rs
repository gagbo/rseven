// SPDX-FileCopyrightText: 2023 Gerry Agbobada <git@gagbo.net>
//
// SPDX-License-Identifier: LGPL-3.0-only

use super::{
    expression::compile_expression, patch_jump, push_code, sequence::compile_sequence, Compiler,
};
use crate::{OpCode, Result};
use parser::ast::{CondClause, CondExpr};
use std::{cell::RefCell, rc::Rc};

pub(super) fn compile_cond(
    current_compiler: Rc<RefCell<Compiler>>,
    cond_expr: CondExpr,
) -> Result<()> {
    let CondExpr { clauses, fallback } = cond_expr;
    let mut end_jumps = Vec::new();
    let mut next_clause_jump;
    for clause in clauses {
        match clause {
            CondClause::TestSeq { test, consequent } => {
                compile_expression(current_compiler.clone(), test, false)?;
                next_clause_jump = push_code(current_compiler.clone(), OpCode::JumpIfFalse(0));
                push_code(current_compiler.clone(), OpCode::Pop);
                compile_sequence(current_compiler.clone(), consequent)?;
            }
            CondClause::Test(test) => {
                compile_expression(current_compiler.clone(), test, false)?;
                next_clause_jump = push_code(current_compiler.clone(), OpCode::JumpIfFalse(0));
                // Nothing else, we leave the value of the test on top of the stack.
            }
            CondClause::TestRecipient { test, recipient } => {
                compile_expression(current_compiler.clone(), test, false)?;
                next_clause_jump = push_code(current_compiler.clone(), OpCode::JumpIfFalse(0));
                // We leave the value on the top of the stack, as it's called by recipient
                compile_expression(current_compiler.clone(), recipient, false)?;
                push_code(current_compiler.clone(), OpCode::Become(1));
            }
        }

        end_jumps.push(push_code(current_compiler.clone(), OpCode::Jump(0)));
        patch_jump(current_compiler.clone(), next_clause_jump)?;
    }

    push_code(current_compiler.clone(), OpCode::Pop); // Pop the condition if every cond clause was dealt with
    if let Some(fallback) = fallback {
        compile_sequence(current_compiler.clone(), fallback)?;
    } else {
        // Without fallback, behaviour is unspecified. We choose to make cond return #f
        push_code(current_compiler.clone(), OpCode::False);
    }

    for jump in end_jumps {
        patch_jump(current_compiler.clone(), jump)?;
    }

    Ok(())
}
