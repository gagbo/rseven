// SPDX-FileCopyrightText: 2023 Gerry Agbobada <git@gagbo.net>
//
// SPDX-License-Identifier: LGPL-3.0-only

use super::{
    add_local, begin_scope, definition::compile_definition, end_scope,
    expression::compile_expression, validate_locals_at_depth, Compiler,
};
use crate::Result;
use parser::ast::LetExpr;
use std::{cell::RefCell, rc::Rc};

pub(super) fn compile_let(
    current_compiler: Rc<RefCell<Compiler>>,
    let_expr: LetExpr,
    in_tail_position: bool,
) -> Result<()> {
    let LetExpr {
        has_star,
        bindings,
        body,
    } = let_expr;
    begin_scope(current_compiler.clone())?;
    for binding in bindings {
        compile_expression(current_compiler.clone(), binding.value, false)?;
        add_local(current_compiler.clone(), binding.binding, has_star)?;
    }
    let current_depth = current_compiler.borrow().scope_depth;
    validate_locals_at_depth(current_compiler.clone(), current_depth)?;
    for def in body.definitions {
        compile_definition(current_compiler.clone(), def)?;
    }
    for expr in body.sequence.commands {
        compile_expression(current_compiler.clone(), expr, false)?;
    }
    compile_expression(
        current_compiler.clone(),
        *body.sequence.tail_expression,
        in_tail_position,
    )?;
    end_scope(current_compiler, false)?;
    Ok(())
}
