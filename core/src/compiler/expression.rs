// SPDX-FileCopyrightText: 2023 Gerry Agbobada <git@gagbo.net>
//
// SPDX-License-Identifier: LGPL-3.0-only

use super::{
    assignment::compile_assignment,
    begin_scope,
    case::{compile_case_recipient_fallback, compile_case_sequence_fallback},
    cond::compile_cond,
    conditional::{compile_if, compile_unless, compile_when},
    derex_and_or::{compile_and, compile_or},
    derex_let::compile_let,
    end_scope,
    identifier::compile_identifier_resolution,
    lambda::compile_lambda,
    literal::compile_literal,
    procedure_call::compile_procedure_call,
    sequence::compile_sequence,
    Compiler,
};
use crate::{Error, Result};
use parser::ast::Expression;
use std::{cell::RefCell, rc::Rc};

pub(super) fn compile_expression(
    current_compiler: Rc<RefCell<Compiler>>,
    expr: Expression,
    in_tail_position: bool,
) -> Result<()> {
    match expr {
        Expression::Ident(ident) => compile_identifier_resolution(current_compiler, ident),
        Expression::Literal(lit) => {
            compile_literal(current_compiler, lit)?;
            Ok(())
        }
        Expression::ProcedureCall(proc_call) => {
            compile_procedure_call(current_compiler, proc_call, in_tail_position)
        }
        Expression::Lambda(lambda) => {
            compile_lambda(current_compiler, lambda, None)?;
            Ok(())
        }
        Expression::If(if_expr) => compile_if(current_compiler, if_expr, in_tail_position),
        Expression::Assignment(assignment) => compile_assignment(current_compiler, assignment),
        Expression::Cond(cond_expr) => compile_cond(current_compiler, cond_expr),
        Expression::CaseWithSequenceFallback(case_expr) => {
            compile_case_sequence_fallback(current_compiler, case_expr)
        }
        Expression::CaseWithRecipientFallback(case_expr) => {
            compile_case_recipient_fallback(current_compiler, case_expr)
        }
        Expression::And(and_expr) => compile_and(current_compiler, and_expr),
        Expression::Or(or_expr) => compile_or(current_compiler, or_expr),
        Expression::When(when_expr) => compile_when(current_compiler, when_expr),
        Expression::Unless(unless_expr) => compile_unless(current_compiler, unless_expr),
        Expression::Let(let_expr) => compile_let(current_compiler, let_expr, in_tail_position),
        Expression::NamedLet(parser::ast::NamedLetExpr {
            has_star,
            name,
            bindings,
            body,
        }) => {
            if has_star {
                return Err(Error::Unknown(
                    "named let must not have be 'let*'".to_string(),
                ));
            }
            // Compile bindings
            // Compile name
            // Create a scope where the name is a local procedure taking bindings
            // Remember the first bindings for the first "call" of the function.
            Ok(())
        }
        Expression::LetRec(parser::ast::LetRecExpr {
            has_star: _,
            bindings: _,
            body: _,
        }) => Err(Error::UnsupportedCompilation(
            dbg_pls::pretty(&expr).to_string(),
        )),
        Expression::LetValues(parser::ast::LetValuesExpr {
            has_star: _,
            bindings: _,
            body: _,
        }) => Err(Error::UnsupportedCompilation(
            dbg_pls::pretty(&expr).to_string(),
        )),
        Expression::BeginBlock(block) => {
            begin_scope(current_compiler.clone())?;
            compile_sequence(current_compiler.clone(), block)?;
            end_scope(current_compiler, false)?;
            Ok(())
        }
        Expression::DoLoop(parser::ast::DoLoopExpr {
            iteration_specs: _,
            test: _,
            result: _,
            commands: _,
        }) => Err(Error::UnsupportedCompilation(
            dbg_pls::pretty(&expr).to_string(),
        )),
        Expression::Delay(_) => Err(Error::UnsupportedCompilation(
            dbg_pls::pretty(&expr).to_string(),
        )),
        Expression::DelayForce(_) => Err(Error::UnsupportedCompilation(
            dbg_pls::pretty(&expr).to_string(),
        )),
        Expression::Parameterize(parser::ast::ParameterizeExpr {
            parameters: _,
            body: _,
        }) => Err(Error::UnsupportedCompilation(
            dbg_pls::pretty(&expr).to_string(),
        )),
        Expression::Guard(parser::ast::GuardExpr {
            ident: _,
            clauses: _,
            body: _,
        }) => Err(Error::UnsupportedCompilation(
            dbg_pls::pretty(&expr).to_string(),
        )),
        Expression::Quasiquotation(_) => Err(Error::UnsupportedCompilation(
            dbg_pls::pretty(&expr).to_string(),
        )),
        Expression::CaseLambda(_) => Err(Error::UnsupportedCompilation(
            dbg_pls::pretty(&expr).to_string(),
        )),
        Expression::MacroUse(parser::ast::MacroUse {
            keyword: _,
            data: _,
        }) => Err(Error::UnsupportedCompilation(
            dbg_pls::pretty(&expr).to_string(),
        )),
        Expression::MacroBlock(parser::ast::MacroBlock {
            is_rec: _,
            specs: _,
            body: _,
        }) => Err(Error::UnsupportedCompilation(
            dbg_pls::pretty(&expr).to_string(),
        )),
        Expression::Includer(_) => Err(Error::UnsupportedCompilation(
            dbg_pls::pretty(&expr).to_string(),
        )),
    }
}
