// SPDX-FileCopyrightText: 2023 Gerry Agbobada <git@gagbo.net>
//
// SPDX-License-Identifier: LGPL-3.0-only

use super::{expression::compile_expression, Compiler};
use crate::Result;
use parser::ast::Sequence;
use std::{cell::RefCell, rc::Rc};

pub(super) fn compile_sequence(
    current_compiler: Rc<RefCell<Compiler>>,
    sequence: Sequence,
) -> Result<()> {
    for expr in sequence.commands {
        compile_expression(current_compiler.clone(), expr, false)?;
    }
    compile_expression(current_compiler, *sequence.tail_expression, true)
}
