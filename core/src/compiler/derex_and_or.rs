// SPDX-FileCopyrightText: 2023 Gerry Agbobada <git@gagbo.net>
//
// SPDX-License-Identifier: LGPL-3.0-only

use super::{expression::compile_expression, patch_jump, push_code, Compiler};
use crate::{OpCode, Result};
use parser::ast::{AndExpr, OrExpr};
use std::{cell::RefCell, rc::Rc};

pub(super) fn compile_and(
    current_compiler: Rc<RefCell<Compiler>>,
    and_expr: AndExpr,
) -> Result<()> {
    let AndExpr { mut tests } = and_expr;
    if tests.is_empty() {
        push_code(current_compiler, OpCode::True);
        return Ok(());
    }

    let mut jumps = Vec::new();
    for _ in 0..tests.len() - 1 {
        let test = tests.pop_front().unwrap();
        compile_expression(current_compiler.clone(), test, false)?;
        jumps.push(push_code(current_compiler.clone(), OpCode::JumpIfFalse(0)));
        // If it's true, we pop the value and leave room for the
        // next test
        push_code(current_compiler.clone(), OpCode::Pop);
    }

    let last_test = tests.pop_front().unwrap();
    compile_expression(current_compiler.clone(), last_test, false)?;

    for jump in jumps {
        patch_jump(current_compiler.clone(), jump)?;
    }

    Ok(())
}

pub(super) fn compile_or(current_compiler: Rc<RefCell<Compiler>>, or_expr: OrExpr) -> Result<()> {
    let OrExpr { mut tests } = or_expr;
    if tests.is_empty() {
        push_code(current_compiler, OpCode::False);
        return Ok(());
    }

    let mut jumps = Vec::new();
    for _ in 0..tests.len() - 1 {
        let test = tests.pop_front().unwrap();
        compile_expression(current_compiler.clone(), test, false)?;
        jumps.push(push_code(current_compiler.clone(), OpCode::JumpIfTrue(0)));
        // If it's false, we pop the value and leave room for the
        // next test
        push_code(current_compiler.clone(), OpCode::Pop);
    }

    let last_test = tests.pop_front().unwrap();
    compile_expression(current_compiler.clone(), last_test, false)?;

    for jump in jumps {
        patch_jump(current_compiler.clone(), jump)?;
    }

    Ok(())
}
