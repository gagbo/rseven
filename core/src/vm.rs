// SPDX-FileCopyrightText: 2023 Gerry Agbobada <git@gagbo.net>
//
// SPDX-License-Identifier: LGPL-3.0-only

use std::collections::HashMap;

use gc::{Finalize, Gc, GcCell, Trace};
use itertools::Itertools;
use tracing::{trace, Instrument};

use crate::{
    compile, CallFrame, Closure, CompilationTarget, Compiler, Error, Heap, NativeProc, OpCode,
    ProcArity, Result, Upvalue, Value,
};

const INITIAL_STACK_SIZE: usize = 128;

// PERF: we skipped the hash table chapter and the interning part:
// - there's no point interning Symbols just yet (we can just compare
// strings at runtime to compare symbols, especially since symbols
// can either be direct String on the stack or in Gc<GcCell> on the heap)
// - there's no point interning String Literals for the same reason.
//
// Mostly, the fact that we have a string representation that can
// live on the stack (through the String struct), means that interning
// everything needs either:
// - a refactor of core::Value to store Symbol and String and Vec...
//   as HeapValues, or
// - special casing to do the comparison between interned values from
//   the stack or from the heap.
//
// This choice might bite later, so the interning is not out of question.
#[derive(Debug, Clone, Default, Trace, Finalize)]
pub struct VM {
    // NOTE: Vec has builtin functions to interact with the
    // furthest end of it (push, pop, peek), so we
    // use simply a Vec for the stack, and we restrict
    // ourselves to use the Stack-oriented API
    /// Value stack
    stack: Vec<Value>,
    globals: HashMap<String, Value>,
    current_frame: Option<Heap<CallFrame>>,
    // NOTE: We shouldn't need this, as all captured locals as upvalues are lifted
    // to the heap on capture.
    // open_upvalues: Vec<Heap<Value>>,
}

impl VM {
    pub fn new() -> Self {
        let mut new_vm = Self {
            stack: Vec::with_capacity(INITIAL_STACK_SIZE),
            globals: HashMap::new(),
            current_frame: None,
            // open_upvalues: Vec::new(),
        };

        new_vm.define_native(NativeProc::new(
            "+".to_string(),
            ProcArity::ListVar,
            crate::stdlib::add,
        ));

        new_vm.define_native(NativeProc::new(
            "-".to_string(),
            ProcArity::ListVar,
            crate::stdlib::sub,
        ));

        new_vm.define_native(NativeProc::new(
            "eq?".to_string(),
            ProcArity::ListVar,
            crate::stdlib::eq,
        ));

        new_vm.define_native(NativeProc::new(
            "clock".to_string(),
            ProcArity::Fixed(0),
            crate::stdlib::clock,
        ));

        new_vm
    }

    pub fn capture(&mut self, index: usize) -> Result<Heap<Value>> {
        let stack_index = self.get_offset_stack_index(index)?;
        let target = self.stack[stack_index].clone();
        if let Value::Upvalue(closed_upvalue) = &target {
            Ok(closed_upvalue.location.clone())
        } else {
            // We need to create a new captured value on the heap for this slot. In
            // order to have sibling children also reference this, we replace
            // the old stack value with the one on the heap
            let new_upvalue = Gc::new(GcCell::new(target));
            self.stack[stack_index] = Value::Upvalue(Upvalue {
                location: new_upvalue.clone(),
            });
            // self.open_upvalues.push(new_upvalue.clone());
            Ok(new_upvalue)
        }
    }

    pub fn define_native(&mut self, proc: NativeProc) {
        self.globals
            .insert(proc.name.clone(), Value::NativeProc(proc));
    }

    pub fn interpret(&mut self, source: &str) -> Result<Vec<Value>> {
        let root_compiler = Compiler::new(CompilationTarget::Script, 0);
        root_compiler
            .borrow()
            .target
            .borrow()
            .function
            .borrow_mut()
            .name = "top-level script".to_string();
        compile(root_compiler.clone(), source)?;

        let fun: Value = Value::Closure(root_compiler.borrow().target.clone());
        self.stack.push(fun.clone());
        self.call_value(&fun, 0, false)?;
        self.run()
    }

    fn get_ip(&self) -> Result<usize> {
        let frame = self.get_current_frame().ok_or(Error::NoCurrentFrame)?;
        Ok(frame.borrow().instruction_pointer)
    }

    fn get_ip_unchecked(&self) -> usize {
        let frame = self.current_frame.as_ref().unwrap();
        frame.borrow().instruction_pointer
    }

    fn get_current_frame(&self) -> Option<&Heap<CallFrame>> {
        self.current_frame.as_ref()
    }

    fn get_offset_stack_index(&self, local_index: usize) -> Result<usize> {
        Ok(local_index
            + self
                .get_current_frame()
                .ok_or(Error::NoCurrentFrame)?
                .borrow()
                .vm_stack_start_index)
    }

    fn read_constant(frame: &Heap<CallFrame>, index: usize) -> Value {
        frame.borrow().read_constant(index)
    }

    // TODO: Add a stack trace helper
    fn stack_trace(&self) -> String {
        todo!()
    }

    pub fn run(&mut self) -> Result<Vec<Value>> {
        // TODO: integrate a stack trace printer to create an error context around.
        // This is probably better done with a helper?
        // Note that stack traces are going to be more helpful when we have the
        // line data associated with the bytecode. So no rush here, having better
        // source code -> byte code traceability is a more important first step
        if self.current_frame.is_none() {
            return Err(Error::Unknown("No code to run".to_string()));
        }

        while let Some(inst) = self.next_opcode() {
            let frame = self.get_current_frame().unwrap().clone();
            trace!(
                instruction = %frame.borrow().closure.borrow().function.borrow().chunk.disassemble_inst(self.get_ip().unwrap() - 1),
                stack = %format!(
                    "[{}]",
                    Itertools::intersperse(
                        self.stack.iter().map(ToString::to_string),
                        "\n\t".to_string()
                    )
                    .collect::<String>()
                ),
                "Executing instruction",
            );
            match inst {
                OpCode::Return => {
                    let ret_val = self.pop_value()?;
                    let stack_top = self.stack.len();
                    let stack_begin = frame.borrow().vm_stack_start_index;
                    for _ in 0..(stack_top - stack_begin) {
                        self.pop_value()?;
                    }

                    let new_current_frame = self
                        .current_frame
                        .as_ref()
                        .unwrap()
                        .borrow()
                        .parent_frame
                        .clone();
                    self.current_frame = new_current_frame;
                    self.push_value(ret_val);

                    // This code returns all the values left on the top of the stack.
                    // This was done in a lazy attempt to design and get "values" working,
                    // but the proper solution is likely to do continuations instead
                    //
                    // let mut ret_val = Vec::new();
                    // let stack_top = self.stack.len();
                    // let stack_begin = frame.borrow().vm_stack_start_index;
                    // for _ in 0..(stack_top - stack_begin) {
                    //     ret_val.insert(0, self.pop_value()?);
                    // }

                    // let new_current_frame = self
                    //     .current_frame
                    //     .as_ref()
                    //     .unwrap()
                    //     .borrow()
                    //     .parent_frame
                    //     .clone();
                    // self.current_frame = new_current_frame;
                    // for val in ret_val {
                    //     self.stack.push(val)
                    // }
                }
                OpCode::Constant(index) => {
                    let val = frame
                        .borrow()
                        .closure
                        .borrow()
                        .function
                        .borrow()
                        .chunk
                        .constants[index]
                        .clone();
                    self.push_value(val);
                }
                OpCode::True => {
                    self.push_value(Value::Boolean(true));
                }
                OpCode::False => self.push_value(Value::Boolean(false)),
                OpCode::Nil => self.push_value(Value::Nil),
                OpCode::Eof => self.push_value(Value::Eof),
                OpCode::Eq => {
                    let second = self.peek_value(0)?;
                    let first = self.peek_value(1)?;
                    self.push_value(Value::Boolean(first == second));
                }
                OpCode::Pop => {
                    self.pop_value()?;
                }
                OpCode::Popn(pops) => {
                    for _ in 0..pops {
                        self.pop_value()?;
                    }
                }
                OpCode::CloseUpvalue => {
                    // This is called when a captured local value is about to get
                    // yeeted.

                    // NOTE: This does nothing special, because we lift ALL captured
                    // variable to the Heap directly in VM::capture
                    let value = self.pop_value()?;
                    // panic!("We are supposed to close on {:?} now", value);
                }
                OpCode::DefineGlobal(index) => {
                    let val = Self::read_constant(&frame, index);
                    if let Value::String(name) = &val {
                        let name = name.clone();
                        // NOTE: maybe here we could peek to keep the immutable borrow,
                        // and pop outside of the if statement...
                        let value = self.pop_value()?;
                        self.globals.insert(name, value);
                    } else {
                        return Err(Error::Unknown(
                            "Expected a string constant for variable name".to_string(),
                        ));
                    }
                }
                OpCode::GetGlobal(name_index) => {
                    let name = Self::read_constant(&frame, name_index);
                    if let Value::String(name) = &name {
                        // NOTE: maybe here we could peek to keep the immutable borrow,
                        // and pop outside of the if statement...
                        let value = self
                            .globals
                            .get(name)
                            .ok_or(Error::VoidVariable(name.clone()))?;
                        self.stack.push(value.clone());
                    } else {
                        return Err(Error::Unknown(
                            "Expected a string constant for variable name".to_string(),
                        ));
                    }
                }
                OpCode::SetGlobal(index) => {
                    let value = self.pop_value()?;

                    if let Value::String(name) = &frame
                        .borrow()
                        .closure
                        .borrow()
                        .function
                        .borrow()
                        .chunk
                        .constants[index]
                    {
                        *self
                            .globals
                            .get_mut(name)
                            .ok_or(Error::VoidVariable(name.clone()))? = value;
                    } else {
                        return Err(Error::Unknown(
                            "Expected a string constant for variable name".to_string(),
                        ));
                    }
                }
                OpCode::GetLocal(local_index) => {
                    let value = self
                        .stack
                        .get(self.get_offset_stack_index(local_index)?)
                        .ok_or(Error::Unknown("Wrong stack index for local".to_string()))?;
                    self.stack.push(value.clone());
                }
                OpCode::SetLocal(local_index) => {
                    let value = self.pop_value()?;
                    let global_stack_index = self.get_offset_stack_index(local_index)?;
                    *self
                        .stack
                        .get_mut(global_stack_index)
                        .ok_or(Error::Unknown("Wrong stack index for local".to_string()))? =
                        value.clone();
                }
                OpCode::GetUpvalue(upvalue_index) => {
                    // This code should check whether the upvalue needs to be created out of a
                    // stack local value??
                    self.push_value(Value::Upvalue(Upvalue {
                        location: frame.borrow().closure.borrow().upvalues[upvalue_index].clone(),
                    }));
                }
                OpCode::SetUpvalue(upvalue_index) => {
                    // This code should check whether the upvalue needs to be created out of a
                    // stack local value??
                    let value = self.pop_value()?;
                    *frame.borrow().closure.borrow().upvalues[upvalue_index].borrow_mut() = value;
                }
                OpCode::JumpIfFalse(offset) => {
                    let value = self.peek_value(0)?;
                    if !value.is_truthy() {
                        frame.borrow_mut().advance_ip(offset);
                    }
                }
                OpCode::JumpIfTrue(offset) => {
                    let value = self.peek_value(0)?;
                    if value.is_truthy() {
                        frame.borrow_mut().advance_ip(offset);
                    }
                }
                OpCode::Jump(offset) => frame.borrow_mut().advance_ip(offset),
                OpCode::Loop(offset) => frame.borrow_mut().backwards_ip(offset),
                OpCode::Become(arg_count) => {
                    // Create a new frame and make the call
                    let function = self.pop_value()?;
                    self.call_value(&function, arg_count, true)?;
                }
                OpCode::Call(arg_count) => {
                    // Create a new frame and make the call
                    let function = self.pop_value()?;
                    self.call_value(&function, arg_count, false)?;
                }
                OpCode::Closure(index, upvalues) => {
                    let val = frame
                        .borrow()
                        .closure
                        .borrow()
                        .function
                        .borrow()
                        .chunk
                        .constants[index]
                        .clone();
                    if let Value::SchemeBareProc(ref fun) = val {
                        let mut closure = Closure::new(fun.clone());
                        closure.upvalue_count = upvalues.len();
                        for upvalue in upvalues {
                            if upvalue.is_local {
                                let capture = self.capture(upvalue.index)?;
                                closure.upvalues.push(capture);
                            } else {
                                closure.upvalues.push(
                                    frame.borrow().closure.borrow().upvalues[upvalue.index].clone(),
                                );
                            }
                        }
                        let closure = closure.into();
                        self.push_value(closure);
                    } else if let Value::Closure(ref fun) = val {
                        let mut closure = Closure::new(fun.borrow().function.clone());
                        closure.upvalue_count = upvalues.len();
                        for upvalue in upvalues {
                            if upvalue.is_local {
                                let capture = self.capture(upvalue.index)?;
                                closure.upvalues.push(capture);
                            } else {
                                closure.upvalues.push(
                                    frame.borrow().closure.borrow().upvalues[upvalue.index].clone(),
                                );
                            }
                        }
                        let closure = closure.into();
                        self.push_value(closure);
                    } else {
                        return Err(Error::Unknown(format!(
                            "VM can't build a closure from a non function, got {val:?} instead"
                        )));
                    }
                }
            }
        }

        let mut ret_val = Vec::new();

        // The initial thunk (top level script built in interpret) is still in the
        // stack at the 0th value, so we pop everything except the
        // last value
        while self.stack.len() > 1 {
            let val = self.pop_value().unwrap();
            // We only insert the top value of the stack in the return vector.
            // Effectively that means we always return at most one value
            if ret_val.is_empty() {
                ret_val.insert(0, val);
            }
        }

        // Cleaning up the stack by removing that top level script
        self.pop_value()?;

        Ok(ret_val)
    }

    /// Pop values away from the value stack, so that
    /// the start of stack is now at new_start.
    ///
    /// That means that stack[new_start] gets deleted and
    /// is the last element to get deleted.
    fn clear_stack_until(&mut self, new_start: usize) -> Result<()> {
        if self.stack.len() < new_start {
            return Err(Error::Unknown(
                "Trying to clear stack to an invalid level".to_string(),
            ));
        }

        if self.stack.len() == new_start {
            return Ok(());
        }

        while self.stack.len() > new_start {
            self.pop_value()?;
        }

        Ok(())
    }

    fn call_value(
        &mut self,
        callee: &Value,
        arg_count: usize,
        replace_current_frame: bool,
    ) -> Result<()> {
        match callee {
            Value::Continuation(_) => todo!(),
            Value::Closure(ref p) => {
                let mut vm_stack_start_index = self.stack.len() - arg_count;

                match p.borrow().function.borrow().arity {
                    crate::ProcArity::Fixed(f) => {
                        if f as usize != arg_count {
                            return Err(Error::Unknown("Wrong number of arguments".to_string()));
                        }
                    }
                    crate::ProcArity::ListVar => {
                        // Consuming all the "rest" values on the stack to build a list for it.
                        let mut arg_list = Vec::new();
                        for _ in 0..arg_count {
                            arg_list.insert(0, self.pop_value()?);
                        }
                        if replace_current_frame {
                            let old_frame_start = self.get_current_frame().ok_or_else(||Error::Unknown("Cannot replace 'current frame' if there's no current frame".to_string()))?.borrow().vm_stack_start_index;
                            self.clear_stack_until(old_frame_start)?;
                        }
                        self.push_value(arg_list.into_iter().collect());
                        vm_stack_start_index = self.stack.len() - 1;
                    }
                    crate::ProcArity::FixedAndRest(f) => {
                        // This statement guards the `arg_count - f` from underflow later.
                        if arg_count < f as usize {
                            return Err(Error::Unknown("Wrong number of arguments".to_string()));
                        }

                        // Consuming all the extra arguments for the "rest" values on the stack
                        let mut arg_list = Vec::new();
                        for _ in 0..arg_count - f as usize {
                            arg_list.insert(0, self.pop_value()?);
                        }
                        if replace_current_frame {
                            let old_frame_start = self.get_current_frame().ok_or_else(||Error::Unknown("Cannot replace 'current frame' if there's no current frame".to_string()))?.borrow().vm_stack_start_index;
                            self.clear_stack_until(old_frame_start)?;
                        }
                        self.push_value(arg_list.into_iter().collect());
                    }
                }

                let frame = CallFrame {
                    closure: p.clone(),
                    instruction_pointer: 0,
                    vm_stack_start_index,
                    parent_frame: if replace_current_frame {
                        self.get_current_frame()
                            .and_then(|parent| parent.borrow().parent_frame.clone())
                    } else {
                        self.get_current_frame().cloned()
                    },
                };
                self.current_frame = Some(Gc::new(GcCell::new(frame)));
                Ok(())
            }
            Value::NativeProc(ref n) => {
                let mut args = Vec::new();

                match n.arity {
                    crate::ProcArity::Fixed(f) => {
                        if f as usize != arg_count {
                            return Err(Error::Unknown("Wrong number of arguments".to_string()));
                        }

                        for _ in 0..arg_count {
                            args.insert(0, self.stack.pop().unwrap());
                        }
                    }
                    crate::ProcArity::ListVar => {
                        // Consuming all the "rest" values on the stack to build a list for it.
                        let mut arg_list = Vec::new();
                        for _ in 0..arg_count {
                            arg_list.insert(0, self.pop_value()?);
                        }
                        args.push(arg_list.into_iter().collect());
                        if replace_current_frame {
                            let old_frame_start = self.get_current_frame().ok_or_else(||Error::Unknown("Cannot replace 'current frame' if there's no current frame".to_string()))?.borrow().vm_stack_start_index;
                            self.clear_stack_until(old_frame_start)?;
                        }
                    }
                    crate::ProcArity::FixedAndRest(f) => {
                        if arg_count < f as usize {
                            return Err(Error::Unknown("Wrong number of arguments".to_string()));
                        }

                        let mut arg_list = Vec::new();
                        for _ in 0..arg_count - f as usize {
                            arg_list.insert(0, self.pop_value()?);
                        }
                        args.push(arg_list.into_iter().collect());

                        for _ in 0..f {
                            args.insert(0, self.stack.pop().unwrap());
                        }
                        if replace_current_frame {
                            let old_frame_start = self.get_current_frame().ok_or_else(||Error::Unknown("Cannot replace 'current frame' if there's no current frame".to_string()))?.borrow().vm_stack_start_index;
                            self.clear_stack_until(old_frame_start)?;
                        }
                    }
                }

                let results = (n.function)(args)?;
                for res in results {
                    self.stack.push(res);
                }

                Ok(())
            }
            Value::Upvalue(inner) => {
                self.call_value(&inner.location.borrow(), arg_count, replace_current_frame)
            }
            other => Err(Error::Unknown(format!(
                "Wrong type, expected procedure, got {other:?}",
            ))),
        }
    }

    fn next_opcode(&mut self) -> Option<OpCode> {
        let frame = self.get_current_frame();

        match frame {
            Some(frame) => {
                let current_ip = self.get_ip_unchecked();

                let res = frame
                    .borrow_mut()
                    .closure
                    .borrow()
                    .function
                    .borrow()
                    .chunk
                    .code
                    .get(current_ip)
                    .cloned();
                frame.borrow_mut().advance_ip(1);

                res
            }
            None => None,
        }
    }

    fn push_value(&mut self, value: Value) {
        self.stack.push(value);
    }

    fn pop_value(&mut self) -> Result<Value> {
        self.stack.pop().ok_or(Error::EmptyStack)
    }

    /// Read depth positions from the top of the stack without
    /// popping anything.
    ///
    /// peek_value(0) looks at the top of the stack,
    /// peek_value(self.stack.len() - 1) looks at the bottom-most value in the stack
    fn peek_value(&self, depth: usize) -> Result<&Value> {
        self.stack
            .get(self.stack.len() - depth - 1)
            .ok_or(Error::EmptyStack)
    }

    pub fn reset_stack(&mut self) {
        self.stack.clear();
    }
}
