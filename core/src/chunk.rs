// SPDX-FileCopyrightText: 2023 Gerry Agbobada <git@gagbo.net>
//
// SPDX-License-Identifier: LGPL-3.0-only

use gc::{Finalize, Trace};

use crate::{CompileTimeUpvalue, Error, Result, Value};

pub type ConstantTableIndex = usize;
pub type UpvalueTableIndex = usize;

/// 0 is the bottom of the stack
pub type ValueStackIndex = usize;

pub type ForwardsInstructionPointerOffset = usize;
pub type BackwardsInstructionPointerOffset = usize;

pub type ArgCount = usize;

#[derive(Debug, Clone, PartialEq, Eq)]
pub enum OpCode {
    /// Return from the current callframe.
    ///
    /// Stack effect = -X where X is the number of values in the current callframe's stack
    Return,
    // NOTE: using a usize for the index here means
    // that we might lose space as Chunk are unlikely
    // to need that much indexing
    /// Access a value from the constant table of
    /// the chunk.
    ///
    /// Stack effect = +1
    Constant(ConstantTableIndex),
    /// `#true` constant
    ///
    /// Stack effect = +1
    True,
    /// `#false` constant
    ///
    /// Stack effect = +1
    False,
    /// `()` constant (nil)
    ///
    /// Stack effect = +1
    Nil,
    /// Test if the 2 top values on the stack are equal
    ///
    /// Stack effect = +1 (reads 2 elements and leave the retval on stack)
    Eq,
    /// End of File object constant
    ///
    /// Stack effect = +1
    Eof,
    /// Just pop a value from the stack
    ///
    /// Stack effect = -1
    Pop,
    /// Just pop X values from the stack
    ///
    /// Stack effect = -X
    Popn(usize),
    /// Define a global variable whose name is in
    /// the given constant table index.
    ///
    /// Stack effect = -1
    DefineGlobal(ConstantTableIndex),
    /// Get the global variable whose name is in
    /// the given constant table index.
    ///
    /// Stack effect = +1
    GetGlobal(ConstantTableIndex),
    /// Set the global variable whose name is in
    /// the given constant table index.
    ///
    /// Stack effect = -1
    SetGlobal(ConstantTableIndex),
    /// Get the local variable whose position in
    /// the locals stack at
    /// the given index.
    ///
    /// Stack effect = +1
    GetLocal(ValueStackIndex),
    /// Set the local variable whose position in
    /// the locals stack at
    /// the given index.
    ///
    /// Stack effect = -1
    SetLocal(ValueStackIndex),
    /// Get the local upvalue whose position in
    /// the upvalue table at
    /// the given index.
    ///
    /// Stack effect = +1
    GetUpvalue(UpvalueTableIndex),
    /// Set the local upvalue whose position in
    /// the upvalue table at
    /// the given index.
    ///
    /// Stack effect = -1
    SetUpvalue(UpvalueTableIndex),
    /// Jump if false, contains the offset to add
    /// to the IP
    ///
    /// Stack effect = 0
    JumpIfFalse(ForwardsInstructionPointerOffset),
    /// Jump if true, contains the offset to add
    /// to the IP
    ///
    /// Stack effect = 0
    JumpIfTrue(ForwardsInstructionPointerOffset),
    /// Jump inconditionally, contains the offset
    /// to the IP
    ///
    /// Stack effect = 0
    Jump(ForwardsInstructionPointerOffset),
    /// Jump inconditionally BACKWARDS, contains the offset
    /// to the IP
    ///
    /// Stack effect = 0
    Loop(BackwardsInstructionPointerOffset),
    /// Call the operator on the stack, consuming the
    /// given amount of arguments _and replacing the current frame_
    ///
    /// This is the operator to call to keep the stack growth under check
    /// with tail call eliminations.
    ///
    /// Stack effect = -1 (operator) -X (arguments) +Y (the number of return values operator leaves on the stack)
    Become(ArgCount),
    /// Call the operator on the stack, consuming the
    /// given amount of arguments
    ///
    /// Stack effect = -1 (operator) -X (arguments) +Y (the number of return values operator leaves on the stack)
    Call(ArgCount),
    /// Build a closure from the function that's stored
    /// in the given constant table index
    Closure(ConstantTableIndex, Vec<CompileTimeUpvalue>),
    /// Closes over the local on the stack
    ///
    /// Stack effect = -1
    CloseUpvalue,
}

#[derive(Debug, Clone, Default, Trace, Finalize)]
pub struct Chunk {
    // SAFETY
    // OpCode does not contain any Gc type in its variants
    #[unsafe_ignore_trace]
    pub(crate) code: Vec<OpCode>,
    pub(crate) constants: Vec<Value>,
    // TODO: Add an array of lines to match the originating line with each
    // opcode or something. It would need a refactor in the parser to add
    // location information in all AST nodes. So it will come later.
}

impl Chunk {
    pub fn new() -> Self {
        Self {
            code: vec![],
            constants: vec![],
        }
    }

    /// Return the index of the instruction we just pushed
    pub fn push_code(&mut self, code: OpCode) -> usize {
        self.code.push(code);
        self.code.len() - 1
    }

    pub fn patch_jump(&mut self, jump_inst_index: usize) -> Result<()> {
        let offset = self.code.len() - 1 - jump_inst_index;
        let new_code = match self.code[jump_inst_index] {
            OpCode::JumpIfFalse(_) => OpCode::JumpIfFalse(offset),
            OpCode::JumpIfTrue(_) => OpCode::JumpIfTrue(offset),
            OpCode::Jump(_) => OpCode::Jump(offset),
            _ => {
                return Err(Error::Unknown(
                    "Trying to patch a jump at an offset that doesn't have a jump".to_string(),
                ))
            }
        };
        self.code[jump_inst_index] = new_code;
        Ok(())
    }

    pub fn push_constant(&mut self, constant: Value) -> usize {
        if let Some((constant_index, _)) = self
            .constants
            .iter()
            .enumerate()
            .find(|(_, existing_const)| constant.ptr_eq(existing_const))
        {
            constant_index
        } else {
            self.constants.push(constant);
            self.constants.len() - 1
        }
    }

    pub fn disassemble_inst(&self, index: usize) -> String {
        use std::fmt::Write;
        let mut s = String::new();
        write!(&mut s, "{index:04}\t").unwrap();
        match self.code[index].clone() {
            OpCode::Return => {
                write!(&mut s, "RETURN").unwrap();
            }
            OpCode::Constant(constant_index) => {
                write!(&mut s, "CONSTANT {constant_index}").unwrap();
                write!(&mut s, "\t{}", self.constants[constant_index]).unwrap();
            }
            OpCode::True => write!(&mut s, "TRUE").unwrap(),
            OpCode::False => write!(&mut s, "FALSE").unwrap(),
            OpCode::Nil => write!(&mut s, "NIL").unwrap(),
            OpCode::Eof => write!(&mut s, "EOF").unwrap(),
            OpCode::Eq => write!(&mut s, "EQ").unwrap(),
            OpCode::Pop => write!(&mut s, "POP").unwrap(),
            OpCode::Popn(pops) => write!(&mut s, "POP_N {pops}").unwrap(),
            OpCode::DefineGlobal(constant_index) => {
                write!(&mut s, "DEFINE_GLOBAL {constant_index}").unwrap();
                write!(&mut s, "\t{}", self.constants[constant_index]).unwrap();
            }
            OpCode::GetGlobal(constant_index) => {
                write!(&mut s, "GET_GLOBAL {constant_index}").unwrap();
                write!(&mut s, "\t{}", self.constants[constant_index]).unwrap();
            }
            OpCode::SetGlobal(constant_index) => {
                write!(&mut s, "SET_GLOBAL {constant_index}").unwrap();
                write!(&mut s, "\t{}", self.constants[constant_index]).unwrap();
            }
            OpCode::GetLocal(stack_index) => {
                write!(&mut s, "GET_LOCAL {stack_index}").unwrap();
            }
            OpCode::SetLocal(stack_index) => {
                write!(&mut s, "SET_LOCAL {stack_index}").unwrap();
            }
            OpCode::GetUpvalue(upvalue_index) => {
                write!(&mut s, "GET_UPVALUE {upvalue_index}").unwrap();
            }
            OpCode::SetUpvalue(upvalue_index) => {
                write!(&mut s, "SET_UPVALUE {upvalue_index}").unwrap();
            }
            OpCode::JumpIfFalse(jump) => {
                write!(&mut s, "JUMP_IF_FALSE {jump}").unwrap();
            }
            OpCode::JumpIfTrue(jump) => {
                write!(&mut s, "JUMP_IF_TRUE {jump}").unwrap();
            }
            OpCode::Jump(jump) => {
                write!(&mut s, "JUMP {jump}").unwrap();
            }
            OpCode::Loop(back_jump) => {
                write!(&mut s, "LOOP {back_jump}").unwrap();
            }
            OpCode::Become(arg_count) => {
                write!(&mut s, "BECOME {arg_count}").unwrap();
            }
            OpCode::Call(arg_count) => {
                write!(&mut s, "CALL {arg_count}").unwrap();
            }
            OpCode::Closure(constant_index, upvalues) => {
                write!(&mut s, "CLOSURE {constant_index}").unwrap();
                write!(&mut s, "\t{}", self.constants[constant_index]).unwrap();
                for upvalue in upvalues {
                    write!(
                        &mut s,
                        "\n\t\t{} {}",
                        if upvalue.is_local { "local" } else { "upvalue" },
                        upvalue.index
                    )
                    .unwrap();
                }
            }
            OpCode::CloseUpvalue => {
                write!(&mut s, "CLOSE_UPVALUE").unwrap();
            }
        }
        writeln!(&mut s).unwrap();
        s
    }

    pub fn disassemble(&self, name: &str) -> String {
        use std::fmt::Write;
        let mut s = String::new();

        writeln!(&mut s, "== {name} ==").unwrap();
        for (i, _inst) in self.code.iter().enumerate() {
            write!(&mut s, "{}", self.disassemble_inst(i)).unwrap();
        }

        s
    }
}
