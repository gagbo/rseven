// SPDX-FileCopyrightText: 2023 Gerry Agbobada <git@gagbo.net>
//
// SPDX-License-Identifier: LGPL-3.0-only

mod assignment;
mod case;
mod cond;
mod conditional;
mod definition;
mod derex_and_or;
mod derex_let;
mod expression;
mod identifier;
mod lambda;
mod literal;
mod procedure_call;
mod sequence;
#[cfg(test)]
mod tests;

use crate::{Closure, Error, Heap, OpCode, Result, UpvalueTableIndex, Value, ValueStackIndex};
use definition::compile_definition;
use expression::compile_expression;
use gc::{Gc, GcCell};
use parser::{ast::Identifier, Ast};
use std::{cell::RefCell, rc::Rc};
use tracing::trace;

const INITIAL_LOCALS_ARRAY_SIZE: usize = 128;
const INITIAL_MACRO_NAMES_ARRAY_SIZE: usize = 256;

// NOTE: all struct attributes that use a Vec here instead of a fixed size array
// mean that the generated instructions have to take usize operands in the
// opcode (e.g. OpCode::DefineGlobal needs to wrap a usize)
//
// This makes the bytecode less compact, but for now I don't care.

// NOTE: This whole module is an interior mutability fiesta, as Crafting Interpreters
// uses a lot of mutating global scope pointers to maintain the state of compilation.
//
// Later, after everything is working, we'll review it to see if we can reduce the
// number of runtime checks.

#[derive(Debug)]
pub struct Compiler {
    pub(crate) macro_names: Vec<Identifier>,
    locals: Vec<Local>,
    scope_depth: usize,
    target_type: CompilationTarget,
    upvalues: Vec<CompileTimeUpvalue>,
    pub(crate) target: Heap<Closure>,
    parent_compiler: Option<Rc<RefCell<Self>>>,
}

#[derive(Debug, Clone, Copy)]
pub enum CompilationTarget {
    Function,
    Script,
}

/// A local variable in a given scope
#[derive(Debug, Clone, Default)]
struct Local {
    name: String,
    depth: usize,
    /// A local "is valid" if the "name" is allowed to resolve to this
    /// local.
    ///
    /// Notably, a local is _not_ valid while compiling the binding
    /// specs of a let bindings; but _is_ valid while compiling the
    /// binding specs of a let* binding.
    is_valid: bool,
    is_captured: bool,
}

/// An upvalue
#[derive(Debug, Clone, Default, Copy, PartialEq, Eq)]
pub struct CompileTimeUpvalue {
    pub(crate) is_local: bool,
    pub(crate) index: usize,
}

impl Compiler {
    pub fn new(target_type: CompilationTarget, scope_depth: usize) -> Rc<RefCell<Self>> {
        let locals = Vec::with_capacity(INITIAL_LOCALS_ARRAY_SIZE);
        Rc::new(RefCell::new(Self {
            macro_names: Vec::with_capacity(INITIAL_MACRO_NAMES_ARRAY_SIZE),
            locals,
            scope_depth,
            target_type,
            target: Gc::new(GcCell::new(Closure::default())),
            upvalues: Vec::new(),
            parent_compiler: None,
        }))
    }

    pub fn child(parent: Rc<RefCell<Self>>, target_type: CompilationTarget) -> Rc<RefCell<Self>> {
        let scope_depth = parent.borrow().scope_depth;
        let locals = Vec::with_capacity(INITIAL_LOCALS_ARRAY_SIZE);
        Rc::new(RefCell::new(Self {
            macro_names: Vec::with_capacity(INITIAL_MACRO_NAMES_ARRAY_SIZE),
            locals,
            scope_depth,
            target_type,
            target: Gc::new(GcCell::new(Closure::default())),
            upvalues: Vec::new(),
            parent_compiler: Some(parent),
        }))
    }

    pub fn target_name(&self) -> String {
        if self.target.borrow().function.borrow().name.is_empty() {
            "<script>".to_string()
        } else {
            self.target.borrow().function.borrow().name.clone()
        }
    }
}

fn in_global_scope(current_compiler: Rc<RefCell<Compiler>>) -> bool {
    current_compiler.borrow().scope_depth == 0
}

fn begin_scope(current_compiler: Rc<RefCell<Compiler>>) -> Result<()> {
    let new_depth = current_compiler
        .borrow()
        .scope_depth
        .checked_add(1)
        .ok_or(Error::Compilation("Too many scopes".to_string()))?;
    current_compiler.borrow_mut().scope_depth = new_depth;
    Ok(())
}

// TODO: clear_stack might be unnecessary, as it always seems to be "false" ??
// Basically, is there a scope where we don't want to return the last expression
// from?
fn end_scope(current_compiler: Rc<RefCell<Compiler>>, clear_stack: bool) -> Result<()> {
    let new_depth =
        current_compiler
            .borrow()
            .scope_depth
            .checked_sub(1)
            .ok_or(Error::Compilation(
                "Cannot end the global scope".to_string(),
            ))?;
    current_compiler.borrow_mut().scope_depth = new_depth;

    // Pop all the remaining local variables from the stack
    loop {
        if current_compiler
            .borrow()
            .locals
            .last()
            .map_or(true, |top_local| top_local.depth <= new_depth)
        {
            break;
        }

        // NOTE: We could accumulate the number of variables to pop
        // and add the suggested OpCode::PopN(usize) to have a single
        // instruction popping everything from the stack.
        let local = current_compiler.borrow_mut().locals.pop().unwrap();
        if clear_stack {
            if local.is_captured {
                push_code(current_compiler.clone(), OpCode::CloseUpvalue);
            } else {
                push_code(current_compiler.clone(), OpCode::Pop);
            }
        }
    }

    Ok(())
}

fn patch_jump(current_compiler: Rc<RefCell<Compiler>>, jump_inst_index: usize) -> Result<()> {
    current_compiler
        .borrow()
        .target
        .borrow()
        .function
        .borrow_mut()
        .chunk
        .patch_jump(jump_inst_index)
}

fn push_code(current_compiler: Rc<RefCell<Compiler>>, code: OpCode) -> usize {
    current_compiler
        .borrow()
        .target
        .borrow()
        .function
        .borrow_mut()
        .chunk
        .push_code(code)
}

fn push_constant(current_compiler: Rc<RefCell<Compiler>>, constant: Value) -> usize {
    current_compiler
        .borrow()
        .target
        .borrow()
        .function
        .borrow_mut()
        .chunk
        .push_constant(constant)
}

pub fn compile(current_compiler: Rc<RefCell<Compiler>>, source: &str) -> Result<()> {
    let ast: Ast = parser::parse(source).map_err(Error::from)?;
    compile_ast(current_compiler, ast)
}

fn compile_ast(current_compiler: Rc<RefCell<Compiler>>, ast: Ast) -> Result<()> {
    match ast.node {
        parser::ast::AstNode::Nil => {
            let constant = push_constant(current_compiler.clone(), Value::Nil);
            push_code(current_compiler.clone(), OpCode::Constant(constant));
        }
        parser::ast::AstNode::Expression(expr) => {
            compile_expression(current_compiler.clone(), expr, false)?;
        }
        parser::ast::AstNode::Definition(def) => {
            compile_definition(current_compiler.clone(), def)?;
        }
        parser::ast::AstNode::Program(_) => {
            return Err(Error::UnsupportedCompilation(
                dbg_pls::pretty(&ast.node).to_string(),
            ));
        }
        parser::ast::AstNode::Library(_) => {
            return Err(Error::UnsupportedCompilation(
                dbg_pls::pretty(&ast.node).to_string(),
            ));
        }
    }
    trace!(
        "Compilation result:\n{}",
        current_compiler
            .borrow()
            .target
            .borrow()
            .function
            .borrow()
            .chunk
            .disassemble(&current_compiler.borrow().target_name())
    );
    Ok(())
}

fn add_local(
    current_compiler: Rc<RefCell<Compiler>>,
    name: String,
    is_valid: bool,
) -> Result<usize> {
    let local = Local {
        name,
        depth: current_compiler.borrow().scope_depth,
        is_valid,
        is_captured: false,
    };
    current_compiler.borrow_mut().locals.push(local);
    Ok(current_compiler.borrow().locals.len())
}

fn validate_locals_at_depth(current_compiler: Rc<RefCell<Compiler>>, depth: usize) -> Result<()> {
    for local in current_compiler.borrow_mut().locals.iter_mut() {
        if local.depth == depth {
            local.is_valid = true;
        }
    }
    Ok(())
}

fn resolve_local_variable(
    current_compiler: Rc<RefCell<Compiler>>,
    name: &str,
) -> Result<Option<ValueStackIndex>> {
    // This is a trick to end the borrow before the for loop.
    let locals_len = current_compiler.borrow().locals.len();
    // NOTE: Hopefully, the rev() call here is _very_ fast, as std::slice::Iter implements DoubleEndedIterator trait, and
    // if we're lucky enough, rev() is going to have a shortcut implementation for those iterators.
    for (i, local) in current_compiler
        .borrow_mut()
        .locals
        .iter_mut()
        .rev()
        .enumerate()
    {
        if local.is_valid && local.name == name {
            local.is_captured = true;
            return Ok(Some(locals_len - i - 1));
        }
    }

    Ok(None)
}

fn resolve_upvalue(
    current_compiler: Rc<RefCell<Compiler>>,
    name: &str,
) -> Result<Option<UpvalueTableIndex>> {
    if current_compiler.borrow().parent_compiler.is_none() {
        return Ok(None);
    }

    let parent_local_index = resolve_local_variable(
        current_compiler.borrow().parent_compiler.clone().unwrap(),
        name,
    )?;
    if let Some(parent_local_index) = parent_local_index {
        return Ok(Some(add_upvalue(
            current_compiler,
            parent_local_index,
            true,
        )?));
    }

    let parent_upvalue_index = resolve_upvalue(
        current_compiler.borrow().parent_compiler.clone().unwrap(),
        name,
    )?;
    if let Some(parent_upvalue_index) = parent_upvalue_index {
        return Ok(Some(add_upvalue(
            current_compiler,
            parent_upvalue_index,
            false,
        )?));
    }

    Ok(None)
}

fn add_upvalue(
    current_compiler: Rc<RefCell<Compiler>>,
    parent_local_index: usize,
    is_local: bool,
) -> Result<UpvalueTableIndex> {
    for (i, upvalue) in current_compiler.borrow().upvalues.iter().enumerate() {
        if upvalue.index == parent_local_index && upvalue.is_local == is_local {
            return Ok(i);
        }
    }

    //FIXME: The issue here is that Crafting Interpreters uses pointers to Upvalue objects all the time,
    // even in the Closure::upvalues vector. That means that the closures track the upvalues they use "for free", whether they are
    // - On the stack (since an Upvalue is "just" a pointer to a value owned by VM::stack), or
    // - On the heap (since the Upvalue internally has a storage to keep the new value when it's closed.)
    //
    // Ownership in Rust doesn't really allow leaking a reference to VM::stack directly inside a Closure, and
    // REMINDER: we are on the compiler side here.
    //
    // So I would really like to find a way to:
    // - Add a CompileTime upvalue to a closure, that would get
    //   expanded to a RunTime upvalue enum variant pointing to the
    //   local slot (UpvalueOnStack<'stack> { index: usize }).
    // - Have a RunTime upvalue enum variant that points to
    //   non local slot (UpvalueOnStack<'stack> again?? with a lower index?)
    // - Have a RunTime upvalue enum that lives on the heap
    //
    // TBH, it seems quite complex to have the "upvalue on Stack" optimized
    // part now (mostly because no bare pointers == how am I supposed to know
    // when a reference to a stack slot gets manipulated??). So I think I'll
    // just make upvalues always live on the heap and share mutability through
    // Gc<GcCell<>>.
    //
    // That means I need an OpCode that will transform a local variable to
    // an upvalue on the heap once we detect that a value has been captured,
    // so that the `current_compiler.target.upvalues` can be appended with
    // a fresh reference to the upvalue at runtime.
    current_compiler
        .borrow_mut()
        .upvalues
        .push(CompileTimeUpvalue {
            is_local,
            index: parent_local_index,
        });
    current_compiler.borrow().target.borrow_mut().upvalue_count += 1;
    Ok(current_compiler.borrow().upvalues.len() - 1)
}
