// SPDX-FileCopyrightText: 2023 Gerry Agbobada <git@gagbo.net>
//
// SPDX-License-Identifier: LGPL-3.0-only

use crate::{Error, Result, Value};
use parser::Number;

pub fn add(args: Vec<Value>) -> Result<Vec<Value>> {
    // Beginning of the part that could be macro-generated ?
    // Note that there is special handling of Nil here.
    let list = &args[0];
    let values = match list {
        Value::Pair(p) => p.clone(),
        Value::Nil => return Ok(vec![Value::Number(Number::exact_int(0))]),
        _ => unreachable!(
            "The arity of the native function is ListVar, got {:?} instead",
            list
        ),
    };
    // End of the part that could be macro-generated ?

    Ok(vec![Value::Number(values.into_iter().try_fold(
        Number::exact_int(0),
        |acc, val| match &*val.borrow() {
            Value::Number(n) => Ok(acc + *n),
            Value::Upvalue(up) => {
                if let Value::Number(n) = &*up.location.borrow() {
                    Ok(acc + *n)
                } else {
                    Err(Error::Unknown("Expected number".to_string()))
                }
            }
            _ => Err(Error::Unknown("Expected number".to_string())),
        },
    )?)])
}

pub fn sub(args: Vec<Value>) -> Result<Vec<Value>> {
    // Beginning of the part that could be macro-generated ?
    // Note that there is special handling of Nil here.
    let list = &args[0];
    let values = match list {
        Value::Pair(p) => p.clone(),
        Value::Nil => return Ok(vec![Value::Number(Number::exact_int(0))]),
        _ => unreachable!(
            "The arity of the native function is ListVar, got {:?} instead",
            list
        ),
    };
    // End of the part that could be macro-generated ?

    let mut iter = values.into_iter().peekable();
    let first = iter.next().unwrap();

    let init = match &*first.borrow() {
        Value::Number(n) => Ok(*n),
        Value::Upvalue(up) => {
            if let Value::Number(n) = &*up.location.borrow() {
                Ok(*n)
            } else {
                Err(Error::Unknown("Expected number".to_string()))
            }
        }
        _ => Err(Error::Unknown("Expected number".to_string())),
    }?;

    if iter.peek().is_none() {
        return Ok(vec![Value::Number(Number::exact_int(-1) * init)]);
    }

    Ok(vec![Value::Number(iter.try_fold(
        init,
        |acc, val| match &*val.borrow() {
            Value::Number(n) => Ok(acc - *n),
            Value::Upvalue(up) => {
                if let Value::Number(n) = &*up.location.borrow() {
                    Ok(acc - *n)
                } else {
                    Err(Error::Unknown("Expected number".to_string()))
                }
            }
            _ => Err(Error::Unknown("Expected number".to_string())),
        },
    )?)])
}

pub fn eq(args: Vec<Value>) -> Result<Vec<Value>> {
    // Beginning of the part that could be macro-generated ?
    // Note that there is special handling of Nil here.
    let list = &args[0];
    let values = match list {
        Value::Pair(p) => p.clone(),
        Value::Nil => return Ok(vec![Value::Boolean(true)]),
        _ => unreachable!(
            "The arity of the native function is ListVar, got {:?} instead",
            list
        ),
    };
    // End of the part that could be macro-generated ?

    let init_val = values.car.clone();

    for val in values.into_iter().skip(1) {
        if !init_val.eq(&val) {
            return Ok(vec![Value::Boolean(false)]);
        }
    }

    Ok(vec![Value::Boolean(true)])
}

pub fn clock(_: Vec<Value>) -> Result<Vec<Value>> {
    Ok(vec![Value::Number(Number::inexact_real(
        std::time::SystemTime::now()
            .duration_since(std::time::SystemTime::UNIX_EPOCH)
            .unwrap()
            .as_secs_f64(),
    ))])
}
