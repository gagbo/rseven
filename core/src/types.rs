// SPDX-FileCopyrightText: 2022-2023 Gerry Agbobada <git@gagbo.net>
//
// SPDX-License-Identifier: LGPL-3.0-only

//! R7RS Scheme types
//!
//! # Extract of the specification
//! No object satisfies more than one of the following predicates:
//! - boolean?
//! - bytevector?
//! - char?
//! - eof-object?
//! - null?
//! - number?
//! - pair?
//! - port?
//! - procedure?
//! - string?
//! - symbol?
//! - vector?, and
//! - all predicates created by define-record-type.
//!
//! These predicates define the types
//! - boolean,
//! - bytevector,
//! - character,
//! - the empty list object,
//! - eof-object,
//! - number,
//! - pair,
//! - port,
//! - procedure,
//! - string,
//! - symbol,
//! - vector, and
//! - all record types.

mod continuation;
mod error;
mod pair;
mod ports;
mod procedure;
mod record;

use gc::{Finalize, Gc, GcCell, Trace};
use itertools::Itertools;
use parser::{ast::Literal, Number};

pub use continuation::*;
pub use error::*;
pub use pair::*;
pub use ports::*;
pub use procedure::*;
pub use record::*;

#[derive(Trace, Finalize, Debug, Clone)]
pub enum Value {
    Boolean(bool),
    Bytevector(Vec<u8>),
    Character(char),
    Nil,
    Eof,
    // SAFETY
    // It is safe to ignore trace for the number literal,
    // as it only contains empty enum variants, bare integers,
    // and bare floats
    Number(#[unsafe_ignore_trace] Number),
    Pair(Pair),
    Port(Port),
    Continuation(Continuation),
    String(String),
    Symbol(String),
    Vector(Vec<Value>),
    // Non primitives -ish start here
    CallFrame(Heap<CallFrame>),
    Closure(Heap<Closure>),
    SchemeBareProc(Heap<BytecodeProc>),
    // SAFETY
    // It is safe to ignore trace for native procedures,
    // They don't hold garbage-collected data.
    // Maybe it will need change when Closures are added??
    NativeProc(#[unsafe_ignore_trace] NativeProc),
    Upvalue(Upvalue),
}

#[derive(Trace, Finalize, Debug, Clone)]
pub struct Upvalue {
    pub(crate) location: Heap<Value>,
}

pub type Heap<T> = Gc<GcCell<T>>;
pub type HeapValue = Heap<Value>;

impl From<Value> for HeapValue {
    fn from(value: Value) -> Self {
        Gc::new(GcCell::new(value))
    }
}

impl From<&Value> for HeapValue {
    fn from(value: &Value) -> Self {
        Gc::new(GcCell::new(value.clone()))
    }
}

impl std::fmt::Display for Value {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        match self {
            Value::Boolean(true) => write!(f, "#true"),
            Value::Boolean(false) => write!(f, "#false"),
            Value::Bytevector(vals) => {
                write!(f, "#u8(")?;
                write!(
                    f,
                    "{}",
                    Itertools::intersperse(vals.iter().map(ToString::to_string), " ".to_string())
                        .collect::<String>()
                )?;
                write!(f, ")")
            }
            Value::Character(c) => write!(f, "#\\{c}"),
            Value::Nil => write!(f, "()"),
            Value::Eof => todo!("display EOF"),
            Value::Number(num) => write!(f, "{num}"),
            Value::Pair(pair) => {
                write!(f, "(")?;
                write!(f, "{}", pair.car.borrow())?;
                if pair.cdr.borrow().nullp() {
                    write!(f, ")")
                } else {
                    write!(f, " . ")?;
                    write!(f, "{}", pair.cdr.borrow())?;
                    write!(f, ")")
                }
            }
            Value::Port(_) => todo!("display port"),
            Value::Continuation(_) => todo!("display continuation?"),
            Value::String(s) => write!(f, "{s:?}"),
            Value::Symbol(s) => write!(f, "{s}"),
            Value::Vector(v) => {
                write!(f, "[")?;
                write!(
                    f,
                    "{}",
                    Itertools::intersperse(v.iter().map(ToString::to_string), " ".to_string())
                        .collect::<String>()
                )?;
                write!(f, "]")
            }
            Value::CallFrame(_) => write!(f, "#<call frame>"),
            Value::Closure(p) => write!(f, "{}", p.borrow()),
            Value::SchemeBareProc(p) => write!(f, "{}", p.borrow()),
            Value::NativeProc(n) => write!(f, "#<native procedure {}>", n.name),
            Value::Upvalue(v) => write!(f, "{}", v.location.borrow()),
        }
    }
}

macro_rules! impl_value_ref_downcast {
    ($type:ty, $slot_type:path, $typename:ident, $hr_type:literal) => {
        paste::paste! {
            #[automatically_derived]
            impl Value{
                #[allow(dead_code)]
                pub(crate) fn [<as_ $typename _ref_mut_and_then>]<T, F> (
                    &mut self,
                    f: F
                ) -> std::result::Result<T, crate::Error>
            where
                F: FnOnce(&mut $type) -> std::result::Result<T, crate::Error>
                 {
                    match self {
                        crate::Value::$slot_type(ref mut inner_slot) => f(inner_slot),
                        crate::Value::Upvalue(ref upvalue) => {
                            if let crate::Value::$slot_type(ref mut inner_slot) = &mut *upvalue.location.borrow_mut() {
                                f(inner_slot)
                            } else {
                                Err(crate::types::Error::WrongType(
                                    $hr_type.into(),
                                    format!("{self:?}"),
                                ))
                            }
                        }
                        _ =>
                            Err(crate::types::Error::WrongType(
                            $hr_type.into(),
                            format!("{self:?}"),
                        )),
                    }
                }

                #[allow(dead_code)]
                pub(crate) fn [<as_ $typename _ref_and_then>]<T, F> (
                    &self,
                    f: F
                ) -> std::result::Result<T, crate::Error>
            where
                F: FnOnce(&$type) -> std::result::Result<T, crate::Error>
                 {
                    match &self {
                        crate::Value::$slot_type(ref inner_slot) => f(inner_slot),
                        crate::Value::Upvalue(ref upvalue) => {
                            if let crate::Value::$slot_type(ref inner_slot) = &*upvalue.location.borrow() {
                                f(inner_slot)
                            } else {

                            Err(crate::Error::WrongType(
                            $hr_type.into(),
                            format!("{self:?}"),
                        ))
                            }
                        }
                        _ =>
                            Err(crate::Error::WrongType(
                            $hr_type.into(),
                            format!("{self:?}"),
                        )),
                    }
                }
            }
        }
    };
}

// macro_rules! impl_value_downcast {
//     ($type:ty, $slot_type:path, $typename:literal) => {
//         impl std::convert::TryFrom<crate::types::Value> for $type {
//             type Error = crate::Error;

//             fn try_from(value: crate::Value) -> Result<Self, Self::Error> {
//                 match value {
//                     crate::types::Value::Slot(ref slot_content) => match &slot_content {
//                         $slot_type(inner_slot) => Ok(inner_slot.clone()),
//                         _ => Err(crate::Error::WrongType($typename.into(), value.clone())),
//                     },
//                     _ => Err(crate::Error::WrongType($typename.into(), value)),
//                 }
//             }
//         }
//     };
// }

impl_value_ref_downcast!(bool, Boolean, bool, "boolean");
impl_value_ref_downcast!(Vec<u8>, Bytevector, bytevector, "bytevector");
impl_value_ref_downcast!(char, Character, char, "character");
impl_value_ref_downcast!(Number, Number, number, "number");
impl_value_ref_downcast!(String, String, string, "string");
impl_value_ref_downcast!(Vec<Value>, Vector, vector, "vector");
impl_value_ref_downcast!(Pair, Pair, pair, "pair");

impl Value {
    // TODO: Add the downcast utility functions

    pub fn booleanp(&self) -> bool {
        matches!(self, Self::Boolean(_))
            || matches!(self, Self::Upvalue(u) if u.location.borrow().booleanp())
    }
    pub fn bytevectorp(&self) -> bool {
        matches!(self, Self::Bytevector(_))
            || matches!(self, Self::Upvalue(u) if u.location.borrow().bytevectorp())
    }
    pub fn charp(&self) -> bool {
        matches!(self, Self::Character(_))
            || matches!(self, Self::Upvalue(u) if u.location.borrow().charp())
    }
    pub fn eof_objectp(&self) -> bool {
        matches!(self, Self::Eof)
            || matches!(self, Self::Upvalue(u) if u.location.borrow().eof_objectp())
    }
    pub fn nullp(&self) -> bool {
        matches!(self, Self::Nil) || matches!(self, Self::Upvalue(u) if u.location.borrow().nullp())
    }
    pub fn numberp(&self) -> bool {
        matches!(self, Self::Number(_))
            || matches!(self, Self::Upvalue(u) if u.location.borrow().numberp())
    }
    pub fn pairp(&self) -> bool {
        matches!(self, Self::Pair(_))
            || matches!(self, Self::Upvalue(u) if u.location.borrow().pairp())
    }
    pub fn portp(&self) -> bool {
        matches!(self, Self::Port(_))
            || matches!(self, Self::Upvalue(u) if u.location.borrow().portp())
    }
    pub fn procedurep(&self) -> bool {
        matches!(
            self,
            Self::Closure(_)
                | Self::SchemeBareProc(_)
                | Self::NativeProc(_)
                | Self::Continuation(_)
        ) || matches!(self, Self::Upvalue(u) if u.location.borrow().procedurep())
    }
    pub fn stringp(&self) -> bool {
        matches!(self, Self::String(_))
            || matches!(self, Self::Upvalue(u) if u.location.borrow().stringp())
    }
    pub fn symbolp(&self) -> bool {
        matches!(self, Self::Symbol(_))
            || matches!(self, Self::Upvalue(u) if u.location.borrow().symbolp())
    }
    pub fn vectorp(&self) -> bool {
        matches!(self, Self::Vector(_))
            || matches!(self, Self::Upvalue(u) if u.location.borrow().vectorp())
    }
    pub fn recordp(&self) -> bool {
        false
    }

    /// Return true if the values are exactly the same for
    /// constant compilation compacting.
    pub(crate) fn ptr_eq(&self, other: &Self) -> bool {
        match (self, other) {
            (Value::Boolean(s), Value::Boolean(o)) => s == o,
            (Value::Bytevector(s), Value::Bytevector(o)) => s == o,
            (Value::Character(s), Value::Character(o)) => s == o,
            (Value::Nil, Value::Nil) => true,
            (Value::Eof, Value::Eof) => true,
            (Value::Number(s), Value::Number(o)) => s == o,
            (Value::Pair(s), Value::Pair(o)) => {
                Gc::ptr_eq(&s.car, &o.car) && Gc::ptr_eq(&s.cdr, &o.cdr)
            }
            (Value::Port(s), Value::Port(o)) => todo!(),
            (Value::Closure(s), Value::Closure(o)) => Gc::ptr_eq(s, o),
            (Value::NativeProc(n), Value::NativeProc(o)) => todo!(),
            (Value::Continuation(s), Value::Continuation(o)) => todo!(),
            (Value::String(s), Value::String(o)) => s == o,
            (Value::Symbol(s), Value::Symbol(o)) => s == o,
            (Value::Vector(s), Value::Vector(o)) => {
                s.len() == o.len() && s.iter().zip(o.iter()).all(|(s, o)| s.ptr_eq(o))
            }
            (Value::Upvalue(s), Value::Upvalue(o)) => Gc::ptr_eq(&s.location, &o.location),
            _ => false,
        }
    }

    pub(crate) fn is_truthy(&self) -> bool {
        !(matches!(self, Value::Boolean(false))
            || matches!(self, Value::Upvalue(u) if !u.location.borrow().is_truthy()))
    }
}

impl From<Literal> for Value {
    fn from(value: Literal) -> Self {
        match value {
            Literal::Bool(b) => Self::Boolean(b),
            Literal::Num(n) => Self::Number(n),
            Literal::Vec(v) => Self::Vector(v.into_iter().map(Into::into).collect()),
            Literal::Char(c) => Self::Character(c),
            Literal::StringLiteral(s) => Self::String(s),
            Literal::Bytevector(bv) => Self::Bytevector(bv),
            Literal::Compound(_) => todo!(),
            /* NOTE: This is a label assignment in literal, looking like
             * tree> '#0=(a b c . #0#)
             * Expression(
             *     Literal(Assignment {
             *         label: 0,
             *         datum: Compound(List {
             *             head: [Symbol("a"), Symbol("b"), Symbol("c")],
             *             tail: Some(Label(0)),
             *         }),
             *     }),
             * )
             * */
            Literal::Assignment { label, datum } => todo!(),
            Literal::Label(_) => todo!(),
            Literal::Symbol(s) => Self::Symbol(s),
        }
    }
}

impl PartialEq for Value {
    fn eq(&self, other: &Self) -> bool {
        match (self, other) {
            (Self::Boolean(l0), Self::Boolean(r0)) => l0 == r0,
            (Self::Bytevector(l0), Self::Bytevector(r0)) => l0 == r0,
            (Self::Character(l0), Self::Character(r0)) => l0 == r0,
            (Self::Number(l0), Self::Number(r0)) => l0 == r0,
            (Self::Pair(l0), Self::Pair(r0)) => todo!("Pair equality"),
            (Self::Port(l0), Self::Port(r0)) => todo!("Port equality"),
            (Self::Closure(s), Self::Closure(o)) => todo!(),
            (Self::NativeProc(_), Self::NativeProc(_)) => todo!(),
            (Self::Continuation(l0), Self::Continuation(r0)) => todo!("Continuation equality"),
            (Self::String(l0), Self::String(r0)) => l0 == r0,
            (Self::Symbol(l0), Self::Symbol(r0)) => l0 == r0,
            (Self::Vector(l0), Self::Vector(r0)) => l0 == r0,
            (Self::Upvalue(l0), other) => l0.location.borrow().eq(other),
            (_, Self::Upvalue(r0)) => self.eq(&r0.location.borrow()),
            _ => false,
        }
    }
}
