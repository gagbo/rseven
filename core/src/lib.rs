// SPDX-FileCopyrightText: 2022-2023 Gerry Agbobada <git@gagbo.net>
//
// SPDX-License-Identifier: LGPL-3.0-only

pub mod chunk;
pub mod compiler;
pub mod stdlib;
pub mod types;
pub mod vm;

pub use chunk::*;
pub use compiler::*;
pub use types::*;
pub use vm::*;

#[cfg(test)]
mod end_to_end {
    use parser::Number;

    use super::*;

    fn assert_eval(primer: Vec<&str>, scheme: &str, expected: Vec<Value>) {
        let mut vm = VM::new();
        for statement in &primer {
            vm.interpret(statement).unwrap();
        }
        let val = vm.interpret(scheme);
        assert_eq!(val.unwrap(), expected, "\nEvaluated\n{primer:?}\n{scheme}");
    }

    #[test]
    fn bools() {
        assert_eval(vec![], "#t", vec![Value::Boolean(true)]);
        assert_eval(vec![], "#f", vec![Value::Boolean(false)]);
        assert_eval(vec![], "#false", vec![Value::Boolean(false)]);
        assert_eval(vec![], "#true", vec![Value::Boolean(true)]);
    }

    #[test]
    fn and() {
        assert_eval(vec![], "(and)", vec![Value::Boolean(true)]);
        assert_eval(vec![], "(and #f 0)", vec![Value::Boolean(false)]);
        assert_eval(
            vec![],
            "(and #t 0)",
            vec![Value::Number(Number::exact_int(0))],
        );
    }

    #[test]
    fn or() {
        assert_eval(vec![], "(or)", vec![Value::Boolean(false)]);
        assert_eval(
            vec![],
            "(or #f 0)",
            vec![Value::Number(Number::exact_int(0))],
        );
        assert_eval(
            vec![],
            "(or 0 #t)",
            vec![Value::Number(Number::exact_int(0))],
        );
    }

    #[test]
    fn ident() {
        assert_eval(
            vec!["(define foo 123+4i)"],
            "foo",
            vec![Value::Number(Number::exact_complex((123, 1), (4, 1)))],
        );
    }

    #[test]
    fn if_conditional() {
        assert_eval(
            vec!["(define foo #t)"],
            "(if foo 'true 'false)",
            vec![Value::Symbol("true".to_string())],
        );
        assert_eval(
            vec!["(define foo #f)"],
            "(if foo 'true 'false)",
            vec![Value::Symbol("false".to_string())],
        );
        assert_eval(
            vec!["(define foo #f)"],
            "(if foo 'true)",
            vec![Value::Boolean(false)],
        );
    }

    #[test]
    fn when_conditional() {
        assert_eval(
            vec!["(define foo #t)"],
            "(when foo 'true)",
            vec![Value::Symbol("true".to_string())],
        );
        assert_eval(
            vec!["(define foo #f)"],
            "(when foo 'true)",
            vec![Value::Boolean(false)],
        );
    }

    #[test]
    fn unless_conditional() {
        assert_eval(
            vec!["(define foo #t)"],
            "(unless foo 'true)",
            vec![Value::Boolean(false)],
        );
        assert_eval(
            vec!["(define foo #f)"],
            "(unless foo 'true)",
            vec![Value::Symbol("true".to_string())],
        );
    }

    #[test]
    fn closure_capture() {
        assert_eval(
            vec!["(define (account init) (let ((balance init)) (lambda (amount) (- balance amount))))",
            "(define actual-account (account 12))"],
            "(actual-account 2)",
            vec![Value::Number(Number::exact_int(10))],
        )
    }

    #[test]
    fn closure_capture_sibling() {
        assert_eval(
            vec![
                "
(define (account init)
  (let* ((current-balance init))
    (define (withdraw amount) (set! current-balance (- current-balance amount)))
    (define (balance) current-balance)
    (lambda (message) (if (eq? message 'balance) (balance) (withdraw message)))))",
                "(define actual-account (account 12))",
                "(actual-account 4)",
            ],
            "(actual-account 'balance)",
            vec![Value::Number(Number::exact_int(8))],
        )
    }

    #[test]
    fn cond() {
        assert_eval(
            Vec::new(),
            "(cond (#f +i) (#t 'hello))",
            vec![Value::Symbol("hello".to_string())],
        );
        assert_eval(
            Vec::new(),
            "(cond (#f +i) (#f 'hello) (else 'goodbye))",
            vec![Value::Symbol("goodbye".to_string())],
        );
        assert_eval(
            Vec::new(),
            "(cond (#f +i) (1 => -))",
            vec![Value::Number(Number::exact_int(-1))],
        );
        assert_eval(Vec::new(), "(cond (#f +i))", vec![Value::Boolean(false)]);
    }
}
