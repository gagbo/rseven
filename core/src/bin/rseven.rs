// SPDX-FileCopyrightText: 2022-2023 Gerry Agbobada <git@gagbo.net>
//
// SPDX-License-Identifier: LGPL-3.0-only

use anyhow::bail;
use parser::parse;
use rustyline::{error::ReadlineError, DefaultEditor};
use std::{path::PathBuf, result::Result};
use tracing::debug;
use tracing_subscriber::EnvFilter;

fn main() -> Result<(), anyhow::Error> {
    let subscriber = tracing_subscriber::fmt()
        .with_env_filter(EnvFilter::from_default_env())
        .pretty()
        .finish();
    tracing::subscriber::set_global_default(subscriber).expect("setting tracing default failed");

    let args: Vec<_> = std::env::args().collect();
    if args.len() == 1 {
        repl()
    } else if args.len() == 2 {
        run_file(args[1].clone().into())
    } else {
        bail!("rseven takes 0 or 1 argument.")
    }
}

fn repl() -> Result<(), anyhow::Error> {
    let mut rl = DefaultEditor::new()?;
    if rl.load_history(".rseven-history").is_err() {
        debug!("No previous history.");
    }

    let mut vm = rseven_lib::VM::new();

    loop {
        let readline = rl.readline("user> ");
        match readline {
            Ok(line) => {
                rl.add_history_entry(&line)?;
                rl.save_history(".rseven-history").unwrap();
                match vm.interpret(&line) {
                    Ok(val) => {
                        println!("Result:");
                        for (i, res) in val.iter().enumerate() {
                            println!("\t${i}: {res}");
                        }
                    }
                    Err(err) => {
                        println!("Error: {err}");
                        vm.reset_stack();
                    }
                };
            }
            Err(ReadlineError::Interrupted) => continue,
            Err(ReadlineError::Eof) => break Ok(()),
            Err(err) => {
                println!("Error: {err:?}");
                break Err(err.into());
            }
        }
    }
}

fn run_file(path: PathBuf) -> Result<(), anyhow::Error> {
    let source = std::fs::read_to_string(path)?;
    let mut vm = rseven_lib::VM::new();
    let result = vm.interpret(&source)?;
    println!("Result:");
    for (i, res) in result.iter().enumerate() {
        println!("\t${i}: {res}");
    }
    Ok(())
}
