; SPDX-FileCopyrightText: 2022-2023 Gerry Agbobada <git@gagbo.net>
;
; SPDX-License-Identifier: LGPL-3.0-only

;; Example of getting the length of decimal repr of
;; a fibonacci number
(define (fib n)
  (let loop ((n n) (a 1) (b 1))
    (cond ((= n 0) a)
          ((= n 1) b)
          (else
           (loop (1- n) b (+ a b))))))

(define (main args)
  (let* ((n (string->number (cadr args)))
         (r (fib n)))
    (format #t "~s\n" (string-length (number->string r)))))
