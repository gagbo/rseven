; SPDX-FileCopyrightText: 2022-2023 Gerry Agbobada <git@gagbo.net>
;
; SPDX-License-Identifier: LGPL-3.0-only

;; Testing thunk storage/evaluation
(define evaled 2)
(define (add2 value) (+ value evaled))
(define before-change (add2 5))         ; should be 7
(define evaled 129)
(define after-change (add2 5))          ; should STILL be 7
(eq? before-change after-change)        ; should be #t
