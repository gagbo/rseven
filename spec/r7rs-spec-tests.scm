; SPDX-FileCopyrightText: 2022-2023 Gerry Agbobada <git@gagbo.net>
;
; SPDX-License-Identifier: CC0-1.0

(eq? (read "( 08 13 )") (read "(8 . (13 . ()))"))
;;;
(define x 28)
x
;; 28

;;;
(quote a)
;; a
(quote #(a b c))
;; #(a b c)
(quote (+ 1 2))
;; (+ 1 2)
;;;
'(quote a)
;; (quote a)
''a
;; (quote a)

;;;
'145932
;; 145932
145932
;; 145932
'"abc"
;; "abc"
"abc"
;; "abc"
'#
;; #
#
;; #
'#(a 10)
;; #(a 10)
#(a 10)
;; #(a 10)
'#u8(64 65)
;; #u8(64 65)
#u8(64 65)
;; #u8(64 65)
'#t
;; #t
#t
;; #t

;;;
(+ 3 4)
;; 7
((if #f + *) 3 4)
;; 12
(lambda (x) (+ x x))
;; a procedure ?
((lambda (x) (+ x x)) 4)
;; 8
(define reverse-substract
  (lambda (x y) (- x y)))
(reverse-substract 7 10)
;; 3
(define add4
  (let ((x 4))
    (lambda (y) (+ x y))))
(add4 6)
;; 10

;;;
((lambda x x) 3 4 5 6)
;; (3 4 5 6)
((lambda (x y . z) z)
 3 4 5 6)
;; (5 6)

;;;
(if (> 3 2) 'yes 'no)
;; yes
(if (< 3 2) 'yes 'no)
;; no
(if (> 3 2)
    (- 3 2)
    (+ 3 2))
;; 1

;;;
(define x 2)
(+ x 1) ; 3
(set! x 4) ; unspecified
(+ x 1)
;; 5
  
