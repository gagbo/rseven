// SPDX-FileCopyrightText: 2022-2023 Gerry Agbobada <git@gagbo.net>
//
// SPDX-License-Identifier: LGPL-3.0-only

mod data;
mod expression;
mod libraries;
mod number;
mod program_definitions;
mod quasiquotations;
mod transformers;

pub use data::*;
use dbg_pls::DebugPls;
pub use expression::*;
pub use libraries::*;
pub use number::*;
pub use program_definitions::*;
pub use quasiquotations::*;
pub use transformers::*;

pub type Identifier = String;
pub type StringLiteral = String;

/// Alias for the root of the AST,
/// used as the target of parsing the top rule in r7rs.pest
///
/// r7rs.pest currently has 'r7rs' as the top level rule.
#[derive(Clone, Debug, DebugPls, PartialEq)]
pub struct Ast<'code> {
    pub source: &'code str,
    pub node: AstNode,
}

/// Node of an Abstract Syntax Tree (AST), which must be compatible with a root
/// of the AST
#[derive(Clone, Debug, DebugPls, PartialEq)]
pub enum AstNode {
    Nil,
    /// An expression.
    /// Expression being the top level of the AST is useful for
    /// the REPL use case
    Expression(Expression),
    /// A definition.
    /// Definition being the top level of the AST is useful for
    /// the REPL use case
    Definition(Definition),
    Program(Program),
    Library(Library),
}

#[derive(Clone, Debug, DebugPls, PartialEq)]
pub struct Sequence {
    pub commands: Vec<Expression>,
    pub tail_expression: Box<Expression>,
}

impl From<Sequence> for Literal {
    fn from(sequence: Sequence) -> Self {
        let mut head = Vec::new();
        head.extend(sequence.commands.into_iter().map(Into::into));
        head.push((*sequence.tail_expression).into());
        Self::Compound(CompoundDatum::List { head, tail: None })
    }
}
