// SPDX-FileCopyrightText: 2022-2023 Gerry Agbobada <git@gagbo.net>
//
// SPDX-License-Identifier: LGPL-3.0-only

//! Parser of R7RS grammar

#[cfg(test)]
mod tests;

pub(crate) use pest::iterators::Pair;
use pest::Parser as _;
use pest_derive::Parser;

use crate::error::{LexicalError, Result};

#[cfg(debug_assertions)]
const _GRAMMAR: &str = include_str!("r7rs.pest");

#[derive(Parser)]
#[grammar = "r7rs.pest"]
pub struct R7Parser;

pub fn lex(content: &str) -> Result<(Pair<Rule>, &str)> {
    let span = tracing::span!(tracing::Level::TRACE, "lex");
    let _enter = span.enter();
    tracing::event!(tracing::Level::INFO, length = content.len());
    let parsed = R7Parser::parse(Rule::r7rs, content)
        .map_err(|err| LexicalError::Generic {
            message: err.to_string(),
        })?
        .next();
    if let Some(ast) = parsed {
        tracing::event!(tracing::Level::INFO, "successful parse");
        Ok((ast, content))
    } else {
        tracing::event!(tracing::Level::INFO, "failed parse");
        Err(LexicalError::Generic {
            message: "Empty AST".into(),
        })
    }
}

#[cfg(feature = "ascii_tree")]
pub fn print_tree(content: &str) -> Result<()> {
    let parsed = lex(content)?;
    println!(
        "{}",
        pest_ascii_tree::into_ascii_tree(parsed.into_inner()).unwrap()
    );
    Ok(())
}
