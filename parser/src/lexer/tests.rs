// SPDX-FileCopyrightText: 2022-2023 Gerry Agbobada <git@gagbo.net>
//
// SPDX-License-Identifier: LGPL-3.0-only

use super::*;
use pest_ascii_tree::print_ascii_tree;
use pest_test_gen::pest_tests;
use std::fs;

#[pest_tests(
    crate::lexer::R7Parser,
    crate::lexer::Rule,
    "r7rs",
    recursive = true,
    lazy_static = true
)]
#[cfg(test)]
mod corpus {}

fn print_tree(ast: Pair<Rule>) {
    print_ascii_tree(Ok(ast.into_inner()));
}

// Just a small test to see what the parser sees.
// Run with `cargo test repl -- --nocapture`
#[test]
fn repl() {
    let scheme_str = "(car '(c d))";
    let parsed = lex(scheme_str).unwrap();
    println!("Input:\n{scheme_str}\nTree:");
    print_tree(parsed.0);
}

// This test only checks that no parsing error occured
#[test]
fn parse_base() {
    let base = fs::read_to_string("../lib/scheme/base.sld").expect("cannot read file");
    let parsed = lex(&base).unwrap();
    eprintln!("Start");
    print_tree(parsed.0);
}

#[test]
fn parse_comment_only() {
    let parsed = lex(";;hello");
    assert!(parsed.is_err());
}
