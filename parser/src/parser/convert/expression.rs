// SPDX-FileCopyrightText: 2023 Gerry Agbobada <git@gagbo.net>
//
// SPDX-License-Identifier: LGPL-3.0-only

use std::collections::VecDeque;

use crate::{
    ast::{
        AndExpr, Assignment, CaseWithRecipientFallback, CaseWithSequenceFallback, CompoundDatum,
        CondExpr, DoLoopExpr, Expression, GuardExpr, Identifier, IfExpr, LambdaExpr, LetExpr,
        LetRecExpr, LetValuesExpr, Literal, NamedLetExpr, Number, OrExpr, ParameterizeExpr,
        ProcedureCall, Sequence, StringLiteral, UnlessExpr, WhenExpr,
    },
    error::{LexicalError, Result},
    lexer::Rule,
    parser::convert::{
        parse_binding_spec, parse_body, parse_case_clause, parse_case_lambda_clause,
        parse_cond_clause, parse_formals, parse_includer, parse_iteration_spec,
        parse_mv_binding_spec, parse_quasiquotation, unwrap_inner,
    },
};
use itertools::Itertools;
use pest::iterators::Pair;

use super::number::convert_number_literal;

#[tracing::instrument(skip(ast))]
pub(super) fn parse_expression(ast: Pair<Rule>) -> Result<Expression> {
    // Command rule wraps expression transparently
    // command = _{ expression }
    // in Pest grammar
    // All the other rules mentioned here wrap expression transparently as well
    debug_assert!(matches!(
        ast.as_rule(),
        Rule::expression
            | Rule::command
            | Rule::test
            | Rule::consequent
            | Rule::alternate
            | Rule::recipient
            | Rule::init
            | Rule::step
            | Rule::qq_template_0
            | Rule::operator
            | Rule::operand
    ));

    let inner = ast
        .into_inner()
        .next()
        .expect("'expression' rule has exactly 1 child");

    match inner.as_rule() {
        Rule::identifier => Ok(Expression::Ident(parse_identifier(inner)?)),
        Rule::literal => Ok(Expression::Literal(parse_literal(inner)?)),
        Rule::procedure_call => parse_procedure_call(inner),
        Rule::lambda_expression => parse_lambda_expr(inner),
        // TODO(#A0): Finish parsing expressions
        Rule::conditional => parse_conditional(inner),
        Rule::assignment => {
            let mut inner_iter = inner.into_inner();
            let ident = parse_identifier(
                inner_iter
                    .next()
                    .expect("r7rs.pest ensures an assigment has the identifier first inside."),
            )?;
            let value =
                Box::new(parse_expression(inner_iter.next().expect(
                    "r7rs.pest ensures an assigment has the expression second inside.",
                ))?);
            Ok(Expression::Assignment(Assignment { ident, value }))
        }
        Rule::derived_expression => parse_derived_expression(inner),
        Rule::macro_use => {
            todo!("macro_use")
        }
        Rule::macro_block => {
            todo!("macro_block")
        }
        Rule::includer => Ok(Expression::Includer(parse_includer(inner)?)),
        _ => {
            unreachable!("Pest won't nest any other rule in expression according to r7rs.pest.")
        }
    }
}

#[tracing::instrument(skip(ast))]
pub(super) fn parse_conditional(ast: Pair<Rule>) -> Result<Expression> {
    debug_assert!(matches!(ast.as_rule(), Rule::conditional));

    // Dropping the first element inside, which is
    // 'conditional_if'
    let mut iter = ast.into_inner().skip(1);

    let test = Box::new(parse_expression(
        iter.next()
            .expect("r7rs.pest ensures that the 'test' element is in second place."),
    )?);
    let consequent = Box::new(parse_expression(
        iter.next()
            .expect("r7rs.pest ensures that the 'consequent' element is in second place."),
    )?);
    let alternate = iter.next().map(parse_expression).transpose()?.map(Box::new);

    Ok(Expression::If(IfExpr {
        test,
        consequent,
        alternate,
    }))
}

#[tracing::instrument(skip(ast))]
pub(super) fn parse_identifier(ast: Pair<Rule>) -> Result<Identifier> {
    debug_assert!(matches!(
        ast.as_rule(),
        Rule::identifier
            | Rule::symbol
            | Rule::field_name
            | Rule::accessor
            | Rule::mutator
            | Rule::rest_identifier
            | Rule::keyword
    ));
    let res = ast.as_str().to_string();

    // Erroring on -i/+i
    if res == "-i" || res == "+i" {
        return Err(LexicalError::Generic {
            message: "-i or +i are not valid identifiers".to_string(),
        });
    }

    if res.starts_with('|') && res.ends_with('|') {
        let mut ident = String::with_capacity(res.len());
        let mut iter = res[1..res.len() - 1].chars();
        while let Some(char) = iter.next() {
            if char == '\\' {
                let next = iter.next().unwrap();
                match next {
                    '|' => ident.push('|'),
                    'a' => ident.push('\u{0007}'),
                    'b' => ident.push('\u{0008}'),
                    't' => ident.push('\t'),
                    'n' => ident.push('\n'),
                    'r' => ident.push('\r'),
                    'x' => {
                        let hex_char_value: String =
                            iter.by_ref().take_while(|c| *c != ';').collect();
                        let hex_char = u32::from_str_radix(&hex_char_value, 16).unwrap();
                        ident.push(char::from_u32(hex_char).unwrap());
                    }
                    _ => {
                        return Err(LexicalError::Generic {
                            message: format!("unexpected escape char: {next}"),
                        });
                    }
                }
            } else {
                ident.push(char);
            }
        }
        return Ok(ident);
    }

    Ok(res)
}

#[tracing::instrument(skip(ast))]
fn parse_literal(ast: Pair<Rule>) -> Result<Literal> {
    debug_assert!(matches!(ast.as_rule(), Rule::literal));
    let inner = ast
        .into_inner()
        .next()
        .expect("r7rs.pest guarantees an inner value to 'literal' rule");

    match inner.as_rule() {
        Rule::quotation => parse_datum(
            inner
                .into_inner()
                .next()
                .expect("r7rs.pest ensures that a quotation contains a single datum"),
        ),
        Rule::boolean => Ok(Literal::Bool(inner.as_str().starts_with("#t"))),
        Rule::number => Ok(Literal::Num(convert_number_literal(inner)?)),
        Rule::vector => Ok(Literal::Vec(
            inner
                .into_inner()
                .map(parse_datum)
                .collect::<Result<Vec<_>>>()?,
        )),
        Rule::character => parse_character(inner),
        Rule::string => Ok(Literal::StringLiteral(parse_string_literal(inner)?)),
        Rule::bytevector => parse_bytevector(inner),
        _ => unreachable!(),
    }
}

#[tracing::instrument(skip(ast))]
pub(super) fn parse_bytevector(ast: Pair<Rule>) -> Result<Literal> {
    debug_assert!(matches!(ast.as_rule(), Rule::bytevector));

    Ok(Literal::Bytevector(
        ast.into_inner().map(|byte| {
            // FIXME: eager computation for error message only
            let literal = byte.as_str().to_string();

            let value = convert_number_literal(byte)?;
            if let Number::Exact(num) = value {
                if !num.is_real() {
                    return Err(LexicalError::IllegalBytevector { literal, message: "A value in a bytevector must be real.".to_string() })
                }

                match num.real.int() {
                    Some(integer) => {
                        Ok(integer.try_into().map_err(|_|

                            LexicalError::IllegalBytevector { literal, message: "A value in a bytevector must be an integer between 0 and 255 inclusive.".to_string() }
                        )?)
                    }
                    None => {
                        Err(LexicalError::IllegalBytevector { literal, message: "A value in a bytevector must be an integer.".to_string() })
                    }
                }
            } else {
                Err(LexicalError::IllegalBytevector { literal, message: "A value in a bytevector must be an exact integer.".to_string() })
            }
        }).collect::<Result<Vec<_>>>()?
    ))
}

// TODO(#D): Measure perf of this function while working
// with big source code
#[tracing::instrument(skip(ast))]
pub(super) fn parse_string_literal(ast: Pair<Rule>) -> Result<StringLiteral> {
    debug_assert!(matches!(ast.as_rule(), Rule::string));
    Ok(ast.into_inner().flat_map(parse_string_element).collect())
}

#[tracing::instrument(skip(ast))]
pub(super) fn parse_string_element(ast: Pair<Rule>) -> Option<char> {
    debug_assert!(matches!(ast.as_rule(), Rule::string_element));
    let element = ast.as_str();

    if element == r#"\a"# {
        return Some('\u{0007}');
    }
    if element == r#"\b"# {
        return Some('\u{0008}');
    }
    if element.len() == 1 {
        return element.chars().next();
    }
    if element == r#"\r"# {
        return Some('\r');
    }
    if element == r#"\t"# {
        return Some('\t');
    }
    if element == r#"\n"# {
        return Some('\n');
    }

    if element == r#"\""# {
        return Some('"');
    }
    if element == r#"\\"# {
        return Some('\\');
    }
    if element == r#"\|"# {
        return Some('|');
    }

    if element.starts_with(r#"\x"#) && element.ends_with(';') {
        let hex_char = u32::from_str_radix(
            element
                .strip_prefix(r#"\x"#)
                .unwrap()
                .strip_suffix(';')
                .unwrap(),
            16,
        )
        .unwrap();
        return char::from_u32(hex_char);
    }

    // Otherwise, it's an escaped newline in the literal
    None
}

#[tracing::instrument(skip(ast))]
pub(super) fn parse_character(ast: Pair<Rule>) -> Result<Literal> {
    debug_assert!(matches!(ast.as_rule(), Rule::character));
    match ast.as_str() {
        raw_char if raw_char.starts_with("#\\") && raw_char.len() == 3 => {
            if raw_char == "#\\" {
                Ok(Literal::Char(' '))
            } else {
                Ok(Literal::Char(raw_char.chars().last().unwrap()))
            }
        }
        "#\\alarm" => Ok(Literal::Char('\u{0007}')),
        "#\\backspace" => Ok(Literal::Char('\u{0008}')),
        "#\\delete" => Ok(Literal::Char('\u{007F}')),
        "#\\escape" => Ok(Literal::Char('\u{001B}')),
        "#\\newline" => Ok(Literal::Char('\n')),
        "#\\null" => Ok(Literal::Char('\0')),
        "#\\return" => Ok(Literal::Char('\r')),
        "#\\space" => Ok(Literal::Char(' ')),
        "#\\tab" => Ok(Literal::Char('\t')),
        hex_char if hex_char.starts_with("#\\x") => {
            let hex_char = u32::from_str_radix(hex_char.strip_prefix("#\\x").unwrap(), 16).unwrap();
            Ok(Literal::Char(char::from_u32(hex_char).unwrap()))
        }
        // In practice this is unreachable with the current state of
        // r7rs.pest which will refuse to parse if an unknown char
        // name is used.
        illegal => Err(LexicalError::IllegalCharacter {
            literal: illegal.to_string(),
        }),
    }
}

#[tracing::instrument(skip(ast))]
pub(super) fn parse_sequence(ast: Pair<Rule>) -> Result<Sequence> {
    debug_assert!(matches!(ast.as_rule(), Rule::sequence));
    let mut commands = ast
        .into_inner()
        .map(|exp| {
            if exp.as_rule() != Rule::expression {
                Err(LexicalError::unexpected_rule(exp, &[Rule::expression]))
            } else {
                parse_expression(exp)
            }
        })
        .collect::<Result<Vec<Expression>>>()?;

    let tail_expression = Box::new(
        commands
            .pop()
            .expect("r7rs.pest guarantees at least one expression in a sequence rule"),
    );

    Ok(Sequence {
        commands,
        tail_expression,
    })
}

#[tracing::instrument(skip(ast))]
pub(super) fn parse_procedure_call(ast: Pair<Rule>) -> Result<Expression> {
    debug_assert!(matches!(ast.as_rule(), Rule::procedure_call));
    let mut iter = ast.into_inner();

    let operator = Box::new(parse_expression(iter.next().expect(
        "r7rs.pest ensures there's at least an operator in procedure_call rule",
    ))?);

    let operands = iter
        .map(parse_expression)
        .collect::<Result<VecDeque<Expression>>>()?;

    Ok(Expression::ProcedureCall(ProcedureCall {
        operator,
        operands,
    }))
}

#[tracing::instrument(skip(ast))]
pub(super) fn parse_lambda_expr(ast: Pair<Rule>) -> Result<Expression> {
    debug_assert!(matches!(ast.as_rule(), Rule::lambda_expression));
    let mut iter = ast.into_inner();

    let formals = parse_formals(
        iter.next()
            .expect("r7rs.pest ensures there are formals in lamdba_expression rule"),
    )?;

    let body = parse_body(
        iter.next()
            .expect("r7rs.pest ensures there is a body in lamdba_expression rule"),
    )?;

    Ok(Expression::Lambda(LambdaExpr { formals, body }))
}

#[tracing::instrument(skip(ast))]
pub(super) fn parse_datum(ast: Pair<Rule>) -> Result<Literal> {
    debug_assert!(matches!(ast.as_rule(), Rule::datum));
    let mut iter = ast.into_inner();
    let first = iter
        .next()
        .expect("r7rs.pest ensures that the 'datum' rule has a first element");

    match first.as_rule() {
        Rule::label => {
            // FIXME: eager computation for error message only
            let literal = first.as_str().to_string();

            let label = first
                .into_inner()
                .next()
                .expect("r7rs.pest ensures that a label rule has a unique uinteger_10 inside")
                .as_str()
                .parse()
                .map_err(|parse_err| LexicalError::IllegalNumber {
                    literal,
                    message: format!("expected a base10 integer, parse error: {parse_err}"),
                })?;

            if let Some(datum) = iter.next() {
                Ok(Literal::Assignment {
                    label,
                    datum: Box::new(parse_datum(datum)?),
                })
            } else {
                Ok(Literal::Label(label))
            }
        }
        // simple_datum handling begin
        Rule::boolean => Ok(Literal::Bool(first.as_str().starts_with("#t"))),
        Rule::number => Ok(Literal::Num(convert_number_literal(first)?)),
        Rule::character => parse_character(first),
        Rule::string => Ok(Literal::StringLiteral(parse_string_literal(first)?)),
        Rule::symbol => Ok(Literal::Symbol(parse_identifier(first)?)),
        Rule::bytevector => parse_bytevector(first),
        // simple_datum handling end
        Rule::list => {
            let (head, tail) =
                first
                    .into_inner()
                    .try_fold(
                        (Vec::new(), None),
                        |(mut acc, mut last), datum| match datum.as_rule() {
                            Rule::datum => {
                                acc.push(parse_datum(datum)?);
                                Ok((acc, last))
                            }
                            Rule::dotted_last => {
                                last = Some(Box::new(parse_datum(datum.into_inner().next().expect(
                            "r7rs.pest ensures that dotted_last only contains a single datum.",
                        ))?));
                                Ok((acc, last))
                            }
                            _ => Err(LexicalError::unexpected_rule(
                                datum,
                                &[Rule::datum, Rule::dotted_last],
                            )),
                        },
                    )?;
            Ok(Literal::Compound(CompoundDatum::List { head, tail }))
        }
        Rule::vector => Ok(Literal::Vec(
            first
                .into_inner()
                .map(parse_datum)
                .collect::<Result<Vec<_>>>()?,
        )),
        Rule::abbreviation => {
            let mut abbrev_parts = first.into_inner();
            let prefix = abbrev_parts
                .next()
                .expect("r7rs.pest ensures an abbreviation has the prefix as first part.");
            let datum = abbrev_parts
                .next()
                .expect("r7rs.pest ensures an abbreviation has the data as second part.");
            match prefix.as_str() {
                "'" => Ok(Literal::Compound(CompoundDatum::Quote(Box::new(
                    parse_datum(datum)?,
                )))),
                "`" => Ok(Literal::Compound(CompoundDatum::Backquote(Box::new(
                    parse_datum(datum)?,
                )))),
                "," => Ok(Literal::Compound(CompoundDatum::Comma(Box::new(
                    parse_datum(datum)?,
                )))),
                ",@" => Ok(Literal::Compound(CompoundDatum::CommaAt(Box::new(
                    parse_datum(datum)?,
                )))),
                illegal => Err(LexicalError::Generic {
                    message: format!("Illegal abbrev prefix in data {illegal:?}"),
                }),
            }
        }
        _ => Err(LexicalError::unexpected_rule(
            first,
            &[
                Rule::label,
                Rule::boolean,
                Rule::number,
                Rule::character,
                Rule::string,
                Rule::symbol,
                Rule::bytevector,
                Rule::list,
                Rule::vector,
                Rule::abbreviation,
            ],
        )),
    }
}

#[tracing::instrument(skip(ast))]
pub(super) fn parse_derived_expression(ast: Pair<Rule>) -> Result<Expression> {
    debug_assert!(matches!(ast.as_rule(), Rule::derived_expression));

    let mut iter = ast.into_inner();
    let first = iter
        .next()
        .expect("r7rs.pest ensures that 'derived_expression' has an inner rule");
    match first.as_rule() {
        Rule::derived_expr_cond => {
            let clauses = iter
                .by_ref()
                .take_while(|rule| rule.as_rule() != Rule::derived_expr_else)
                .map(parse_cond_clause)
                .collect::<Result<VecDeque<_>>>()?;
            let fallback = parse_sequence(
                iter.next()
                    .expect("r7rs.pest rule for derived_expr_cond guarantees an else clause"),
            )?;
            Ok(Expression::Cond(CondExpr {
                clauses,
                fallback: Some(fallback),
            }))
        }
        Rule::derived_expr_cond_no_else => {
            let clauses = iter
                .map(parse_cond_clause)
                .collect::<Result<VecDeque<_>>>()?;
            Ok(Expression::Cond(CondExpr {
                clauses,
                fallback: None,
            }))
        }
        Rule::derived_expr_case => {
            let tested_value = Box::new(parse_expression(
                iter.next()
                    .expect("r7rs.pest ensures after derived_expr_case there is the expression."),
            )?);
            let clauses = iter
                .by_ref()
                .take_while(|case_clause| case_clause.as_rule() == Rule::case_clause)
                .map(parse_case_clause)
                .collect::<Result<VecDeque<_>>>()?;
            // NOTE: take_while consumed the derived_expr_else
            let fallback = iter
                .next()
                .expect("r7rs.pest ensures that there is a fallback case in derived_expr_case");
            match fallback.as_rule() {
                Rule::recipient => Ok(Expression::CaseWithRecipientFallback(
                    CaseWithRecipientFallback {
                        tested_value,
                        clauses,
                        fallback: Box::new(parse_expression(unwrap_inner(fallback)?)?),
                    },
                )),
                Rule::sequence => Ok(Expression::CaseWithSequenceFallback(
                    CaseWithSequenceFallback {
                        tested_value,
                        clauses,
                        fallback: Some(parse_sequence(fallback)?),
                    },
                )),
                _ => Err(LexicalError::unexpected_rule(
                    fallback,
                    &[Rule::recipient, Rule::sequence],
                )),
            }
        }
        Rule::derived_expr_case_no_else => {
            let tested_value = Box::new(parse_expression(
                iter.next()
                    .expect("r7rs.pest ensures after derived_expr_case there is the expression."),
            )?);
            let clauses = iter
                .map(parse_case_clause)
                .collect::<Result<VecDeque<_>>>()?;
            Ok(Expression::CaseWithSequenceFallback(
                CaseWithSequenceFallback {
                    tested_value,
                    clauses,
                    fallback: None,
                },
            ))
        }
        Rule::derived_expr_and => Ok(Expression::And(AndExpr {
            tests: iter
                .map(parse_expression)
                .collect::<Result<VecDeque<_>>>()?,
        })),
        Rule::derived_expr_or => Ok(Expression::Or(OrExpr {
            tests: iter
                .map(parse_expression)
                .collect::<Result<VecDeque<_>>>()?,
        })),
        Rule::derived_expr_when => {
            let test = parse_expression(
                iter.next()
                    .expect("r7rs.pest ensures there's a test in a 'when' rule."),
            )?;
            let consequent = parse_sequence(
                iter.next()
                    .expect("r7rs.pest ensures there's a consequent in a 'when' rule."),
            )?;
            Ok(Expression::When(WhenExpr {
                test: Box::new(test),
                consequent,
            }))
        }
        Rule::derived_expr_unless => {
            let test = parse_expression(
                iter.next()
                    .expect("r7rs.pest ensures there's a test in an 'unless' rule."),
            )?;
            let consequent = parse_sequence(
                iter.next()
                    .expect("r7rs.pest ensures there's a consequent in an 'unless' rule."),
            )?;
            Ok(Expression::Unless(UnlessExpr {
                test: Box::new(test),
                consequent,
            }))
        }
        Rule::binding_let => {
            let mut iter = iter.peekable();
            let bindings = iter
                .peeking_take_while(|binding| binding.as_rule() == Rule::binding_spec)
                .map(parse_binding_spec)
                .collect::<Result<VecDeque<_>>>()?;
            let body = parse_body(
                iter.next()
                    .expect("r7rs.pest ensures that a body follow the bindings in binding_let"),
            )?;
            Ok(Expression::Let(LetExpr {
                has_star: false,
                bindings,
                body,
            }))
        }
        Rule::binding_let_star => {
            let mut iter = iter.peekable();
            let bindings = iter
                .peeking_take_while(|binding| binding.as_rule() == Rule::binding_spec)
                .map(parse_binding_spec)
                .collect::<Result<VecDeque<_>>>()?;
            let body = parse_body(
                iter.next()
                    .expect("r7rs.pest ensures that a body follow the bindings in binding_let"),
            )?;
            Ok(Expression::Let(LetExpr {
                has_star: true,
                bindings,
                body,
            }))
        }
        Rule::binding_letrec => {
            let mut iter = iter.peekable();
            let bindings = iter
                .peeking_take_while(|binding| binding.as_rule() == Rule::binding_spec)
                .map(parse_binding_spec)
                .collect::<Result<VecDeque<_>>>()?;
            let body = parse_body(
                iter.next()
                    .expect("r7rs.pest ensures that a body follow the bindings in binding_letrec"),
            )?;
            Ok(Expression::LetRec(LetRecExpr {
                has_star: false,
                bindings,
                body,
            }))
        }
        Rule::binding_letrec_star => {
            let mut iter = iter.peekable();
            let bindings = iter
                .peeking_take_while(|binding| binding.as_rule() == Rule::binding_spec)
                .map(parse_binding_spec)
                .collect::<Result<VecDeque<_>>>()?;
            let body = parse_body(
                iter.next()
                    .expect("r7rs.pest ensures that a body follow the bindings in binding_letrec"),
            )?;
            Ok(Expression::LetRec(LetRecExpr {
                has_star: true,
                bindings,
                body,
            }))
        }
        Rule::binding_let_values => {
            let mut iter = iter.peekable();
            let bindings = iter
                .peeking_take_while(|binding| binding.as_rule() == Rule::mv_binding_spec)
                .map(parse_mv_binding_spec)
                .collect::<Result<VecDeque<_>>>()?;
            let body = parse_body(iter.next().expect(
                "r7rs.pest ensures that a body follow the bindings in binding_let_values",
            ))?;
            Ok(Expression::LetValues(LetValuesExpr {
                has_star: false,
                bindings,
                body,
            }))
        }
        Rule::binding_let_values_star => {
            let mut iter = iter.peekable();
            let bindings = iter
                .peeking_take_while(|binding| binding.as_rule() == Rule::mv_binding_spec)
                .map(parse_mv_binding_spec)
                .collect::<Result<VecDeque<_>>>()?;
            let body = parse_body(iter.next().expect(
                "r7rs.pest ensures that a body follow the bindings in binding_let_values",
            ))?;
            Ok(Expression::LetValues(LetValuesExpr {
                has_star: true,
                bindings,
                body,
            }))
        }
        Rule::named_let => {
            let name = parse_identifier(
                iter.next()
                    .expect("r7rs.pest ensures that a name exists in named_let"),
            )?;
            let mut iter = iter.peekable();
            let bindings = iter
                .peeking_take_while(|binding| binding.as_rule() == Rule::binding_spec)
                .map(parse_binding_spec)
                .collect::<Result<VecDeque<_>>>()?;
            let body = parse_body(
                iter.next()
                    .expect("r7rs.pest ensures that a body follow the bindings in named_let"),
            )?;
            Ok(Expression::NamedLet(NamedLetExpr {
                has_star: false,
                name,
                bindings,
                body,
            }))
        }
        Rule::derived_expr_begin => {
            let sequence = parse_sequence(
                iter.next()
                    .expect("r7rs.pest ensures that the begin block has a sequence"),
            )?;
            Ok(Expression::BeginBlock(sequence))
        }
        Rule::do_loop => {
            let mut iter = iter.peekable();
            let iteration_specs = iter
                .peeking_take_while(|iteration| iteration.as_rule() == Rule::iteration_spec)
                .map(parse_iteration_spec)
                .collect::<Result<VecDeque<_>>>()?;
            let test = parse_expression(
                iter.next()
                    .expect("r7rs.pest ensures that a test exist for the do loop"),
            )?;
            let next_rule = iter.peek().map(|pair| pair.as_rule());
            match next_rule {
                Some(Rule::sequence) => {
                    let result = parse_sequence(iter.next().unwrap())?;
                    let commands = iter
                        .map(parse_expression)
                        .collect::<Result<VecDeque<_>>>()?;
                    Ok(Expression::DoLoop(DoLoopExpr {
                        iteration_specs,
                        test: Box::new(test),
                        result: Some(result),
                        commands,
                    }))
                }
                // Rule::command is a transparent rule, so it should be just expression here
                Some(Rule::command | Rule::expression) => {
                    let commands = iter
                        .map(parse_expression)
                        .collect::<Result<VecDeque<_>>>()?;
                    Ok(Expression::DoLoop(DoLoopExpr {
                        iteration_specs,
                        test: Box::new(test),
                        result: None,
                        commands,
                    }))
                }
                Some(_) => Err(LexicalError::unexpected_rule(
                    iter.next().unwrap(),
                    &[Rule::sequence, Rule::command, Rule::expression],
                )),
                None => Ok(Expression::DoLoop(DoLoopExpr {
                    iteration_specs,
                    test: Box::new(test),
                    result: None,
                    commands: VecDeque::new(),
                })),
            }
        }
        Rule::derived_expr_delay => {
            let expr = parse_expression(
                iter.next()
                    .expect("r7rs.pest ensures the existence of an expression in 'delay' rule"),
            )?;
            Ok(Expression::Delay(Box::new(expr)))
        }
        Rule::derived_expr_delay_force => {
            let expr =
                parse_expression(iter.next().expect(
                    "r7rs.pest ensures the existence of an expression in 'delay-force' rule",
                ))?;
            Ok(Expression::DelayForce(Box::new(expr)))
        }
        Rule::derived_expr_parametrize => {
            let mut iter = iter.peekable();
            let mut param_pairs = iter
                .peeking_take_while(|expr| expr.as_rule() == Rule::expression)
                .collect::<VecDeque<_>>()
                .into_iter();
            let mut parameters = VecDeque::new();
            while let Some(expr_0) = param_pairs.next() {
                let expr_1 = param_pairs
                    .next()
                    .expect("r7rs.pest ensures that expressions in parametrize come in pairs.");
                parameters.push_back((parse_expression(expr_0)?, parse_expression(expr_1)?));
            }

            let body = parse_body(
                iter.next()
                    .expect("r7rs.pest ensures that a body follows in parametrize expressions."),
            )?;

            Ok(Expression::Parameterize(ParameterizeExpr {
                parameters,
                body,
            }))
        }
        Rule::derived_expr_guard => {
            let ident = parse_identifier(
                iter.next()
                    .expect("r7rs.pest ensures an ident exist in a 'guard' rule."),
            )?;
            let mut iter = iter.peekable();
            let clauses = iter
                .peeking_take_while(|rule| rule.as_rule() == Rule::cond_clause)
                .map(parse_cond_clause)
                .collect::<Result<VecDeque<_>>>()?;
            let body = parse_body(
                iter.next()
                    .expect("r7rs.pest rule for guard guarantees a body clause"),
            )?;

            Ok(Expression::Guard(GuardExpr {
                ident,
                clauses,
                body,
            }))
        }
        Rule::quasiquotation => Ok(Expression::Quasiquotation(parse_quasiquotation(first)?)),
        Rule::derived_expr_case_lambda => Ok(Expression::CaseLambda(
            iter.map(parse_case_lambda_clause)
                .collect::<Result<VecDeque<_>>>()?,
        )),
        _ => Err(LexicalError::unexpected_rule(
            first,
            &[
                Rule::derived_expr_cond,
                Rule::derived_expr_cond_no_else,
                Rule::derived_expr_case,
                Rule::derived_expr_case_no_else,
                Rule::derived_expr_and,
                Rule::derived_expr_or,
                Rule::derived_expr_when,
                Rule::derived_expr_unless,
                Rule::binding_let,
                Rule::binding_let_star,
                Rule::binding_letrec,
                Rule::binding_letrec_star,
                Rule::binding_let_values,
                Rule::binding_let_values_star,
                Rule::named_let,
                Rule::derived_expr_begin,
                Rule::do_loop,
                Rule::derived_expr_delay,
                Rule::derived_expr_delay_force,
                Rule::derived_expr_parametrize,
                Rule::derived_expr_guard,
                Rule::quasiquotation,
                Rule::derived_expr_case_lambda,
            ],
        )),
    }
}
