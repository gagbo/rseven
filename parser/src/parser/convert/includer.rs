// SPDX-FileCopyrightText: 2022-2023 Gerry Agbobada <git@gagbo.net>
//
// SPDX-License-Identifier: LGPL-3.0-only

use crate::{
    ast::Includer,
    error::{LexicalError, Result},
    lexer::Rule,
    parser::convert::parse_string_literal,
};
use pest::iterators::Pair;

#[tracing::instrument(skip(ast))]
pub(super) fn parse_includer(ast: Pair<Rule>) -> Result<Includer> {
    debug_assert!(matches!(ast.as_rule(), Rule::includer));

    let mut iter = ast.into_inner();
    let first = iter
        .next()
        .expect("r7rs.pest ensures that the 'includer' rule has the includer keyword first.");
    let is_ci = match first.as_rule() {
        Rule::includer_case_insensitive => Ok(true),
        Rule::includer_case_sensitive => Ok(false),
        _ => Err(LexicalError::unexpected_rule(
            first,
            &[
                Rule::includer_case_insensitive,
                Rule::includer_case_sensitive,
            ],
        )),
    }?;
    let targets = iter.map(parse_string_literal).collect::<Result<Vec<_>>>()?;

    Ok(Includer { is_ci, targets })
}
