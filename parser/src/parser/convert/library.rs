// SPDX-FileCopyrightText: 2023 Gerry Agbobada <git@gagbo.net>
//
// SPDX-License-Identifier: LGPL-3.0-only

use crate::{
    ast::{
        ExceptImportSet, ExportSpec, ImportSet, Library, LibraryDeclaration, LibraryName,
        LibraryNamePart, OnlyImportSet, PrefixImportSet, RenameImportSet,
    },
    error::{LexicalError, Result},
    lexer::Rule,
    parser::convert::{parse_command_or_def, parse_identifier, parse_includer, unwrap_inner},
};
use itertools::Itertools;
use pest::iterators::Pair;

#[tracing::instrument(skip(ast))]
pub(super) fn parse_library(ast: Pair<Rule>) -> Result<Library> {
    debug_assert!(matches!(ast.as_rule(), Rule::library));
    let mut iter = ast.into_inner();
    let name = parse_library_name(
        iter.next()
            .expect("r7rs.pest ensure there's a library name"),
    )?;
    let statements = iter
        .map(parse_library_declaration)
        .collect::<Result<Vec<LibraryDeclaration>>>()?;
    Ok(Library { name, statements })
}

#[tracing::instrument(skip(ast))]
pub(super) fn parse_library_declaration(ast: Pair<Rule>) -> Result<LibraryDeclaration> {
    debug_assert!(matches!(ast.as_rule(), Rule::library_declaration));
    let mut iter = ast.into_inner();
    let first = iter
        .next()
        .expect("r7rs.pest ensures that the library_declaration rule has at least 1 child");
    // TODO(#A1): DFS here
    match first.as_rule() {
        Rule::library_declaration_export => Ok(LibraryDeclaration::Export(
            iter.map(parse_export_spec).collect::<Result<Vec<_>>>()?,
        )),
        Rule::import_declaration => Ok(LibraryDeclaration::Import(
            first
                .into_inner()
                .map(parse_import_set)
                .collect::<Result<Vec<_>>>()?,
        )),
        Rule::library_declaration_begin => Ok(LibraryDeclaration::BeginBlock(
            iter.map(parse_command_or_def).collect::<Result<Vec<_>>>()?,
        )),
        Rule::includer => Ok(LibraryDeclaration::Includer(parse_includer(first)?)),
        Rule::library_declaration_include_declarations => {
            todo!("include-library-declarations")
        }
        Rule::library_declaration_cond_expand_no_else => {
            todo!("cond-expand-no-else")
        }
        Rule::library_declaration_cond_expand => {
            todo!("cond-expand")
        }
        _ => Err(LexicalError::unexpected_rule(
            first,
            &[
                Rule::library_declaration_export,
                Rule::import_declaration,
                Rule::library_declaration_begin,
                Rule::includer,
                Rule::library_declaration_include_declarations,
                Rule::library_declaration_cond_expand_no_else,
                Rule::library_declaration_cond_expand,
            ],
        )),
    }
}

#[tracing::instrument(skip(ast))]
pub(super) fn parse_export_spec(ast: Pair<Rule>) -> Result<ExportSpec> {
    debug_assert!(matches!(ast.as_rule(), Rule::export_spec));

    let mut iter = ast.into_inner();

    // We only match the 2 first elements of the iterator because
    // r7rs.pest guarantees that if we have 2 elements here, that's
    // because the export_spec got parsed as `(rename Id1 Id2)`
    match (iter.next(), iter.next()) {
        (Some(inner_name), Some(new_name)) => Ok(ExportSpec::Renamed {
            inner_name: parse_identifier(inner_name)?,
            new_name: parse_identifier(new_name)?,
        }),
        (Some(name), None) => Ok(ExportSpec::Single(parse_identifier(name)?)),
        _ => Err(LexicalError::Generic {
            message: "Illegal Export Spec".to_string(),
        }),
    }
}

#[tracing::instrument(skip(ast))]
pub(super) fn parse_import_set(ast: Pair<Rule>) -> Result<ImportSet> {
    debug_assert!(matches!(ast.as_rule(), Rule::import_set));

    let mut iter = ast.into_inner();
    let first = iter
        .next()
        .expect("r7rs.pest ensures that the library_declaration rule has at least 1 child");
    match first.as_rule() {
        Rule::library_name => Ok(ImportSet::Bare(parse_library_name(first)?)),
        Rule::import_set_only => {
            let set = parse_import_set(
                iter.next()
                    .expect("r7rs.pest rule for import_set_only ensure there is a set here."),
            )?;
            let only = iter.map(parse_identifier).collect::<Result<Vec<_>>>()?;
            Ok(ImportSet::Only(OnlyImportSet {
                set: Box::new(set),
                only,
            }))
        }
        Rule::import_set_except => {
            let set = parse_import_set(
                iter.next()
                    .expect("r7rs.pest rule for import_set_except ensure there is a set here."),
            )?;
            let except = iter.map(parse_identifier).collect::<Result<Vec<_>>>()?;
            Ok(ImportSet::Except(ExceptImportSet {
                set: Box::new(set),
                except,
            }))
        }
        Rule::import_set_prefix => {
            let set = parse_import_set(
                iter.next()
                    .expect("r7rs.pest rule for import_set_prefix ensure there is a set here."),
            )?;
            let prefix = parse_identifier(iter.next().expect(
                "r7rs.pest rule for import_set_prefix ensure there is an identifier here.",
            ))?;
            Ok(ImportSet::Prefix(PrefixImportSet {
                set: Box::new(set),
                prefix,
            }))
        }
        Rule::import_set_rename => {
            let set = parse_import_set(
                iter.next()
                    .expect("r7rs.pest rule for import_set_rename ensure there is a set here."),
            )?;
            let old_new_pairs = iter
                .chunks(2)
                .into_iter()
                .map(|mut old_new_pair| {
                    let old = old_new_pair.next().unwrap();
                    let new = old_new_pair.next().unwrap();
                    Ok((parse_identifier(old)?, parse_identifier(new)?))
                })
                .collect::<Result<Vec<_>>>()?;
            Ok(ImportSet::Rename(RenameImportSet {
                set: Box::new(set),
                old_new_pairs,
            }))
        }
        _ => Err(LexicalError::unexpected_rule(
            first,
            &[
                Rule::library_name,
                Rule::import_set_only,
                Rule::import_set_except,
                Rule::import_set_prefix,
                Rule::import_set_rename,
            ],
        )),
    }
}

#[tracing::instrument(skip(ast))]
pub(super) fn parse_library_name(ast: Pair<Rule>) -> Result<LibraryName> {
    debug_assert!(matches!(ast.as_rule(), Rule::library_name));

    ast.into_inner().map(parse_library_name_part).collect()
}

#[tracing::instrument(skip(ast))]
pub(super) fn parse_library_name_part(ast: Pair<Rule>) -> Result<LibraryNamePart> {
    debug_assert!(matches!(ast.as_rule(), Rule::library_name_part));

    let part = unwrap_inner(ast)?;

    match part.as_rule() {
        Rule::uinteger_10 => Ok(LibraryNamePart::Num(part.as_str().parse().map_err(
            |parse_err| LexicalError::IllegalNumber {
                literal: part.as_str().to_string(),
                message: format!("expected a base10 integer, parse error: {parse_err}"),
            },
        )?)),
        Rule::identifier => Ok(LibraryNamePart::Ident(parse_identifier(part)?)),
        _ => Err(LexicalError::unexpected_rule(
            part,
            &[Rule::uinteger_10, Rule::identifier],
        )),
    }
}
