// SPDX-FileCopyrightText: 2023 Gerry Agbobada <git@gagbo.net>
//
// SPDX-License-Identifier: LGPL-3.0-only

use crate::{
    ast::{
        CommandOrDef, ExceptImportSet, Identifier, ImportSet, OnlyImportSet, PrefixImportSet,
        Program, RenameImportSet,
    },
    error::{LexicalError, Result},
    lexer::Rule,
    parser::convert::{parse_expression, parse_identifier, parse_library_name},
};
use pest::iterators::Pair;

use super::definition::parse_definition;

#[tracing::instrument(skip(ast))]
pub(super) fn parse_program(ast: Pair<Rule>) -> Result<Program> {
    debug_assert!(matches!(ast.as_rule(), Rule::program));

    let mut program = Program {
        imports: Vec::new(),
        com_defs: Vec::new(),
    };

    for pair in ast.into_inner() {
        match pair.as_rule() {
            Rule::import_declaration => {
                program.imports.append(&mut parse_import_declaration(pair)?);
            }
            Rule::command_or_definition => {
                program.com_defs.push(parse_command_or_def(pair)?);
            }
            _ => {
                return Err(LexicalError::unexpected_rule(
                    pair,
                    &[Rule::import_declaration, Rule::command_or_definition],
                ));
            }
        }
    }

    Ok(program)
}

#[tracing::instrument(skip(ast))]
pub(super) fn parse_import_declaration(ast: Pair<Rule>) -> Result<Vec<ImportSet>> {
    debug_assert!(matches!(ast.as_rule(), Rule::import_declaration));

    let mut sets = Vec::new();

    for pair in ast.into_inner() {
        match pair.as_rule() {
            Rule::import_set => sets.push(parse_import_set(pair)?),
            _ => {
                return Err(LexicalError::unexpected_rule(pair, &[Rule::import_set]));
            }
        }
    }

    if sets.is_empty() {
        Err(LexicalError::Generic {
            message: "Empty import set is not allowed".to_string(),
        })
    } else {
        Ok(sets)
    }
}

#[tracing::instrument(skip(ast))]
pub(super) fn parse_import_set(ast: Pair<Rule>) -> Result<ImportSet> {
    debug_assert!(matches!(ast.as_rule(), Rule::import_set));
    let mut iter = ast.into_inner();
    let first = iter
        .next()
        .expect("r7rs.pest ensures that there's a first rule in import sets");

    match first.as_rule() {
        Rule::library_name => Ok(ImportSet::Bare(parse_library_name(first)?)),
        Rule::import_set_only => {
            let set = parse_import_set(iter.next().unwrap())?;
            Ok(ImportSet::Only(OnlyImportSet {
                set: Box::new(set),
                only: iter
                    .map(parse_identifier)
                    .collect::<Result<Vec<Identifier>>>()?,
            }))
        }
        Rule::import_set_except => {
            let set = parse_import_set(iter.next().unwrap())?;
            Ok(ImportSet::Except(ExceptImportSet {
                set: Box::new(set),
                except: iter
                    .map(parse_identifier)
                    .collect::<Result<Vec<Identifier>>>()?,
            }))
        }
        Rule::import_set_prefix => {
            let set = parse_import_set(iter.next().unwrap())?;
            Ok(ImportSet::Prefix(PrefixImportSet {
                set: Box::new(set),
                prefix: parse_identifier(iter.next().unwrap())?,
            }))
        }
        Rule::import_set_rename => {
            let set = parse_import_set(iter.next().unwrap())?;
            let mut old_new_pairs = Vec::new();
            while let Some(old_name) = iter.next() {
                old_new_pairs.push((
                    parse_identifier(old_name)?,
                    parse_identifier(
                        iter.next()
                            .expect("r7rs.pest ensures that identifiers come in pairs"),
                    )?,
                ));
            }
            Ok(ImportSet::Rename(RenameImportSet {
                set: Box::new(set),
                old_new_pairs,
            }))
        }
        _ => Err(LexicalError::unexpected_rule(
            first,
            &[
                Rule::library_name,
                Rule::import_set_only,
                Rule::import_set_rename,
                Rule::import_set_except,
                Rule::import_set_prefix,
            ],
        )),
    }
}

#[tracing::instrument(skip(ast))]
pub(super) fn parse_command_or_def(ast: Pair<Rule>) -> Result<CommandOrDef> {
    debug_assert!(matches!(ast.as_rule(), Rule::command_or_definition));

    let mut block_commands: Vec<CommandOrDef> = Vec::new();

    for pair in ast.into_inner() {
        match pair.as_rule() {
            Rule::command => {
                if !block_commands.is_empty() {
                    return Err(LexicalError::Generic {
                        message: "Cannot have a simple command within a block of commands"
                            .to_string(),
                    });
                }
                return Ok(CommandOrDef::Command(parse_expression(pair)?));
            }
            Rule::definition => {
                if !block_commands.is_empty() {
                    return Err(LexicalError::Generic {
                        message: "Cannot have a definition within a block of commands".to_string(),
                    });
                }
                return Ok(CommandOrDef::Definition(parse_definition(pair)?));
            }
            Rule::command_or_definition => {
                block_commands.push(parse_command_or_def(pair)?);
            }
            _ => {
                return Err(LexicalError::unexpected_rule(
                    pair,
                    &[Rule::command, Rule::definition, Rule::command_or_definition],
                ));
            }
        }
    }

    if block_commands.is_empty() {
        Err(LexicalError::Generic {
            message: "Empty (begin ) block is not allowed".to_string(),
        })
    } else {
        Ok(CommandOrDef::BeginBlock(block_commands))
    }
}
