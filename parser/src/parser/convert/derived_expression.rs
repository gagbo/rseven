// SPDX-FileCopyrightText: 2023 Gerry Agbobada <git@gagbo.net>
//
// SPDX-License-Identifier: LGPL-3.0-only

use crate::{
    ast::{
        BindingSpec, CaseClause, CaseLambdaClause, CondClause, IterationSpec,
        MultipleValuesBindingSpec,
    },
    error::{LexicalError, Result},
    lexer::Rule,
    parser::convert::{
        parse_body, parse_datum, parse_expression, parse_formals, parse_identifier, parse_sequence,
        unwrap_inner,
    },
};
use itertools::Itertools;
use pest::iterators::Pair;

pub(super) fn parse_cond_clause(ast: Pair<Rule>) -> Result<CondClause> {
    let mut iter = ast.into_inner();
    let test = parse_expression(unwrap_inner(
        iter.next()
            .expect("r7rs.pest ensures that cond_clause has a test as first element."),
    )?)?;
    if let Some(second) = iter.next() {
        match second.as_rule() {
            Rule::recipient => Ok(CondClause::TestRecipient {
                test,
                recipient: parse_expression(unwrap_inner(second)?)?,
            }),
            Rule::sequence => Ok(CondClause::TestSeq {
                test,
                consequent: parse_sequence(second)?,
            }),
            _ => Err(LexicalError::unexpected_rule(
                second,
                &[Rule::recipient, Rule::sequence],
            )),
        }
    } else {
        Ok(CondClause::Test(test))
    }
}

pub(super) fn parse_case_clause(ast: Pair<Rule>) -> Result<CaseClause> {
    debug_assert!(matches!(ast.as_rule(), Rule::case_clause));
    let mut iter = ast.into_inner().peekable();

    let values = iter
        .peeking_take_while(|datum| datum.as_rule() == Rule::datum)
        .map(parse_datum)
        .collect::<Result<Vec<_>>>()?;
    let cons = iter.next().expect(
        "r7rs.pest ensures that after the data in a case_clause we have a sequence or recipient.",
    );
    match cons.as_rule() {
        Rule::sequence => Ok(CaseClause::Sequence {
            values,
            consequent: parse_sequence(cons)?,
        }),
        Rule::recipient => Ok(CaseClause::Recipient {
            values,
            recipient: parse_expression(unwrap_inner(cons)?)?,
        }),
        _ => Err(LexicalError::unexpected_rule(
            cons,
            &[Rule::sequence, Rule::recipient],
        )),
    }
}

pub(super) fn parse_binding_spec(ast: Pair<Rule>) -> Result<BindingSpec> {
    debug_assert!(matches!(ast.as_rule(), Rule::binding_spec));
    let mut iter = ast.into_inner();

    let binding = parse_identifier(
        iter.next()
            .expect("r7rs.pest ensures a binding is in 'binding_spec' rule"),
    )?;
    let value = parse_expression(
        iter.next()
            .expect("r7rs.pest ensures an expression is in 'binding_spec' rule"),
    )?;

    Ok(BindingSpec { binding, value })
}

pub(super) fn parse_mv_binding_spec(ast: Pair<Rule>) -> Result<MultipleValuesBindingSpec> {
    debug_assert!(matches!(ast.as_rule(), Rule::mv_binding_spec));
    let mut iter = ast.into_inner();

    let bindings = parse_formals(
        iter.next()
            .expect("r7rs.pest ensures formals is in 'binding_spec' rule"),
    )?;
    let value = parse_expression(
        iter.next()
            .expect("r7rs.pest ensures an expression is in 'binding_spec' rule"),
    )?;

    Ok(MultipleValuesBindingSpec { bindings, value })
}

pub(super) fn parse_iteration_spec(ast: Pair<Rule>) -> Result<IterationSpec> {
    debug_assert!(matches!(ast.as_rule(), Rule::iteration_spec));
    let mut iter = ast.into_inner();

    let iterator = parse_identifier(
        iter.next()
            .expect("r7rs.pest ensures the identifier for the iterator is in 'iteration_spec'"),
    )?;
    let init_value = parse_expression(
        iter.next()
            .expect("r7rs.pest ensures the initial value for the iterator is in 'iteration_spec'"),
    )?;
    let step_value = iter.next().map(parse_expression).transpose()?;

    Ok(IterationSpec {
        iterator,
        init_value,
        step_value,
    })
}

pub(super) fn parse_case_lambda_clause(ast: Pair<Rule>) -> Result<CaseLambdaClause> {
    debug_assert!(matches!(ast.as_rule(), Rule::case_lambda_clause));

    let mut iter = ast.into_inner();
    let formals = parse_formals(
        iter.next()
            .expect("r7rs.pest ensure a case lambda clause has formals."),
    )?;
    let body = parse_body(
        iter.next()
            .expect("r7rs.pest ensure a case lambda clause has body."),
    )?;
    Ok(CaseLambdaClause { formals, body })
}
