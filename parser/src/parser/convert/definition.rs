// SPDX-FileCopyrightText: 2023 Gerry Agbobada <git@gagbo.net>
//
// SPDX-License-Identifier: LGPL-3.0-only

use crate::{
    ast::{
        Body, DefFormals, Definition, FieldSpec, Formals, LambdaDefinition, RecordDefinition,
        SingleDefinition, ValuesDefinition,
    },
    error::{LexicalError, Result},
    lexer::Rule,
    parser::convert::{parse_expression, parse_identifier, parse_sequence, unwrap_inner},
};
use itertools::Itertools;
use pest::iterators::Pair;

#[tracing::instrument(skip(ast))]
pub(super) fn parse_definition(ast: Pair<Rule>) -> Result<Definition> {
    debug_assert!(matches!(ast.as_rule(), Rule::definition));
    let mut iter = ast.into_inner();
    let definition_type = iter
        .next()
        .expect("r7rs.pest ensures all definition rule branches have a first token.");
    match definition_type.as_rule() {
        Rule::single_value_definition => {
            let identifier = parse_identifier(
                iter.next()
                    .expect("r7rs.pest guarantees an identifier here."),
            )?;
            let value = parse_expression(
                iter.next()
                    .expect("r7rs.pest guarantees an expression here."),
            )?;
            Ok(Definition::Single(SingleDefinition { identifier, value }))
        }
        Rule::lambda_definition => {
            let identifier = parse_identifier(
                iter.next()
                    .expect("r7rs.pest guarantees an identifier here."),
            )?;
            let formals = parse_def_formals(
                iter.next()
                    .expect("r7rs.pest guarantees an identifier here."),
            )?;
            let body = parse_body(
                iter.next()
                    .expect("r7rs.pest guarantees an identifier here."),
            )?;
            Ok(Definition::Lambda(LambdaDefinition {
                identifier,
                formals,
                body,
            }))
        }
        Rule::syntax_definition => todo!("Return Definition::Syntax"),
        Rule::multiple_values_definition => {
            let formals = parse_formals(
                iter.next()
                    .expect("r7rs.pest ensures that there are formals"),
            )?;
            let body = parse_body(
                iter.next()
                    .expect("r7rs.pest ensures that there is a body."),
            )?;
            Ok(Definition::Values(ValuesDefinition { formals, body }))
        }
        Rule::record_definition => {
            let name =
                parse_identifier(iter.next().expect("r7rs.pest ensures that there's a name"))?;

            let constructor_iter = iter
                .next()
                .expect("r7rs.pest ensure that there's a constructor");
            let mut constructor_field_names = Vec::new();
            let mut constructor_name = None;
            for cons in constructor_iter.into_inner() {
                match cons.as_rule() {
                    Rule::identifier => constructor_name = Some(parse_identifier(cons)?),
                    Rule::field_name => {
                        constructor_field_names.push(parse_identifier(unwrap_inner(cons)?)?)
                    }
                    _ => {
                        return Err(LexicalError::unexpected_rule(
                            cons,
                            &[Rule::identifier, Rule::field_name],
                        ))
                    }
                }
            }
            let constructor = (constructor_name.unwrap(), constructor_field_names);

            let pred = parse_identifier(
                iter.next()
                    .expect("r7rs.pest ensures that there's a predicate"),
            )?;

            let fields = iter
                .map(parse_field_spec)
                .collect::<Result<Vec<FieldSpec>>>()?;

            Ok(Definition::Record(RecordDefinition {
                name,
                constructor,
                pred,
                fields,
            }))
        }
        Rule::multiple_definitions => Ok(Definition::Block(
            iter.map(|def| {
                if def.as_rule() != Rule::definition {
                    Err(LexicalError::unexpected_rule(def, &[Rule::definition]))
                } else {
                    parse_definition(def)
                }
            })
            .collect::<Result<Vec<Definition>>>()?,
        )),
        _unhandled => Err(LexicalError::unexpected_rule(
            definition_type,
            &[
                Rule::single_value_definition,
                Rule::lambda_definition,
                Rule::syntax_definition,
                Rule::multiple_values_definition,
                Rule::record_definition,
                Rule::multiple_definitions,
            ],
        )),
    }
}

#[tracing::instrument(skip(ast))]
pub(super) fn parse_def_formals(ast: Pair<Rule>) -> Result<DefFormals> {
    debug_assert!(matches!(ast.as_rule(), Rule::def_formals));
    let mut idents = Vec::new();
    let mut last_dotted_ident = None;

    for inner in ast.into_inner() {
        match inner.as_rule() {
            Rule::identifier => idents.push(parse_identifier(inner)?),
            Rule::rest_identifier => last_dotted_ident = Some(parse_identifier(inner)?),
            _ => {
                return Err(LexicalError::unexpected_rule(
                    inner,
                    &[Rule::identifier, Rule::rest_identifier],
                ))
            }
        }
    }

    Ok(DefFormals {
        idents,
        last_dotted_ident,
    })
}

#[tracing::instrument(skip(ast))]
pub(super) fn parse_body(ast: Pair<Rule>) -> Result<Body> {
    debug_assert!(matches!(ast.as_rule(), Rule::body));
    let mut iter = ast.into_inner();
    let first = iter
        .next()
        .expect("r7rs.pest ensure body rule has at least one child");
    let mut definitions = Vec::new();
    // Means there are no definitions to parse
    if first.as_rule() == Rule::sequence {
        let sequence = parse_sequence(first)?;
        return Ok(Body {
            definitions,
            sequence,
        });
    }
    definitions.push(parse_definition(first)?);

    let mut iter = iter.peekable();
    definitions.extend(
        iter.peeking_take_while(|rule| rule.as_rule() != Rule::sequence)
            .map(|def| {
                if def.as_rule() != Rule::definition {
                    Err(LexicalError::unexpected_rule(def, &[Rule::definition]))
                } else {
                    parse_definition(def)
                }
            })
            .collect::<Result<Vec<Definition>>>()?,
    );
    let sequence = parse_sequence(iter.next().expect("r7rs.pest ensures that if we have a definition list first, then we have a sequence in body rule"))?;

    Ok(Body {
        definitions,
        sequence,
    })
}

#[tracing::instrument(skip(ast))]
pub(super) fn parse_field_spec(ast: Pair<Rule>) -> Result<FieldSpec> {
    debug_assert!(matches!(ast.as_rule(), Rule::field_spec));
    let mut iter = ast.into_inner();

    let name = parse_identifier(
        iter.next()
            .expect("r7rs.pest ensure a field_spec rule always has a field_name first"),
    )?;
    let accessor = parse_identifier(
        iter.next()
            .expect("r7rs.pest ensure a field_spec rule always has an accessor second"),
    )?;
    let mutator = iter.next().map(parse_identifier).transpose()?;

    Ok(FieldSpec {
        name,
        accessor,
        mutator,
    })
}

#[tracing::instrument(skip(ast))]
pub(super) fn parse_formals(ast: Pair<Rule>) -> Result<Formals> {
    debug_assert!(matches!(ast.as_rule(), Rule::formals));

    // Check for the "single identifier" case. All other don't have wrapping parens.
    if !ast.as_str().starts_with('(') {
        return Ok(Formals::Single(parse_identifier(unwrap_inner(ast)?)?));
    }

    let mut idents = Vec::new();
    let mut last = None;
    for ident in ast.into_inner() {
        match ident.as_rule() {
            Rule::identifier => idents.push(parse_identifier(ident)?),
            Rule::rest_identifier => last = Some(parse_identifier(ident)?),
            _ => {
                return Err(LexicalError::unexpected_rule(
                    ident,
                    &[Rule::identifier, Rule::rest_identifier],
                ))
            }
        }
    }

    if let Some(last) = last {
        Ok(Formals::Dotted { head: idents, last })
    } else {
        Ok(Formals::List(idents))
    }
}
