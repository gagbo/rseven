// SPDX-FileCopyrightText: 2022-2023 Gerry Agbobada <git@gagbo.net>
//
// SPDX-License-Identifier: LGPL-3.0-only

use crate::ast::Rational;
use crate::ast::{MaybeRational, MaybeReal, Number, NumberExact, NumberInexact};
use crate::error::{LexicalError, Result};
use crate::lexer::{Pair, Rule};

pub(crate) fn convert_number_literal(ast: Pair<Rule>) -> Result<Number> {
    let span = tracing::span!(tracing::Level::TRACE, "eval_number");
    let _enter = span.enter();
    let literal = ast.as_str();
    ast.into_inner().fold(
        Err(LexicalError::Generic {
            message: format!("No number found in {literal}"),
        }),
        |_acc, pair| match pair.as_rule() {
            // Call eval_number again in this case.
            // Currently it occurs when we parse the inner
            // values of a bytevector
            Rule::number => convert_number_literal(pair),
            Rule::num_2 => convert_num2(pair),
            Rule::num_8 => convert_num8(pair),
            Rule::num_10 => convert_num10(pair),
            Rule::num_16 => convert_num16(pair),
            unhandled_rule => {
                todo!("Unhandled rule: {:#?}", unhandled_rule)
            }
        },
    )
}

#[derive(Debug, Default)]
pub(super) struct NumberBuilder {
    pub base: u8,
    pub exact: Option<bool>,
    pub number: String,
}

impl NumberBuilder {
    pub(super) fn build(self) -> Result<Number> {
        let span = tracing::span!(tracing::Level::TRACE, "NumberBuilder::build");
        let _enter = span.enter();
        // Rules to handle
        //
        // complex_10 = { (real_10 ~ "@" ~ real_10)
        //     | (real_10 ~ "+" ~ ureal_10 ~ "i")
        //     | (real_10 ~ "-" ~ ureal_10 ~ "i")
        //     | (real_10 ~ "+" ~ "i")
        //     | (real_10 ~ "-" ~ "i")
        //     | (real_10 ~ infnan ~ "i")
        //     | ("+" ~ ureal_10 ~ "i")
        //     | ("-" ~ ureal_10 ~ "i")
        //     | (infnan ~ "i")
        //     | ("+" ~ "i")
        //     | ("-" ~ "i")
        //     | real_10 }
        // // Is it atomic ???
        // real_10 = { (sign{,1} ~ ureal_10) | infnan }
        // ureal_10 = ${ ( uinteger_10 ~ "/" ~ uinteger_10) | uinteger_10 | decimal_10 }

        let deduce_as_inexact =
            |repr: &str| repr.contains(|c: char| vec!['.', 'E', 'e'].contains(&c));

        let parse_exact = |repr: &str| -> Result<MaybeRational> {
            let span = tracing::span!(tracing::Level::TRACE, "NumberBuilder::build::parse_exact");
            let _enter = span.enter();
            // When we receive 23/8
            if let Some(idx) = repr.find('/') {
                let numerator: i128 =
                    i128::from_str_radix(&repr[0..idx], self.base as u32).unwrap();
                let denominator: i128 =
                    i128::from_str_radix(&repr[idx + 1..], self.base as u32).unwrap();
                Ok(Rational::new(numerator, denominator).into())
            }
            // When we receive #e6.6
            // repr is expected here to just be '6.6'
            // or '-6.6' maybe
            else if let Some(idx) = repr.find('.') {
                if self.base != 10 {
                    return Err(LexicalError::IllegalNumber {
                        literal: repr.to_string(),
                        message: format!(
                            "Decimal . is only allowed for base10 literals, not {}",
                            self.base
                        ),
                    });
                }
                let decimal_part_length = repr.len() - idx;
                let denominator = 10_i128.pow(decimal_part_length as u32);
                let numerator = repr.parse::<f64>().unwrap() * (denominator as f64);
                assert!((numerator.floor() - numerator).abs() < f64::EPSILON);
                Ok(Rational::new(numerator.floor() as i128, denominator).into())
            }
            // Otherwise it's just an int
            else {
                let value: i128 = i128::from_str_radix(repr, self.base as u32).unwrap();
                Ok(Rational::int(value).into())
            }
        };

        let parse_inexact = |repr: &str| -> Result<MaybeReal> {
            let span = tracing::span!(tracing::Level::TRACE, "NumberBuilder::build::parse_inexact");
            let _enter = span.enter();
            // When we receive #i23/7
            // repr is expected here to just be '23/7'
            // or '-23/7' maybe (for #i-23/7)
            if let Some(idx) = repr.find('/') {
                let numerator: i64 = i64::from_str_radix(&repr[0..idx], self.base as u32).unwrap();
                let denominator: i64 =
                    i64::from_str_radix(&repr[idx + 1..], self.base as u32).unwrap();
                Ok((numerator as f64 / denominator as f64).into())
            }
            // Otherwise it's just a float
            else {
                let value: f64 = repr.parse().unwrap();
                Ok(value.into())
            }
        };

        if self
            .exact
            .unwrap_or_else(|| !deduce_as_inexact(&self.number))
        {
            if let Some(idx) = self.number.find('@') {
                let magnitude = match parse_exact(&self.number[0..idx])? {
                    MaybeRational::MinusInf => {
                        todo!("Handle non finite magnitude")
                    }
                    MaybeRational::PlusInf => {
                        todo!("Handle non finite magnitude")
                    }
                    MaybeRational::Nan => {
                        todo!("Handle non finite magnitude")
                    }
                    MaybeRational::Rational(val) => val.value(),
                };
                let angle = match parse_exact(&self.number[idx + 1..])? {
                    MaybeRational::MinusInf => {
                        todo!("Handle non finite angle")
                    }
                    MaybeRational::PlusInf => {
                        todo!("Handle non finite angle")
                    }
                    MaybeRational::Nan => {
                        todo!("Handle non finite angle")
                    }
                    MaybeRational::Rational(val) => val.value(),
                };
                let real = magnitude * angle.cos();
                let imag = magnitude * angle.sin();
                if imag.abs() > f64::EPSILON {
                    Ok(NumberInexact::new(real, imag).into())
                } else {
                    Ok(NumberInexact::new(real, 0.0).into())
                }
            } else if self.number.ends_with('i') {
                let idx = self.number.find(&['-', '+'][..]);
                let len = self.number.len();
                match idx {
                    None => Ok(NumberExact::new(0, parse_exact(&self.number[0..len - 1])?).into()),
                    // Testing specifically ±i
                    Some(i) if i == 0 && len == 2 => Ok(NumberExact::new(
                        0,
                        if self.number.starts_with('+') { 1 } else { -1 },
                    )
                    .into()),
                    Some(i) if i == 0 => {
                        Ok(NumberExact::new(0, parse_exact(&self.number[0..len - 1])?).into())
                    }
                    Some(i) => Ok(NumberExact::new(
                        parse_exact(&self.number[0..i])?,
                        parse_exact(&self.number[i..len - 1])?,
                    )
                    .into()),
                }
            } else {
                Ok(NumberExact::new(parse_exact(&self.number)?, 0).into())
            }
        // Inexact number evaluation
        } else if let Some(idx) = self.number.find('@') {
            let magnitude = match parse_inexact(&self.number[0..idx])? {
                MaybeReal::MinusInf => {
                    todo!("Handle non finite magnitude")
                }
                MaybeReal::PlusInf => {
                    todo!("Handle non finite magnitude")
                }
                MaybeReal::Nan => {
                    todo!("Handle non finite magnitude")
                }
                MaybeReal::Real(val) => val,
            };
            let angle = match parse_inexact(&self.number[idx + 1..])? {
                MaybeReal::MinusInf => {
                    todo!("Handle non finite angle")
                }
                MaybeReal::PlusInf => {
                    todo!("Handle non finite angle")
                }
                MaybeReal::Nan => {
                    todo!("Handle non finite angle")
                }
                MaybeReal::Real(val) => val,
            };
            let real = magnitude * angle.cos();
            let imag = magnitude * angle.sin();
            if imag.abs() > f64::EPSILON {
                Ok(NumberInexact::new(real, imag).into())
            } else {
                Ok(NumberInexact::new(real, 0.0).into())
            }
        } else if self.number.ends_with('i') {
            let idx = self.number.find(&['-', '+'][..]);
            let len = self.number.len();
            // No real part in the number
            match idx {
                None => {
                    Ok(NumberInexact::new(0.0, parse_inexact(&self.number[0..len - 1])?).into())
                }
                // Testing specifically ±i
                Some(i) if i == 0 && len == 2 => Ok(NumberInexact::new(
                    0.0,
                    if self.number.starts_with('+') {
                        1.0
                    } else {
                        -1.0
                    },
                )
                .into()),
                Some(i) if i == 0 => {
                    Ok(NumberInexact::new(0.0, parse_inexact(&self.number[0..len - 1])?).into())
                }
                Some(i) => Ok(NumberInexact::new(
                    parse_inexact(&self.number[0..i])?,
                    parse_inexact(&self.number[i..len - 1])?,
                )
                .into()),
            }
        } else {
            Ok(NumberInexact::new(parse_inexact(&self.number)?, 0.0).into())
        }
    }
}

pub fn convert_num10(ast: Pair<Rule>) -> Result<Number> {
    let span = tracing::span!(tracing::Level::TRACE, "eval_num10");
    let _enter = span.enter();
    // We know it's base 10, let's just go on the loop to see how to build the number
    let builder: NumberBuilder = ast.into_inner().fold(
        NumberBuilder {
            base: 10,
            ..NumberBuilder::default()
        },
        |acc, pair| match pair.as_rule() {
            Rule::complex_10 => NumberBuilder {
                number: pair.as_str().into(),
                ..acc
            },
            Rule::prefix_10 => {
                if pair.as_str().contains("#e") {
                    NumberBuilder {
                        exact: Some(true),
                        ..acc
                    }
                } else if pair.as_str().contains("#i") {
                    NumberBuilder {
                        exact: Some(false),
                        ..acc
                    }
                } else {
                    acc
                }
            }
            unhandled_rule => {
                unreachable!("Unhandled rule: {:#?}", unhandled_rule)
            }
        },
    );
    builder.build()
}

pub fn convert_num16(ast: Pair<Rule>) -> Result<Number> {
    let span = tracing::span!(tracing::Level::TRACE, "eval_num16");
    let _enter = span.enter();
    // We know it's base 16, let's just go on the loop to see how to build the number
    let builder: NumberBuilder = ast.into_inner().fold(
        NumberBuilder {
            base: 16,
            ..NumberBuilder::default()
        },
        |acc, pair| match pair.as_rule() {
            Rule::complex_16 => NumberBuilder {
                number: pair.as_str().into(),
                ..acc
            },
            Rule::prefix_16 => {
                if pair.as_str().contains("#e") {
                    NumberBuilder {
                        exact: Some(true),
                        ..acc
                    }
                } else if pair.as_str().contains("#i") {
                    NumberBuilder {
                        exact: Some(false),
                        ..acc
                    }
                } else {
                    acc
                }
            }
            unhandled_rule => {
                unreachable!("Unhandled rule: {:#?}", unhandled_rule)
            }
        },
    );
    builder.build()
}

pub fn convert_num2(ast: Pair<Rule>) -> Result<Number> {
    let span = tracing::span!(tracing::Level::TRACE, "eval_num2");
    let _enter = span.enter();
    // We know it's base 2, let's just go on the loop to see how to build the number
    let builder: NumberBuilder = ast.into_inner().fold(
        NumberBuilder {
            base: 2,
            ..NumberBuilder::default()
        },
        |acc, pair| match pair.as_rule() {
            Rule::complex_2 => NumberBuilder {
                number: pair.as_str().into(),
                ..acc
            },
            Rule::prefix_2 => {
                if pair.as_str().contains("#e") {
                    NumberBuilder {
                        exact: Some(true),
                        ..acc
                    }
                } else if pair.as_str().contains("#i") {
                    NumberBuilder {
                        exact: Some(false),
                        ..acc
                    }
                } else {
                    acc
                }
            }
            unhandled_rule => {
                unreachable!("Unhandled rule: {:#?}", unhandled_rule)
            }
        },
    );
    builder.build()
}

pub fn convert_num8(ast: Pair<Rule>) -> Result<Number> {
    let span = tracing::span!(tracing::Level::TRACE, "eval_num8");
    let _enter = span.enter();
    // We know it's base 8, let's just go on the loop to see how to build the number
    let builder: NumberBuilder = ast.into_inner().fold(
        NumberBuilder {
            base: 8,
            ..NumberBuilder::default()
        },
        |acc, pair| match pair.as_rule() {
            Rule::complex_8 => NumberBuilder {
                number: pair.as_str().into(),
                ..acc
            },
            Rule::prefix_8 => {
                if pair.as_str().contains("#e") {
                    NumberBuilder {
                        exact: Some(true),
                        ..acc
                    }
                } else if pair.as_str().contains("#i") {
                    NumberBuilder {
                        exact: Some(false),
                        ..acc
                    }
                } else {
                    acc
                }
            }
            unhandled_rule => {
                unreachable!("Unhandled rule: {:#?}", unhandled_rule)
            }
        },
    );
    builder.build()
}
