// SPDX-FileCopyrightText: 2022-2023 Gerry Agbobada <git@gagbo.net>
//
// SPDX-License-Identifier: LGPL-3.0-only

use crate::{
    ast::{Literal, QqTemplate, Quasiquotation},
    error::{LexicalError, Result},
    lexer::Rule,
    parser::convert::{
        convert_number_literal, parse_bytevector, parse_character, parse_expression,
        parse_identifier, parse_string_literal, unwrap_inner,
    },
};
use pest::iterators::Pair;

pub(super) fn parse_quasiquotation(ast: Pair<Rule>) -> Result<Quasiquotation> {
    debug_assert!(matches!(ast.as_rule(), Rule::quasiquotation));

    let qq_1 = unwrap_inner(ast)?;
    Ok(Quasiquotation {
        inner: parse_qq_template(qq_1, 1)?,
    })
}

fn parse_qq_template(ast: Pair<Rule>, level: usize) -> Result<QqTemplate> {
    debug_assert!(matches!(ast.as_rule(), Rule::quasiquotation_1));

    let qq_1_variant = unwrap_inner(ast)?;

    match qq_1_variant.as_rule() {
        // simple_datum handling begin
        Rule::boolean => Ok(QqTemplate::Datum(Literal::Bool(
            qq_1_variant.as_str().starts_with("#t"),
        ))),
        Rule::number => Ok(QqTemplate::Datum(Literal::Num(convert_number_literal(
            qq_1_variant,
        )?))),
        Rule::character => Ok(QqTemplate::Datum(parse_character(qq_1_variant)?)),
        Rule::string => Ok(QqTemplate::Datum(Literal::StringLiteral(
            parse_string_literal(qq_1_variant)?,
        ))),
        Rule::symbol => Ok(QqTemplate::Datum(Literal::Symbol(parse_identifier(
            qq_1_variant,
        )?))),
        Rule::bytevector => Ok(QqTemplate::Datum(parse_bytevector(qq_1_variant)?)),
        // simple_datum handling end
        Rule::list_qq_template_1 => {
            todo!()
        }
        Rule::vector_qq_template_1 => {
            todo!()
        }
        Rule::unquotation_1 => {
            if level == 1 {
                Ok(QqTemplate::Zero(Box::new(parse_expression(unwrap_inner(
                    unwrap_inner(qq_1_variant)?,
                )?)?)))
            } else {
                todo!("Re-lex qq_1_variant into qq_template_1, recurse in parse_qq_template with level - 1")
            }
        }
        _ => Err(LexicalError::unexpected_rule(
            qq_1_variant,
            &[
                Rule::simple_datum,
                Rule::boolean,
                Rule::number,
                Rule::character,
                Rule::string,
                Rule::symbol,
                Rule::bytevector,
                Rule::list_qq_template_1,
                Rule::vector_qq_template_1,
                Rule::unquotation_1,
            ],
        )),
    }
}
