// SPDX-FileCopyrightText: 2022-2023 Gerry Agbobada <git@gagbo.net>
//
// SPDX-License-Identifier: LGPL-3.0-only

mod definition;
mod derived_expression;
mod expression;
mod includer;
mod library;
mod number;
mod program;
mod quasiquotation;

use crate::{
    ast::{Ast, AstNode},
    error::{LexicalError, Result},
    lexer::Rule,
};
use definition::*;
use derived_expression::*;
use expression::*;
use includer::*;
use library::*;
use number::*;
use pest::iterators::Pair;
use program::*;
use quasiquotation::*;

#[tracing::instrument(skip(ast))]
pub(super) fn parse_lexed_tree<'code>((ast, code): (Pair<Rule>, &'code str)) -> Result<Ast<'code>> {
    ast.into_inner().try_fold(
        Ast {
            source: code,
            node: AstNode::Nil,
        },
        |mut acc, pair| -> Result<Ast> {
            match pair.as_rule() {
                Rule::r7rs => {
                    unreachable!("The r7rs rule is not matched within itself")
                }
                Rule::EOI | Rule::COMMENT | Rule::atmosphere | Rule::WHITESPACE => Ok(acc),
                Rule::program => {
                    acc.node = AstNode::Program(parse_program(pair)?);
                    Ok(acc)
                }
                Rule::library => {
                    acc.node = AstNode::Library(parse_library(pair)?);
                    Ok(acc)
                }
                Rule::expression => {
                    acc.node = AstNode::Expression(parse_expression(pair)?);
                    Ok(acc)
                }
                Rule::definition => {
                    acc.node = AstNode::Definition(parse_definition(pair)?);
                    Ok(acc)
                }
                unhandled_rule => {
                    unreachable!("Unhandled rule: {:#?}", unhandled_rule)
                }
            }
        },
    )
}

/// Unwrap a rule that has only a unique element inside
/// Useful for the small rules in Pest grammar that
/// are expression or identifier wrappers like:
/// test = { expression }
/// operator = { expression }
/// field_name = { identifier }
#[tracing::instrument(skip(ast))]
pub(super) fn unwrap_inner(ast: Pair<Rule>) -> Result<Pair<Rule>> {
    let mut iter = ast.into_inner();
    let res = iter.next();
    if iter.next().is_some() {
        return Err(LexicalError::Generic {
            message: "Expected a single grammar rule inside, got more.".to_string(),
        });
    }

    res.ok_or_else(|| LexicalError::Generic {
        message: "Expected a single grammar rule inside, got none".to_string(),
    })
}
