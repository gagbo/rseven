// SPDX-FileCopyrightText: 2023 Gerry Agbobada <git@gagbo.net>
//
// SPDX-License-Identifier: LGPL-3.0-only

use pretty_assertions::assert_eq;

use crate::ast::{AstNode, Expression, Literal, Number, NumberExact};

use super::parse;

#[test]
fn identifier_simple() {
    let tree = parse("bait").unwrap();
    assert_eq!(
        tree.node,
        AstNode::Expression(Expression::Ident("bait".to_string()))
    );
}

#[test]
fn identifier_peculiar() {
    let tree = parse("-prefixed").unwrap();
    assert_eq!(
        tree.node,
        AstNode::Expression(Expression::Ident("-prefixed".to_string()))
    );
}

#[test]
fn identifier_peculiar_dot() {
    let tree = parse(".dot").unwrap();
    assert_eq!(
        tree.node,
        AstNode::Expression(Expression::Ident(".dot".to_string()))
    );
}

#[test]
fn number_peculiar_exception() {
    let tree = parse("-i").unwrap();
    assert_eq!(
        tree.node,
        AstNode::Expression(Expression::Literal(Literal::Num(Number::Exact(
            NumberExact::new(0, -1)
        ))))
    );
}

#[test]
fn identifier_vertical_simple() {
    let tree = parse("|between-vertical-lines|").unwrap();
    assert_eq!(
        tree.node,
        AstNode::Expression(Expression::Ident("between-vertical-lines".to_string()))
    );
}

#[test]
fn identifier_vertical_hex_escape() {
    let tree = parse("|b\\x00e9;po|").unwrap();
    assert_eq!(
        tree.node,
        AstNode::Expression(Expression::Ident("bépo".to_string()))
    );

    let tree = parse("|h\\x00E9;ros|").unwrap();
    assert_eq!(
        tree.node,
        AstNode::Expression(Expression::Ident("héros".to_string()))
    );
}

#[test]
fn identifier_vertical_mnemonic_escape() {
    let tree = parse("|S\\nym\\tbol-wi\\ath-eve\\bryt\\rhing|").unwrap();
    assert_eq!(
        tree.node,
        AstNode::Expression(Expression::Ident(
            "S\nym\tbol-wi\u{0007}th-eve\u{0008}ryt\rhing".to_string()
        ))
    );
}
