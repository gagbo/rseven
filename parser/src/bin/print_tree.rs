// SPDX-FileCopyrightText: 2022-2023 Gerry Agbobada <git@gagbo.net>
//
// SPDX-License-Identifier: LGPL-3.0-only

use rustyline::{error::ReadlineError, DefaultEditor};
use tracing::debug;
use tracing_subscriber::EnvFilter;

fn main() -> Result<(), anyhow::Error> {
    let subscriber = tracing_subscriber::fmt()
        .with_env_filter(EnvFilter::from_default_env())
        .pretty()
        .finish();
    tracing::subscriber::set_global_default(subscriber).expect("setting tracing default failed");

    let mut rl = DefaultEditor::new()?;
    if rl.load_history(".rseven-tree-history").is_err() {
        debug!("No previous history.");
    }

    loop {
        let readline = rl.readline("tree> ");
        match readline {
            Ok(line) => {
                rl.add_history_entry(&line)?;
                rl.save_history(".rseven-tree-history")?;
                let res = rseven_parser::parse(&line);
                match res {
                    Ok(tree) => println!("{}", dbg_pls::color(&tree)),
                    Err(err) => println!("Parsing Error: {err}"),
                }
            }
            Err(ReadlineError::Interrupted) => continue,
            Err(ReadlineError::Eof) => break Ok(()),
            Err(err) => {
                println!("Error: {err:?}");
                break Err(err.into());
            }
        }
    }
}
