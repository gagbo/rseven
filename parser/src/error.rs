// SPDX-FileCopyrightText: 2022-2023 Gerry Agbobada <git@gagbo.net>
//
// SPDX-License-Identifier: LGPL-3.0-only

use pest::iterators::Pair;

use crate::lexer::Rule;
use std::error::Error;
use std::fmt::{Display, Formatter};

#[derive(Debug, Clone)]
pub enum LexicalError {
    Generic {
        message: String,
    },
    UnexpectedRule {
        expected: Vec<Rule>,
        actual: Rule,
        location_line_col: (usize, usize),
    },
    IllegalCharacter {
        literal: String,
    },
    IllegalNumber {
        literal: String,
        message: String,
    },
    IllegalBytevector {
        literal: String,
        message: String,
    },
}

pub type Result<T> = std::result::Result<T, LexicalError>;

// Implement Display and Error

impl Display for LexicalError {
    fn fmt(&self, f: &mut Formatter) -> std::fmt::Result {
        match self {
            LexicalError::Generic { message } => write!(f, "Lexical error: {message}")?,
            LexicalError::UnexpectedRule { expected, actual, location_line_col: (line, col) } =>
                write!(f, "Lexical error: expected one of {expected:?}, got {actual:?} at line {line} column {col}")?,
            LexicalError::IllegalCharacter { literal } => {
                write!(f, "Lexical error: illegal character {literal:?}")?
            }
            LexicalError::IllegalNumber { literal, message } => {
                write!(f, "Lexical error: illegal number {literal:?}: {message}")?
            }
            LexicalError::IllegalBytevector { literal, message } => write!(
                f,
                "Lexical error: illegal bytevector {literal:?}: {message}"
            )?,
        }
        Ok(())
    }
}

impl Error for LexicalError {}

impl LexicalError {
    pub(crate) fn unexpected_rule(actual_pair: Pair<Rule>, expected: &[Rule]) -> Self {
        Self::UnexpectedRule {
            expected: expected.to_vec(),
            actual: actual_pair.as_rule(),
            location_line_col: actual_pair.line_col(),
        }
    }
}
