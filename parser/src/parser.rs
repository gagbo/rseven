// SPDX-FileCopyrightText: 2022-2023 Gerry Agbobada <git@gagbo.net>
//
// SPDX-License-Identifier: LGPL-3.0-only

//! Parser of R7RS grammar

#[cfg(test)]
mod tests;

mod convert;
use crate::{ast::Ast, error::Result, lexer::lex};

// TODO: Change signature to take the name of the
// source for content, so that:
// - when AST also contains line/column data for the tokens,
// - AST nodes actually also contain the name of their source.
//
// To avoid cloning a string in each and every node, instead
// the "root" of the AST only would contain the content-source name,
// and all other nodes could reach a reference to the source
// name with a method (so ast::Ast would stop being a simple
// alias to AstNode.)
//
// Another solution would be to have the Ast hold a reference to
// the content str:
// - fn parse(content: &'source str) -> Result<Ast<'source>>
// and then the Ast would have a reference to the content string,
// and the nodes would only contain offsets (line/col) relative
// to the reference of the content we have.
pub fn parse(content: &str) -> Result<Ast> {
    lex(content).and_then(convert::parse_lexed_tree)
}
