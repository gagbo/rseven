// SPDX-FileCopyrightText: 2022-2023 Gerry Agbobada <git@gagbo.net>
//
// SPDX-License-Identifier: LGPL-3.0-only

use super::{
    CompoundDatum, Definition, Identifier, Number, Quasiquotation, Sequence, StringLiteral,
    TransformerSpec,
};
use dbg_pls::DebugPls;
use std::collections::VecDeque;

#[derive(Clone, Debug, DebugPls, PartialEq)]
pub enum Expression {
    Ident(Identifier),
    Literal(Literal),
    ProcedureCall(ProcedureCall),
    Lambda(LambdaExpr),
    If(IfExpr),
    Assignment(Assignment),
    Cond(CondExpr),
    CaseWithSequenceFallback(CaseWithSequenceFallback),
    CaseWithRecipientFallback(CaseWithRecipientFallback),
    And(AndExpr),
    Or(OrExpr),
    When(WhenExpr),
    Unless(UnlessExpr),
    Let(LetExpr),
    NamedLet(NamedLetExpr),
    LetRec(LetRecExpr),
    LetValues(LetValuesExpr),
    BeginBlock(Sequence),
    DoLoop(DoLoopExpr),
    Delay(Box<Expression>),
    DelayForce(Box<Expression>),
    Parameterize(ParameterizeExpr),
    Guard(GuardExpr),
    Quasiquotation(Quasiquotation),
    CaseLambda(VecDeque<CaseLambdaClause>),
    MacroUse(MacroUse),
    MacroBlock(MacroBlock),
    Includer(Includer),
}

#[derive(Clone, Debug, DebugPls, PartialEq)]
pub enum Literal {
    Bool(bool),
    Num(Number),
    Vec(Vec<Literal>),
    Char(char),
    StringLiteral(StringLiteral),
    Bytevector(Vec<u8>),
    Compound(CompoundDatum),
    Assignment { label: usize, datum: Box<Literal> },
    Label(usize),
    Symbol(Identifier),
}

pub type Datum = Literal;

#[derive(Clone, Debug, DebugPls, PartialEq)]
pub struct ProcedureCall {
    pub operator: Box<Expression>,
    pub operands: VecDeque<Expression>,
}

#[derive(Clone, Debug, DebugPls, PartialEq)]
pub struct LambdaExpr {
    pub formals: Formals,
    pub body: Body,
}

#[derive(Clone, Debug, DebugPls, PartialEq)]
/// Conditional
pub struct IfExpr {
    pub test: Box<Expression>,
    pub consequent: Box<Expression>,
    pub alternate: Option<Box<Expression>>,
}

#[derive(Clone, Debug, DebugPls, PartialEq)]
pub struct Assignment {
    pub ident: Identifier,
    pub value: Box<Expression>,
}

#[derive(Clone, Debug, DebugPls, PartialEq)]
pub struct CondExpr {
    pub clauses: VecDeque<CondClause>,
    pub fallback: Option<Sequence>,
}

#[derive(Clone, Debug, DebugPls, PartialEq)]
pub struct CaseWithSequenceFallback {
    pub tested_value: Box<Expression>,
    pub clauses: VecDeque<CaseClause>,
    pub fallback: Option<Sequence>,
}

#[derive(Clone, Debug, DebugPls, PartialEq)]
/// The recipient is mandatory in the fallback here.
/// Use CaseWithSequenceFallback with fallback: None if
/// you don't have fallback
pub struct CaseWithRecipientFallback {
    pub tested_value: Box<Expression>,
    pub clauses: VecDeque<CaseClause>,
    pub fallback: Box<Expression>,
}

#[derive(Clone, Debug, DebugPls, PartialEq)]
pub struct AndExpr {
    pub tests: VecDeque<Expression>,
}

#[derive(Clone, Debug, DebugPls, PartialEq)]
pub struct OrExpr {
    pub tests: VecDeque<Expression>,
}

#[derive(Clone, Debug, DebugPls, PartialEq)]
pub struct WhenExpr {
    pub test: Box<Expression>,
    pub consequent: Sequence,
}

#[derive(Clone, Debug, DebugPls, PartialEq)]
pub struct UnlessExpr {
    pub test: Box<Expression>,
    pub consequent: Sequence,
}

#[derive(Clone, Debug, DebugPls, PartialEq)]
pub struct LetExpr {
    pub has_star: bool,
    pub bindings: VecDeque<BindingSpec>,
    pub body: Body,
}

#[derive(Clone, Debug, DebugPls, PartialEq)]
pub struct NamedLetExpr {
    pub has_star: bool,
    pub name: Identifier,
    pub bindings: VecDeque<BindingSpec>,
    pub body: Body,
}

#[derive(Clone, Debug, DebugPls, PartialEq)]
pub struct LetRecExpr {
    pub has_star: bool,
    pub bindings: VecDeque<BindingSpec>,
    pub body: Body,
}

#[derive(Clone, Debug, DebugPls, PartialEq)]
pub struct LetValuesExpr {
    pub has_star: bool,
    pub bindings: VecDeque<MultipleValuesBindingSpec>,
    pub body: Body,
}

#[derive(Clone, Debug, DebugPls, PartialEq)]
pub struct DoLoopExpr {
    pub iteration_specs: VecDeque<IterationSpec>,
    pub test: Box<Expression>,
    pub result: Option<Sequence>,
    pub commands: VecDeque<Expression>,
}

#[derive(Clone, Debug, DebugPls, PartialEq)]
pub struct ParameterizeExpr {
    pub parameters: VecDeque<(Expression, Expression)>,
    pub body: Body,
}

#[derive(Clone, Debug, DebugPls, PartialEq)]
pub struct GuardExpr {
    pub ident: Identifier,
    pub clauses: VecDeque<CondClause>,
    pub body: Body,
}

#[derive(Clone, Debug, DebugPls, PartialEq)]
pub struct MacroUse {
    pub keyword: Identifier,
    pub data: VecDeque<Literal>,
}

#[derive(Clone, Debug, DebugPls, PartialEq)]
pub struct MacroBlock {
    pub is_rec: bool,
    pub specs: VecDeque<SyntaxSpec>,
    pub body: Body,
}

#[derive(Clone, Debug, DebugPls, PartialEq)]
pub struct SyntaxSpec {
    pub keyword: Identifier,
    pub spec: TransformerSpec,
}

#[derive(Clone, Debug, DebugPls, PartialEq)]
pub struct IterationSpec {
    pub iterator: Identifier,
    pub init_value: Expression,
    pub step_value: Option<Expression>,
}

#[derive(Clone, Debug, DebugPls, PartialEq)]
pub struct BindingSpec {
    pub binding: Identifier,
    pub value: Expression,
}

#[derive(Clone, Debug, DebugPls, PartialEq)]
pub struct MultipleValuesBindingSpec {
    pub bindings: Formals,
    pub value: Expression,
}

#[derive(Clone, Debug, DebugPls, PartialEq)]
pub enum Formals {
    /// (<identifier>*)
    List(Vec<Identifier>),
    /// <identifier>
    Single(Identifier),
    /// (<identifier>+ . <identifier>)
    Dotted {
        head: Vec<Identifier>,
        last: Identifier,
    },
}

#[derive(Clone, Debug, DebugPls, PartialEq)]
pub struct Body {
    pub definitions: Vec<Definition>,
    pub sequence: Sequence,
}

#[derive(Clone, Debug, DebugPls, PartialEq)]
pub struct Includer {
    pub is_ci: bool,
    pub targets: Vec<StringLiteral>,
}

#[derive(Clone, Debug, DebugPls, PartialEq)]
pub struct CaseLambdaClause {
    pub formals: Formals,
    pub body: Body,
}

#[derive(Clone, Debug, DebugPls, PartialEq)]
pub enum CondClause {
    TestSeq {
        test: Expression,
        consequent: Sequence,
    },
    Test(Expression),
    TestRecipient {
        test: Expression,
        recipient: Expression,
    },
}

#[derive(Clone, Debug, DebugPls, PartialEq)]
pub enum CaseClause {
    Sequence {
        values: Vec<Literal>,
        consequent: Sequence,
    },
    Recipient {
        values: Vec<Literal>,
        recipient: Expression,
    },
}

// This conversion "desugars" Expression nodes back to
// Datum nodes. This is useful when something parsed as a
// procedure call needs to be parsed as a macro-use
// instead
impl From<Expression> for Datum {
    fn from(expr: Expression) -> Self {
        match expr {
            Expression::Ident(ident) => Self::Symbol(ident),
            Expression::Literal(inner) => inner,
            Expression::ProcedureCall(ProcedureCall { operator, operands }) => {
                let mut head = Vec::new();
                head.push((*operator).into());
                head.extend(operands.into_iter().map(Into::into));
                Self::Compound(CompoundDatum::List { head, tail: None })
            }
            Expression::Lambda(LambdaExpr { formals, body }) => {
                let mut head = Vec::new();
                head.push(Self::Symbol("lambda".to_string()));
                head.push(formals.into());
                todo!("Convert Body to Datum");
            }
            Expression::If(IfExpr {
                test,
                consequent,
                alternate,
            }) => todo!("Convert If to Datum"),
            Expression::Assignment(Assignment { ident, value }) => {
                todo!("Convert Assignment to Datum")
            }
            Expression::Cond(CondExpr { clauses, fallback }) => todo!("Convert Cond to Datum"),
            Expression::CaseWithSequenceFallback(CaseWithSequenceFallback {
                tested_value,
                clauses,
                fallback,
            }) => todo!("Convert CaseWithSequenceFallback to Datum"),
            Expression::CaseWithRecipientFallback(CaseWithRecipientFallback {
                tested_value,
                clauses,
                fallback,
            }) => todo!("Convert CaseWithRecipientFallback to Datum"),
            Expression::And(AndExpr { tests }) => todo!("Convert And to Datum"),
            Expression::Or(OrExpr { tests }) => todo!("Convert Or to Datum"),
            Expression::When(WhenExpr { test, consequent }) => todo!("Convert When to Datum"),
            Expression::Unless(UnlessExpr { test, consequent }) => todo!("Convert Unless to Datum"),
            Expression::Let(LetExpr {
                has_star,
                bindings,
                body,
            }) => todo!("Convert Let to Datum"),
            Expression::NamedLet(NamedLetExpr {
                has_star,
                name,
                bindings,
                body,
            }) => todo!("Convert NamedLet to Datum"),
            Expression::LetRec(LetRecExpr {
                has_star,
                bindings,
                body,
            }) => todo!("Convert LetRec to Datum"),
            Expression::LetValues(LetValuesExpr {
                has_star,
                bindings,
                body,
            }) => todo!("Convert LetValues to Datum"),
            Expression::BeginBlock(_) => todo!("Convert BeginBlock to Datum"),
            Expression::DoLoop(DoLoopExpr {
                iteration_specs,
                test,
                result,
                commands,
            }) => todo!("Convert DoLoop to Datum"),
            Expression::Delay(_) => todo!("Convert Delay to Datum"),
            Expression::DelayForce(_) => todo!("Convert DelayForce to Datum"),
            Expression::Parameterize(ParameterizeExpr { parameters, body }) => {
                todo!("Convert Parametrize to Datum")
            }
            Expression::Guard(GuardExpr {
                ident,
                clauses,
                body,
            }) => todo!("Convert Guard to Datum"),
            Expression::Quasiquotation(_) => todo!("Convert Quasiquotation to Datum"),
            Expression::CaseLambda(_) => todo!("Convert CaseLambda to Datum"),
            Expression::MacroUse(MacroUse { keyword, data }) => todo!("Convert MacroUse to Datum"),
            Expression::MacroBlock(MacroBlock {
                is_rec,
                specs,
                body,
            }) => todo!("Convert MacroBlock to Datum"),
            Expression::Includer(_) => todo!("Convert Includer to Datum"),
        }
    }
}

impl From<Formals> for Datum {
    fn from(formals: Formals) -> Self {
        match formals {
            Formals::List(list) => Self::Compound(CompoundDatum::List {
                head: list.into_iter().map(Self::Symbol).collect(),
                tail: None,
            }),
            Formals::Single(single) => Self::Symbol(single),
            Formals::Dotted { head, last } => Self::Compound(CompoundDatum::List {
                head: head.into_iter().map(Self::Symbol).collect(),
                tail: Some(Box::new(Self::Symbol(last))),
            }),
        }
    }
}
