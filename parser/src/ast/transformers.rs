// SPDX-FileCopyrightText: 2022-2023 Gerry Agbobada <git@gagbo.net>
//
// SPDX-License-Identifier: LGPL-3.0-only

use dbg_pls::DebugPls;

#[derive(Clone, Debug, DebugPls, PartialEq)]
pub enum TransformerSpec {
    // TODO(#C): Macro AST
}

#[derive(Clone, Debug, DebugPls, PartialEq)]
pub enum Pattern {
    // TODO(#C): Macro AST
}

#[derive(Clone, Debug, DebugPls, PartialEq)]
pub enum PatternData {
    // TODO(#C): Macro AST
}

#[derive(Clone, Debug, DebugPls, PartialEq)]
pub enum Template {
    // TODO(#C): Macro AST
}

#[derive(Clone, Debug, DebugPls, PartialEq)]
pub enum TemplateElement {
    // TODO(#C): Macro AST
}
