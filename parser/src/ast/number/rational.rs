// SPDX-FileCopyrightText: 2022-2023 Gerry Agbobada <git@gagbo.net>
//
// SPDX-License-Identifier: LGPL-3.0-only

//! Rational number type

use dbg_pls::DebugPls;

pub(crate) const ZERO_EXACT: Rational = Rational {
    numerator: 0,
    denominator: 1,
};

#[derive(Debug, DebugPls, Clone, Copy)]
pub struct Rational {
    pub(super) numerator: i128,
    pub(super) denominator: i128,
}

impl Rational {
    pub fn int(numerator: i128) -> Self {
        Self {
            numerator,
            denominator: 1,
        }
    }

    pub fn new(numerator: i128, denominator: i128) -> Self {
        if numerator == 0 {
            ZERO_EXACT
        } else {
            let gcd = gcd(numerator.abs(), denominator.abs());
            Self {
                numerator: numerator / gcd,
                denominator: denominator / gcd,
            }
        }
    }

    pub fn value(&self) -> f64 {
        self.numerator as f64 / self.denominator as f64
    }

    pub fn numerator(&self) -> i128 {
        self.numerator
    }

    pub fn denominator(&self) -> i128 {
        self.denominator
    }
}

// Euclid's two-thousand-year-old algorithm for finding the greatest common
// divisor.
fn gcd(x: i128, y: i128) -> i128 {
    assert!(x >= 0);
    assert!(y >= 0);
    let mut x = x;
    let mut y = y;
    while y != 0 {
        let t = y;
        y = x % y;
        x = t;
    }
    x
}

impl PartialEq for Rational {
    fn eq(&self, other: &Self) -> bool {
        ((self.numerator == other.numerator) && (self.denominator == other.denominator))
            || self
                .numerator
                .checked_mul(other.denominator)
                .map_or(false, |val| {
                    val == self
                        .denominator
                        .checked_mul(other.numerator)
                        .unwrap_or_default()
                })
    }
}

impl PartialOrd for Rational {
    fn partial_cmp(&self, other: &Self) -> Option<std::cmp::Ordering> {
        let res: Rational = *self - *other;
        res.numerator().partial_cmp(&0)
    }
}

impl Default for Rational {
    fn default() -> Self {
        ZERO_EXACT
    }
}

impl std::fmt::Display for Rational {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        if self.denominator() == 1 {
            write!(f, "{}", self.numerator())
        } else {
            write!(f, "{}/{}", self.numerator(), self.denominator())
        }
    }
}
