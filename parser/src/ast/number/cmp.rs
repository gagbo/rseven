// SPDX-FileCopyrightText: 2022-2023 Gerry Agbobada <git@gagbo.net>
//
// SPDX-License-Identifier: LGPL-3.0-only

//! Comparison traits implementations for numbers

use std::cmp::Ordering;

use super::{MaybeRational, MaybeReal, Number, NumberExact, NumberInexact};

impl std::cmp::PartialOrd<Number> for Number {
    fn partial_cmp(&self, other: &Number) -> Option<Ordering> {
        match (self, other) {
            (Number::Exact(ref s), Number::Exact(ref o)) => s.partial_cmp(o),
            (Number::Exact(ref s), Number::Inexact(ref o)) => s.partial_cmp(o),
            (Number::Inexact(ref s), Number::Exact(ref o)) => s.partial_cmp(o),
            (Number::Inexact(ref s), Number::Inexact(ref o)) => s.partial_cmp(o),
        }
    }
}

impl std::cmp::PartialEq<Number> for Number {
    fn eq(&self, other: &Number) -> bool {
        match (self, other) {
            (Number::Exact(s), Number::Exact(o)) => s.eq(o),
            (Number::Exact(s), Number::Inexact(o)) => s.eq(o),
            (Number::Inexact(s), Number::Exact(o)) => s.eq(o),
            (Number::Inexact(s), Number::Inexact(o)) => s.eq(o),
        }
    }
}

impl std::cmp::PartialOrd<MaybeRational> for MaybeRational {
    fn partial_cmp(&self, other: &MaybeRational) -> Option<Ordering> {
        match (self, other) {
            (MaybeRational::Nan, _)
            | (_, MaybeRational::Nan)
            | (MaybeRational::MinusInf, MaybeRational::MinusInf) => None,
            (MaybeRational::MinusInf, _)
            | (MaybeRational::Rational(_), MaybeRational::MinusInf) => Some(Ordering::Less),
            (MaybeRational::PlusInf, MaybeRational::PlusInf) => None,
            (MaybeRational::PlusInf, _) | (MaybeRational::Rational(_), MaybeRational::PlusInf) => {
                Some(Ordering::Greater)
            }
            (MaybeRational::Rational(left), MaybeRational::Rational(right)) => {
                left.partial_cmp(right)
            }
        }
    }
}

impl std::cmp::PartialEq<MaybeReal> for MaybeRational {
    fn eq(&self, other: &MaybeReal) -> bool {
        match (self, other) {
            (MaybeRational::Rational(left), MaybeReal::Real(right)) => left.value() == *right,
            (_, _) => false,
        }
    }
}

impl std::cmp::PartialOrd<MaybeReal> for MaybeRational {
    fn partial_cmp(&self, other: &MaybeReal) -> Option<Ordering> {
        match (self, other) {
            (MaybeRational::MinusInf, MaybeReal::MinusInf)
            | (_, MaybeReal::Nan)
            | (MaybeRational::Nan, _)
            | (MaybeRational::PlusInf, MaybeReal::PlusInf) => None,
            (MaybeRational::MinusInf, MaybeReal::PlusInf)
            | (MaybeRational::MinusInf, MaybeReal::Real(_))
            | (MaybeRational::Rational(_), MaybeReal::PlusInf) => Some(Ordering::Less),
            (MaybeRational::PlusInf, MaybeReal::MinusInf)
            | (MaybeRational::PlusInf, MaybeReal::Real(_))
            | (MaybeRational::Rational(_), MaybeReal::MinusInf) => Some(Ordering::Greater),
            (MaybeRational::Rational(left), MaybeReal::Real(right)) => {
                left.value().partial_cmp(right)
            }
        }
    }
}

impl std::cmp::PartialOrd<MaybeReal> for MaybeReal {
    fn partial_cmp(&self, other: &MaybeReal) -> Option<Ordering> {
        match (self, other) {
            (MaybeReal::MinusInf, MaybeReal::MinusInf)
            | (_, MaybeReal::Nan)
            | (MaybeReal::PlusInf, MaybeReal::PlusInf)
            | (MaybeReal::Nan, _) => None,
            (MaybeReal::MinusInf, _) | (MaybeReal::Real(_), MaybeReal::PlusInf) => {
                Some(Ordering::Less)
            }
            (MaybeReal::PlusInf, _) | (MaybeReal::Real(_), MaybeReal::MinusInf) => {
                Some(Ordering::Greater)
            }
            (MaybeReal::Real(left), MaybeReal::Real(right)) => left.partial_cmp(right),
        }
    }
}

impl std::cmp::PartialEq<MaybeRational> for MaybeReal {
    fn eq(&self, other: &MaybeRational) -> bool {
        match (self, other) {
            (MaybeReal::Real(left), MaybeRational::Rational(right)) => *left == right.value(),
            (_, _) => false,
        }
    }
}

impl std::cmp::PartialOrd<MaybeRational> for MaybeReal {
    fn partial_cmp(&self, other: &MaybeRational) -> Option<Ordering> {
        match (self, other) {
            (MaybeReal::MinusInf, MaybeRational::MinusInf)
            | (MaybeReal::PlusInf, MaybeRational::PlusInf)
            | (_, MaybeRational::Nan)
            | (MaybeReal::Nan, _) => None,
            (MaybeReal::MinusInf, _) | (MaybeReal::Real(_), MaybeRational::PlusInf) => {
                Some(Ordering::Less)
            }
            (MaybeReal::PlusInf, _) | (MaybeReal::Real(_), MaybeRational::MinusInf) => {
                Some(Ordering::Greater)
            }
            (MaybeReal::Real(left), MaybeRational::Rational(right)) => {
                left.partial_cmp(&right.value())
            }
        }
    }
}

impl std::cmp::PartialOrd<NumberExact> for NumberExact {
    fn partial_cmp(&self, other: &NumberExact) -> Option<Ordering> {
        if !self.is_real() || !other.is_real() {
            None
        } else {
            self.real.partial_cmp(&other.real)
        }
    }
}

impl std::cmp::PartialEq<NumberInexact> for NumberExact {
    fn eq(&self, other: &NumberInexact) -> bool {
        self.real == other.real && self.imag == other.imag
    }
}

impl std::cmp::PartialOrd<NumberInexact> for NumberExact {
    fn partial_cmp(&self, other: &NumberInexact) -> Option<Ordering> {
        if !self.is_real() || !other.is_real() {
            None
        } else {
            self.real.partial_cmp(&other.real)
        }
    }
}

impl std::cmp::PartialOrd<NumberInexact> for NumberInexact {
    fn partial_cmp(&self, other: &NumberInexact) -> Option<Ordering> {
        if !self.is_real() || !other.is_real() {
            None
        } else {
            self.real.partial_cmp(&other.real)
        }
    }
}

impl std::cmp::PartialEq<NumberExact> for NumberInexact {
    fn eq(&self, other: &NumberExact) -> bool {
        self.real == other.real && self.imag == other.imag
    }
}

impl std::cmp::PartialOrd<NumberExact> for NumberInexact {
    fn partial_cmp(&self, other: &NumberExact) -> Option<Ordering> {
        if !self.is_real() || !other.is_real() {
            None
        } else {
            self.real.partial_cmp(&other.real)
        }
    }
}
