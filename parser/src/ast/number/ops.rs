// SPDX-FileCopyrightText: 2022-2023 Gerry Agbobada <git@gagbo.net>
//
// SPDX-License-Identifier: LGPL-3.0-only

//! Arithmetic operations on numbers

use crate::Number;

use super::{MaybeRational, MaybeReal, NumberExact, NumberInexact, Rational, ZERO_EXACT};

impl std::ops::Div for Rational {
    // The division of rational numbers is a closed operation.
    type Output = Rational;

    fn div(self, rhs: Self) -> Self::Output {
        if rhs.numerator() == 0 {
            panic!("Cannot divide by zero-valued `Rational`!");
        }

        let numerator = self.numerator() * rhs.denominator();
        let denominator = self.denominator() * rhs.numerator();
        Rational::new(numerator, denominator)
    }
}

impl std::ops::Add for Rational {
    type Output = Rational;

    fn add(self, rhs: Self) -> Self::Output {
        let numerator = self.numerator() * rhs.denominator() + rhs.numerator() * self.denominator();
        let denominator = self.denominator() * rhs.denominator();
        Rational::new(numerator, denominator)
    }
}

impl std::ops::Sub for Rational {
    type Output = Rational;

    fn sub(self, rhs: Self) -> Self::Output {
        let numerator = self.numerator() * rhs.denominator() - rhs.numerator() * self.denominator();
        let denominator = self.denominator() * rhs.denominator();
        Rational::new(numerator, denominator)
    }
}

impl std::ops::Mul for Rational {
    type Output = Rational;

    fn mul(self, rhs: Self) -> Self::Output {
        let numerator = self.numerator() * rhs.numerator();
        let denominator = self.denominator() * rhs.denominator();
        Rational::new(numerator, denominator)
    }
}

impl std::ops::Mul<f64> for Rational {
    type Output = f64;

    fn mul(self, rhs: f64) -> Self::Output {
        let numerator = self.numerator() as f64 * rhs;
        let denominator = self.denominator();
        numerator / denominator as f64
    }
}

impl std::ops::Mul for MaybeRational {
    type Output = MaybeRational;
    fn mul(self, rhs: Self) -> <Self as std::ops::Mul<Self>>::Output {
        use MaybeRational::*;
        match (self, rhs) {
            (MinusInf, Rational(right)) => {
                if right < ZERO_EXACT {
                    PlusInf
                } else if right > ZERO_EXACT {
                    MinusInf
                } else {
                    Nan
                }
            }
            (PlusInf, MinusInf) | (MinusInf, PlusInf) => MinusInf,
            (PlusInf, PlusInf) | (MinusInf, MinusInf) => PlusInf,
            (PlusInf, Rational(right)) => {
                if right > ZERO_EXACT {
                    PlusInf
                } else if right < ZERO_EXACT {
                    MinusInf
                } else {
                    Nan
                }
            }
            (Rational(left), MinusInf) => {
                if left < ZERO_EXACT {
                    PlusInf
                } else if left > ZERO_EXACT {
                    MinusInf
                } else {
                    Nan
                }
            }
            (Rational(left), PlusInf) => {
                if left > ZERO_EXACT {
                    PlusInf
                } else if left < ZERO_EXACT {
                    MinusInf
                } else {
                    Nan
                }
            }
            (_, Nan) | (Nan, _) => Nan,
            (Rational(left), Rational(right)) => Rational(left * right),
        }
    }
}

impl std::ops::Add for MaybeRational {
    type Output = MaybeRational;

    fn add(self, rhs: Self) -> Self::Output {
        use MaybeRational::*;
        match (self, rhs) {
            (MinusInf, PlusInf) | (PlusInf, MinusInf) | (_, Nan) | (Nan, _) => Nan,
            (MinusInf, MinusInf) | (MinusInf, Rational(_)) | (Rational(_), MinusInf) => MinusInf,
            (PlusInf, PlusInf) | (PlusInf, Rational(_)) | (Rational(_), PlusInf) => PlusInf,
            (Rational(left), Rational(right)) => Rational(left + right),
        }
    }
}

impl std::ops::Sub for MaybeRational {
    type Output = MaybeRational;

    fn sub(self, rhs: Self) -> Self::Output {
        use MaybeRational::*;
        match (self, rhs) {
            (MinusInf, MinusInf) | (PlusInf, PlusInf) | (_, Nan) | (Nan, _) => Nan,
            (MinusInf, PlusInf) | (MinusInf, Rational(_)) | (Rational(_), PlusInf) => MinusInf,
            (PlusInf, MinusInf) | (PlusInf, Rational(_)) | (Rational(_), MinusInf) => PlusInf,
            (Rational(left), Rational(right)) => Rational(left - right),
        }
    }
}

impl std::ops::Div for MaybeRational {
    type Output = MaybeRational;

    fn div(self, rhs: Self) -> Self::Output {
        use MaybeRational::*;
        match (self, rhs) {
            (MinusInf, MinusInf)
            | (PlusInf, PlusInf)
            | (_, Nan)
            | (Nan, _)
            | (MinusInf, PlusInf)
            | (PlusInf, MinusInf) => Nan,
            (MinusInf, Rational(right)) => {
                if right > ZERO_EXACT {
                    MinusInf
                } else if right < ZERO_EXACT {
                    PlusInf
                } else {
                    Nan
                }
            }
            (Rational(_), PlusInf) | (Rational(_), MinusInf) => Rational(ZERO_EXACT),
            (PlusInf, Rational(right)) => {
                if right > ZERO_EXACT {
                    PlusInf
                } else if right < ZERO_EXACT {
                    MinusInf
                } else {
                    Nan
                }
            }
            (Rational(left), Rational(right)) => {
                if right == ZERO_EXACT {
                    return Nan;
                }
                Rational(left / right)
            }
        }
    }
}

impl std::ops::Mul<MaybeReal> for MaybeRational {
    type Output = MaybeReal;
    fn mul(self, rhs: MaybeReal) -> <Self as std::ops::Mul<MaybeReal>>::Output {
        match (self, rhs) {
            (MaybeRational::MinusInf, MaybeReal::Real(right)) => {
                if right < 0.0 {
                    MaybeReal::PlusInf
                } else if right > 0.0 {
                    MaybeReal::MinusInf
                } else {
                    MaybeReal::Nan
                }
            }
            (MaybeRational::PlusInf, MaybeReal::MinusInf)
            | (MaybeRational::MinusInf, MaybeReal::PlusInf) => MaybeReal::MinusInf,
            (MaybeRational::PlusInf, MaybeReal::PlusInf)
            | (MaybeRational::MinusInf, MaybeReal::MinusInf) => MaybeReal::PlusInf,
            (MaybeRational::PlusInf, MaybeReal::Real(right)) => {
                if right > 0.0 {
                    MaybeReal::PlusInf
                } else if right < 0.0 {
                    MaybeReal::MinusInf
                } else {
                    MaybeReal::Nan
                }
            }
            (MaybeRational::Rational(left), MaybeReal::MinusInf) => {
                if left < ZERO_EXACT {
                    MaybeReal::PlusInf
                } else if left > ZERO_EXACT {
                    MaybeReal::MinusInf
                } else {
                    MaybeReal::Nan
                }
            }
            (MaybeRational::Rational(left), MaybeReal::PlusInf) => {
                if left > ZERO_EXACT {
                    MaybeReal::PlusInf
                } else if left < ZERO_EXACT {
                    MaybeReal::MinusInf
                } else {
                    MaybeReal::Nan
                }
            }
            (_, MaybeReal::Nan) | (MaybeRational::Nan, _) => MaybeReal::Nan,
            (MaybeRational::Rational(left), MaybeReal::Real(right)) => {
                MaybeReal::Real(left * right)
            }
        }
    }
}

impl std::ops::Add<MaybeReal> for MaybeRational {
    type Output = MaybeReal;

    fn add(self, rhs: MaybeReal) -> Self::Output {
        match (self, rhs) {
            (MaybeRational::MinusInf, MaybeReal::PlusInf)
            | (_, MaybeReal::Nan)
            | (MaybeRational::PlusInf, MaybeReal::MinusInf)
            | (MaybeRational::Nan, _) => MaybeReal::Nan,
            (MaybeRational::MinusInf, MaybeReal::MinusInf)
            | (MaybeRational::MinusInf, MaybeReal::Real(_))
            | (MaybeRational::Rational(_), MaybeReal::MinusInf) => MaybeReal::MinusInf,
            (MaybeRational::PlusInf, MaybeReal::PlusInf)
            | (MaybeRational::PlusInf, MaybeReal::Real(_))
            | (MaybeRational::Rational(_), MaybeReal::PlusInf) => MaybeReal::PlusInf,
            (MaybeRational::Rational(left), MaybeReal::Real(right)) => {
                MaybeReal::Real(left.value() + right)
            }
        }
    }
}

impl std::ops::Sub<MaybeReal> for MaybeRational {
    type Output = MaybeReal;

    fn sub(self, rhs: MaybeReal) -> Self::Output {
        match (self, rhs) {
            (MaybeRational::MinusInf, MaybeReal::MinusInf)
            | (MaybeRational::PlusInf, MaybeReal::PlusInf)
            | (_, MaybeReal::Nan)
            | (MaybeRational::Nan, _) => MaybeReal::Nan,
            (MaybeRational::MinusInf, MaybeReal::PlusInf)
            | (MaybeRational::MinusInf, MaybeReal::Real(_))
            | (MaybeRational::Rational(_), MaybeReal::PlusInf) => MaybeReal::MinusInf,
            (MaybeRational::PlusInf, MaybeReal::MinusInf)
            | (MaybeRational::PlusInf, MaybeReal::Real(_))
            | (MaybeRational::Rational(_), MaybeReal::MinusInf) => MaybeReal::PlusInf,
            (MaybeRational::Rational(left), MaybeReal::Real(right)) => {
                MaybeReal::Real(left.value() - right)
            }
        }
    }
}

impl std::ops::Div<MaybeReal> for MaybeRational {
    type Output = MaybeReal;

    fn div(self, rhs: MaybeReal) -> Self::Output {
        match (self, rhs) {
            (MaybeRational::MinusInf, MaybeReal::MinusInf)
            | (MaybeRational::MinusInf, MaybeReal::PlusInf)
            | (_, MaybeReal::Nan)
            | (MaybeRational::PlusInf, MaybeReal::MinusInf)
            | (MaybeRational::PlusInf, MaybeReal::PlusInf)
            | (MaybeRational::Nan, _) => MaybeReal::Nan,
            (MaybeRational::PlusInf, MaybeReal::Real(right)) => {
                if right > 0.0 {
                    MaybeReal::PlusInf
                } else if right < 0.0 {
                    MaybeReal::MinusInf
                } else {
                    MaybeReal::Nan
                }
            }
            (MaybeRational::MinusInf, MaybeReal::Real(right)) => {
                if right > 0.0 {
                    MaybeReal::MinusInf
                } else if right < 0.0 {
                    MaybeReal::PlusInf
                } else {
                    MaybeReal::Nan
                }
            }
            (MaybeRational::Rational(left), MaybeReal::MinusInf) => {
                if left > ZERO_EXACT {
                    MaybeReal::MinusInf
                } else if left < ZERO_EXACT {
                    MaybeReal::PlusInf
                } else {
                    MaybeReal::Nan
                }
            }
            (MaybeRational::Rational(left), MaybeReal::PlusInf) => {
                if left > ZERO_EXACT {
                    MaybeReal::PlusInf
                } else if left < ZERO_EXACT {
                    MaybeReal::MinusInf
                } else {
                    MaybeReal::Nan
                }
            }
            (MaybeRational::Rational(left), MaybeReal::Real(right)) => {
                if right == 0.0 {
                    return MaybeReal::Nan;
                }
                MaybeReal::Real(left.value() / right)
            }
        }
    }
}

impl std::ops::Mul for MaybeReal {
    type Output = MaybeReal;
    fn mul(self, rhs: Self) -> <Self as std::ops::Mul<Self>>::Output {
        use MaybeReal::*;
        match (self, rhs) {
            (MinusInf, Real(right)) => {
                if right < 0.0 {
                    PlusInf
                } else if right > 0.0 {
                    MinusInf
                } else {
                    Nan
                }
            }
            (PlusInf, MinusInf) | (MinusInf, PlusInf) => MinusInf,
            (PlusInf, PlusInf) | (MinusInf, MinusInf) => PlusInf,
            (PlusInf, Real(right)) => {
                if right > 0.0 {
                    PlusInf
                } else if right < 0.0 {
                    MinusInf
                } else {
                    Nan
                }
            }
            (Real(left), MinusInf) => {
                if left < 0.0 {
                    PlusInf
                } else if left > 0.0 {
                    MinusInf
                } else {
                    Nan
                }
            }
            (Real(left), PlusInf) => {
                if left > 0.0 {
                    PlusInf
                } else if left < 0.0 {
                    MinusInf
                } else {
                    Nan
                }
            }
            (_, Nan) | (Nan, _) => Nan,
            (Real(left), Real(right)) => Real(left * right),
        }
    }
}

impl std::ops::Add for MaybeReal {
    type Output = MaybeReal;

    fn add(self, rhs: Self) -> Self::Output {
        use MaybeReal::*;
        match (self, rhs) {
            (MinusInf, PlusInf) | (PlusInf, MinusInf) | (_, Nan) | (Nan, _) => Nan,
            (MinusInf, MinusInf) | (MinusInf, Real(_)) | (Real(_), MinusInf) => MinusInf,
            (PlusInf, PlusInf) | (PlusInf, Real(_)) | (Real(_), PlusInf) => PlusInf,
            (Real(left), Real(right)) => Real(left + right),
        }
    }
}

impl std::ops::Sub for MaybeReal {
    type Output = MaybeReal;

    fn sub(self, rhs: Self) -> Self::Output {
        use MaybeReal::*;
        match (self, rhs) {
            (MinusInf, MinusInf) | (PlusInf, PlusInf) | (_, Nan) | (Nan, _) => Nan,
            (MinusInf, PlusInf) | (MinusInf, Real(_)) | (Real(_), PlusInf) => MinusInf,
            (PlusInf, MinusInf) | (PlusInf, Real(_)) | (Real(_), MinusInf) => PlusInf,
            (Real(left), Real(right)) => Real(left - right),
        }
    }
}

impl std::ops::Div for MaybeReal {
    type Output = MaybeReal;

    fn div(self, rhs: Self) -> Self::Output {
        use MaybeReal::*;
        match (self, rhs) {
            (MinusInf, MinusInf)
            | (PlusInf, PlusInf)
            | (_, Nan)
            | (Nan, _)
            | (MinusInf, PlusInf)
            | (PlusInf, MinusInf) => Nan,
            (MinusInf, Real(right)) => {
                if right > 0.0 {
                    MinusInf
                } else if right < 0.0 {
                    PlusInf
                } else {
                    Nan
                }
            }
            (Real(_), PlusInf) | (Real(_), MinusInf) => Real(0.0),
            (PlusInf, Real(right)) => {
                if right > 0.0 {
                    PlusInf
                } else if right < 0.0 {
                    MinusInf
                } else {
                    Nan
                }
            }
            (Real(left), Real(right)) => {
                if right == 0.0 {
                    return Nan;
                }
                Real(left / right)
            }
        }
    }
}

impl std::ops::Mul<MaybeRational> for MaybeReal {
    type Output = MaybeReal;
    fn mul(self, rhs: MaybeRational) -> <Self as std::ops::Mul<MaybeRational>>::Output {
        match (self, rhs) {
            (MaybeReal::MinusInf, MaybeRational::Rational(right)) => {
                if right < ZERO_EXACT {
                    MaybeReal::PlusInf
                } else if right > ZERO_EXACT {
                    MaybeReal::MinusInf
                } else {
                    MaybeReal::Nan
                }
            }
            (MaybeReal::PlusInf, MaybeRational::MinusInf)
            | (MaybeReal::MinusInf, MaybeRational::PlusInf) => MaybeReal::MinusInf,
            (MaybeReal::PlusInf, MaybeRational::PlusInf)
            | (MaybeReal::MinusInf, MaybeRational::MinusInf) => MaybeReal::PlusInf,
            (MaybeReal::PlusInf, MaybeRational::Rational(right)) => {
                if right > ZERO_EXACT {
                    MaybeReal::PlusInf
                } else if right < ZERO_EXACT {
                    MaybeReal::MinusInf
                } else {
                    MaybeReal::Nan
                }
            }
            (MaybeReal::Real(left), MaybeRational::MinusInf) => {
                if left < 0.0 {
                    MaybeReal::PlusInf
                } else if left > 0.0 {
                    MaybeReal::MinusInf
                } else {
                    MaybeReal::Nan
                }
            }
            (MaybeReal::Real(left), MaybeRational::PlusInf) => {
                if left > 0.0 {
                    MaybeReal::PlusInf
                } else if left < 0.0 {
                    MaybeReal::MinusInf
                } else {
                    MaybeReal::Nan
                }
            }
            (_, MaybeRational::Nan) | (MaybeReal::Nan, _) => MaybeReal::Nan,
            (MaybeReal::Real(left), MaybeRational::Rational(right)) => {
                MaybeReal::Real(left * right.value())
            }
        }
    }
}

impl std::ops::Add<MaybeRational> for MaybeReal {
    type Output = MaybeReal;

    fn add(self, rhs: MaybeRational) -> Self::Output {
        match (self, rhs) {
            (MaybeReal::MinusInf, MaybeRational::PlusInf)
            | (_, MaybeRational::Nan)
            | (MaybeReal::PlusInf, MaybeRational::MinusInf)
            | (MaybeReal::Nan, _) => MaybeReal::Nan,
            (MaybeReal::MinusInf, MaybeRational::MinusInf)
            | (MaybeReal::MinusInf, MaybeRational::Rational(_))
            | (MaybeReal::Real(_), MaybeRational::MinusInf) => MaybeReal::MinusInf,
            (MaybeReal::PlusInf, MaybeRational::PlusInf)
            | (MaybeReal::PlusInf, MaybeRational::Rational(_))
            | (MaybeReal::Real(_), MaybeRational::PlusInf) => MaybeReal::PlusInf,
            (MaybeReal::Real(left), MaybeRational::Rational(right)) => {
                MaybeReal::Real(left + right.value())
            }
        }
    }
}

impl std::ops::Sub<MaybeRational> for MaybeReal {
    type Output = MaybeReal;

    fn sub(self, rhs: MaybeRational) -> Self::Output {
        match (self, rhs) {
            (MaybeReal::MinusInf, MaybeRational::MinusInf)
            | (MaybeReal::PlusInf, MaybeRational::PlusInf)
            | (_, MaybeRational::Nan)
            | (MaybeReal::Nan, _) => MaybeReal::Nan,
            (MaybeReal::MinusInf, MaybeRational::PlusInf)
            | (MaybeReal::MinusInf, MaybeRational::Rational(_))
            | (MaybeReal::Real(_), MaybeRational::PlusInf) => MaybeReal::MinusInf,
            (MaybeReal::PlusInf, MaybeRational::MinusInf)
            | (MaybeReal::PlusInf, MaybeRational::Rational(_))
            | (MaybeReal::Real(_), MaybeRational::MinusInf) => MaybeReal::PlusInf,
            (MaybeReal::Real(left), MaybeRational::Rational(right)) => {
                MaybeReal::Real(left - right.value())
            }
        }
    }
}

impl std::ops::Div<MaybeRational> for MaybeReal {
    type Output = MaybeReal;

    fn div(self, rhs: MaybeRational) -> Self::Output {
        match (self, rhs) {
            (MaybeReal::MinusInf, MaybeRational::MinusInf)
            | (MaybeReal::MinusInf, MaybeRational::PlusInf)
            | (_, MaybeRational::Nan)
            | (MaybeReal::PlusInf, MaybeRational::MinusInf)
            | (MaybeReal::PlusInf, MaybeRational::PlusInf)
            | (MaybeReal::Nan, _) => MaybeReal::Nan,
            (MaybeReal::PlusInf, MaybeRational::Rational(right)) => {
                if right > ZERO_EXACT {
                    MaybeReal::PlusInf
                } else if right < ZERO_EXACT {
                    MaybeReal::MinusInf
                } else {
                    MaybeReal::Nan
                }
            }
            (MaybeReal::MinusInf, MaybeRational::Rational(right)) => {
                if right > ZERO_EXACT {
                    MaybeReal::MinusInf
                } else if right < ZERO_EXACT {
                    MaybeReal::PlusInf
                } else {
                    MaybeReal::Nan
                }
            }
            (MaybeReal::Real(left), MaybeRational::MinusInf) => {
                if left > 0.0 {
                    MaybeReal::MinusInf
                } else if left < 0.0 {
                    MaybeReal::PlusInf
                } else {
                    MaybeReal::Nan
                }
            }
            (MaybeReal::Real(left), MaybeRational::PlusInf) => {
                if left > 0.0 {
                    MaybeReal::PlusInf
                } else if left < 0.0 {
                    MaybeReal::MinusInf
                } else {
                    MaybeReal::Nan
                }
            }
            (MaybeReal::Real(left), MaybeRational::Rational(right)) => {
                if right == ZERO_EXACT {
                    return MaybeReal::Nan;
                }
                MaybeReal::Real(left / right.value())
            }
        }
    }
}

impl std::ops::Mul for NumberExact {
    type Output = NumberExact;

    fn mul(self, rhs: Self) -> Self::Output {
        Self {
            real: (self.real * rhs.real) - (self.imag * rhs.imag),
            imag: (self.real * rhs.imag) + (self.imag * rhs.real),
        }
    }
}

impl std::ops::Div for NumberExact {
    type Output = NumberExact;

    fn div(self, rhs: Self) -> Self::Output {
        if !rhs.is_real() {
            panic!("Cannot divide by a complex number")
        }
        Self {
            real: self.real / rhs.real,
            imag: self.imag / rhs.real,
        }
    }
}

impl std::ops::Add for NumberExact {
    type Output = NumberExact;

    fn add(self, rhs: Self) -> Self::Output {
        Self {
            real: self.real + rhs.real,
            imag: self.imag + rhs.imag,
        }
    }
}

impl std::ops::Sub for NumberExact {
    type Output = NumberExact;

    fn sub(self, rhs: Self) -> Self::Output {
        Self {
            real: self.real - rhs.real,
            imag: self.imag - rhs.imag,
        }
    }
}

impl std::ops::Mul<NumberInexact> for NumberExact {
    type Output = NumberInexact;

    fn mul(self, rhs: NumberInexact) -> Self::Output {
        NumberInexact {
            real: (self.real * rhs.real) - (self.imag * rhs.imag),
            imag: (self.real * rhs.imag) + (self.imag * rhs.real),
        }
    }
}

impl std::ops::Div<NumberInexact> for NumberExact {
    type Output = NumberInexact;

    fn div(self, rhs: NumberInexact) -> Self::Output {
        if !rhs.is_real() {
            panic!("Cannot divide by a complex number")
        }
        NumberInexact {
            real: self.real / rhs.real,
            imag: self.imag / rhs.real,
        }
    }
}

impl std::ops::Add<NumberInexact> for NumberExact {
    type Output = NumberInexact;

    fn add(self, rhs: NumberInexact) -> Self::Output {
        NumberInexact {
            real: self.real + rhs.real,
            imag: self.imag + rhs.imag,
        }
    }
}

impl std::ops::Sub<NumberInexact> for NumberExact {
    type Output = NumberInexact;

    fn sub(self, rhs: NumberInexact) -> Self::Output {
        NumberInexact {
            real: self.real - rhs.real,
            imag: self.imag - rhs.imag,
        }
    }
}

impl std::ops::Mul for NumberInexact {
    type Output = NumberInexact;

    fn mul(self, rhs: Self) -> Self::Output {
        Self {
            real: (self.real * rhs.real) - (self.imag * rhs.imag),
            imag: (self.real * rhs.imag) + (self.imag * rhs.real),
        }
    }
}

impl std::ops::Div for NumberInexact {
    type Output = NumberInexact;

    fn div(self, rhs: Self) -> Self::Output {
        if !rhs.is_real() {
            panic!("Cannot divide by a complex number")
        }
        Self {
            real: self.real / rhs.real,
            imag: self.imag / rhs.real,
        }
    }
}

impl std::ops::Add for NumberInexact {
    type Output = NumberInexact;

    fn add(self, rhs: Self) -> Self::Output {
        Self {
            real: self.real + rhs.real,
            imag: self.imag + rhs.imag,
        }
    }
}

impl std::ops::Sub for NumberInexact {
    type Output = NumberInexact;

    fn sub(self, rhs: Self) -> Self::Output {
        Self {
            real: self.real - rhs.real,
            imag: self.imag - rhs.imag,
        }
    }
}

impl std::ops::Mul<NumberExact> for NumberInexact {
    type Output = NumberInexact;

    fn mul(self, rhs: NumberExact) -> Self::Output {
        Self {
            real: (self.real * rhs.real) - (self.imag * rhs.imag),
            imag: (self.real * rhs.imag) + (self.imag * rhs.real),
        }
    }
}

impl std::ops::Div<NumberExact> for NumberInexact {
    type Output = NumberInexact;

    fn div(self, rhs: NumberExact) -> Self::Output {
        if !rhs.is_real() {
            panic!("Cannot divide by a complex number")
        }
        Self {
            real: self.real / rhs.real,
            imag: self.imag / rhs.real,
        }
    }
}

impl std::ops::Add<NumberExact> for NumberInexact {
    type Output = NumberInexact;

    fn add(self, rhs: NumberExact) -> Self::Output {
        Self {
            real: self.real + rhs.real,
            imag: self.imag + rhs.imag,
        }
    }
}

impl std::ops::Sub<NumberExact> for NumberInexact {
    type Output = NumberInexact;

    fn sub(self, rhs: NumberExact) -> Self::Output {
        Self {
            real: self.real - rhs.real,
            imag: self.imag - rhs.imag,
        }
    }
}

impl std::ops::Mul for Number {
    type Output = Number;

    fn mul(self, rhs: Self) -> Self::Output {
        match (self, rhs) {
            (Number::Exact(s), Number::Exact(r)) => Number::Exact(s * r),
            (Number::Exact(s), Number::Inexact(r)) => Number::Inexact(s * r),
            (Number::Inexact(s), Number::Exact(r)) => Number::Inexact(s * r),
            (Number::Inexact(s), Number::Inexact(r)) => Number::Inexact(s * r),
        }
    }
}

impl std::ops::Div for Number {
    type Output = Number;

    fn div(self, rhs: Self) -> Self::Output {
        if !rhs.is_real() {
            panic!("Cannot divide by a complex number")
        }
        match (self, rhs) {
            (Number::Exact(s), Number::Exact(r)) => Number::Exact(s / r),
            (Number::Exact(s), Number::Inexact(r)) => Number::Inexact(s / r),
            (Number::Inexact(s), Number::Exact(r)) => Number::Inexact(s / r),
            (Number::Inexact(s), Number::Inexact(r)) => Number::Inexact(s / r),
        }
    }
}

impl std::ops::Add for Number {
    type Output = Number;

    fn add(self, rhs: Self) -> Self::Output {
        match (self, rhs) {
            (Number::Exact(s), Number::Exact(r)) => Number::Exact(s + r),
            (Number::Exact(s), Number::Inexact(r)) => Number::Inexact(s + r),
            (Number::Inexact(s), Number::Exact(r)) => Number::Inexact(s + r),
            (Number::Inexact(s), Number::Inexact(r)) => Number::Inexact(s + r),
        }
    }
}

impl std::ops::Sub for Number {
    type Output = Number;

    fn sub(self, rhs: Self) -> Self::Output {
        match (self, rhs) {
            (Number::Exact(s), Number::Exact(r)) => Number::Exact(s - r),
            (Number::Exact(s), Number::Inexact(r)) => Number::Inexact(s - r),
            (Number::Inexact(s), Number::Exact(r)) => Number::Inexact(s - r),
            (Number::Inexact(s), Number::Inexact(r)) => Number::Inexact(s - r),
        }
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn additions() {
        assert_eq!(
            MaybeReal::Real(2.0) + MaybeReal::Real(3.5),
            MaybeReal::Real(5.5)
        );
        assert_eq!(
            MaybeReal::Real(3.5) + MaybeReal::Real(2.0),
            MaybeReal::Real(5.5)
        );
        assert_eq!(
            MaybeReal::MinusInf + MaybeReal::Real(3.5),
            MaybeReal::MinusInf
        );
        assert_eq!(
            MaybeReal::Real(3.5) + MaybeReal::MinusInf,
            MaybeReal::MinusInf
        );
        assert_eq!(
            MaybeReal::PlusInf + MaybeReal::Real(3.5),
            MaybeReal::PlusInf
        );
        assert_eq!(
            MaybeReal::Real(3.5) + MaybeReal::PlusInf,
            MaybeReal::PlusInf
        );
        assert_eq!(MaybeReal::Nan + MaybeReal::Real(3.5), MaybeReal::Nan);
        assert_eq!(MaybeReal::Real(3.5) + MaybeReal::Nan, MaybeReal::Nan);
        assert_eq!(MaybeReal::MinusInf + MaybeReal::PlusInf, MaybeReal::Nan);
        assert_eq!(MaybeReal::PlusInf + MaybeReal::MinusInf, MaybeReal::Nan);
        assert_eq!(
            MaybeReal::MinusInf + MaybeReal::MinusInf,
            MaybeReal::MinusInf
        );
        assert_eq!(MaybeReal::PlusInf + MaybeReal::PlusInf, MaybeReal::PlusInf);
    }

    #[allow(clippy::eq_op)]
    #[test]
    fn substractions() {
        assert_eq!(
            MaybeReal::Real(2.0) - MaybeReal::Real(3.5),
            MaybeReal::Real(-1.5)
        );
        assert_eq!(
            MaybeReal::Real(3.5) - MaybeReal::Real(2.0),
            MaybeReal::Real(1.5)
        );
        assert_eq!(
            MaybeReal::MinusInf - MaybeReal::Real(3.5),
            MaybeReal::MinusInf
        );
        assert_eq!(
            MaybeReal::Real(3.5) - MaybeReal::MinusInf,
            MaybeReal::PlusInf
        );
        assert_eq!(
            MaybeReal::PlusInf - MaybeReal::Real(3.5),
            MaybeReal::PlusInf
        );
        assert_eq!(
            MaybeReal::Real(3.5) - MaybeReal::PlusInf,
            MaybeReal::MinusInf
        );
        assert_eq!(MaybeReal::Nan - MaybeReal::Real(3.5), MaybeReal::Nan);
        assert_eq!(MaybeReal::Real(3.5) - MaybeReal::Nan, MaybeReal::Nan);
        assert_eq!(
            MaybeReal::MinusInf - MaybeReal::PlusInf,
            MaybeReal::MinusInf
        );
        assert_eq!(MaybeReal::PlusInf - MaybeReal::MinusInf, MaybeReal::PlusInf);
        assert_eq!(MaybeReal::MinusInf - MaybeReal::MinusInf, MaybeReal::Nan);
        assert_eq!(MaybeReal::PlusInf - MaybeReal::PlusInf, MaybeReal::Nan);
    }

    #[test]
    fn multiplications() {
        assert_eq!(
            MaybeReal::Real(2.0) * MaybeReal::Real(3.5),
            MaybeReal::Real(7.0)
        );
        assert_eq!(
            MaybeReal::Real(3.5) * MaybeReal::Real(2.0),
            MaybeReal::Real(7.0)
        );
        assert_eq!(
            MaybeReal::MinusInf * MaybeReal::Real(3.5),
            MaybeReal::MinusInf
        );
        assert_eq!(
            MaybeReal::Real(3.5) * MaybeReal::MinusInf,
            MaybeReal::MinusInf
        );
        assert_eq!(
            MaybeReal::PlusInf * MaybeReal::Real(3.5),
            MaybeReal::PlusInf
        );
        assert_eq!(
            MaybeReal::Real(3.5) * MaybeReal::PlusInf,
            MaybeReal::PlusInf
        );
        assert_eq!(
            MaybeReal::MinusInf * MaybeReal::Real(-3.5),
            MaybeReal::PlusInf
        );
        assert_eq!(
            MaybeReal::Real(-3.5) * MaybeReal::MinusInf,
            MaybeReal::PlusInf
        );
        assert_eq!(
            MaybeReal::PlusInf * MaybeReal::Real(-3.5),
            MaybeReal::MinusInf
        );
        assert_eq!(
            MaybeReal::Real(-3.5) * MaybeReal::PlusInf,
            MaybeReal::MinusInf
        );
        assert_eq!(MaybeReal::Nan * MaybeReal::Real(3.5), MaybeReal::Nan);
        assert_eq!(MaybeReal::Real(3.5) * MaybeReal::Nan, MaybeReal::Nan);
        assert_eq!(MaybeReal::PlusInf * MaybeReal::Real(0.0), MaybeReal::Nan);
        assert_eq!(MaybeReal::Real(0.0) * MaybeReal::PlusInf, MaybeReal::Nan);
        assert_eq!(MaybeReal::MinusInf * MaybeReal::Real(0.0), MaybeReal::Nan);
        assert_eq!(MaybeReal::Real(0.0) * MaybeReal::MinusInf, MaybeReal::Nan);
        assert_eq!(
            MaybeReal::MinusInf * MaybeReal::PlusInf,
            MaybeReal::MinusInf
        );
        assert_eq!(
            MaybeReal::PlusInf * MaybeReal::MinusInf,
            MaybeReal::MinusInf
        );
        assert_eq!(
            MaybeReal::MinusInf * MaybeReal::MinusInf,
            MaybeReal::PlusInf
        );
        assert_eq!(MaybeReal::PlusInf * MaybeReal::PlusInf, MaybeReal::PlusInf);
    }

    #[allow(clippy::eq_op)]
    #[test]
    fn divisions() {
        assert_eq!(
            MaybeReal::Real(2.0) / MaybeReal::Real(3.5),
            MaybeReal::Real(4.0 / 7.0)
        );
        assert_eq!(
            MaybeReal::Real(3.5) / MaybeReal::Real(2.0),
            MaybeReal::Real(1.75)
        );
        assert_eq!(
            MaybeReal::MinusInf / MaybeReal::Real(3.5),
            MaybeReal::MinusInf
        );
        assert_eq!(
            MaybeReal::Real(3.5) / MaybeReal::MinusInf,
            MaybeReal::Real(0.0)
        );
        assert_eq!(
            MaybeReal::PlusInf / MaybeReal::Real(3.5),
            MaybeReal::PlusInf
        );
        assert_eq!(
            MaybeReal::Real(3.5) / MaybeReal::PlusInf,
            MaybeReal::Real(0.0)
        );
        assert_eq!(MaybeReal::Nan / MaybeReal::Real(3.5), MaybeReal::Nan);
        assert_eq!(MaybeReal::Real(3.5) / MaybeReal::Nan, MaybeReal::Nan);
        assert_eq!(MaybeReal::MinusInf / MaybeReal::PlusInf, MaybeReal::Nan);
        assert_eq!(MaybeReal::PlusInf / MaybeReal::MinusInf, MaybeReal::Nan);
        assert_eq!(MaybeReal::MinusInf / MaybeReal::MinusInf, MaybeReal::Nan);
        assert_eq!(MaybeReal::PlusInf / MaybeReal::PlusInf, MaybeReal::Nan);
    }
}
