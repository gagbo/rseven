// SPDX-FileCopyrightText: 2022-2023 Gerry Agbobada <git@gagbo.net>
//
// SPDX-License-Identifier: LGPL-3.0-only

use dbg_pls::DebugPls;

use super::{CommandOrDef, Identifier, Includer, StringLiteral};

#[derive(Clone, Debug, DebugPls, PartialEq)]
pub struct Library {
    /// A list of (identifier | usize)
    pub name: LibraryName,
    pub statements: Vec<LibraryDeclaration>,
}

pub type LibraryName = Vec<LibraryNamePart>;

#[derive(Clone, Debug, DebugPls, PartialEq)]
pub enum LibraryNamePart {
    Ident(Identifier),
    Num(usize),
}

#[derive(Clone, Debug, DebugPls, PartialEq)]
pub enum ExportSpec {
    Single(Identifier),
    Renamed {
        inner_name: Identifier,
        new_name: Identifier,
    },
}

#[derive(Clone, Debug, DebugPls, PartialEq)]
pub struct CondExpand {
    pub clauses: Vec<CondExpandClause>,
    pub fallback: Option<Vec<LibraryDeclaration>>,
}

#[derive(Clone, Debug, DebugPls, PartialEq)]
pub enum LibraryDeclaration {
    Export(Vec<ExportSpec>),
    Import(Vec<ImportSet>),
    BeginBlock(Vec<CommandOrDef>),
    Includer(Includer),
    IncludeLibraryDeclarations(Vec<StringLiteral>),
    CondExpand(CondExpand),
}

#[derive(Clone, Debug, DebugPls, PartialEq)]
pub struct CondExpandClause {
    pub requirement: FeatureRequirement,
    pub statements: Vec<LibraryDeclaration>,
}

#[derive(Clone, Debug, DebugPls, PartialEq)]
pub enum FeatureRequirement {
    HasFeature(Identifier),
    HasLibrary(LibraryName),
    And(Vec<FeatureRequirement>),
    Or(Vec<FeatureRequirement>),
    Not(Vec<FeatureRequirement>),
}

#[derive(Clone, Debug, DebugPls, PartialEq)]
pub enum ImportSet {
    Bare(LibraryName),
    Only(OnlyImportSet),
    Except(ExceptImportSet),
    Prefix(PrefixImportSet),
    Rename(RenameImportSet),
}

#[derive(Clone, Debug, DebugPls, PartialEq)]
pub struct OnlyImportSet {
    pub set: Box<ImportSet>,
    pub only: Vec<Identifier>,
}

#[derive(Clone, Debug, DebugPls, PartialEq)]
pub struct ExceptImportSet {
    pub set: Box<ImportSet>,
    pub except: Vec<Identifier>,
}

#[derive(Clone, Debug, DebugPls, PartialEq)]
pub struct PrefixImportSet {
    pub set: Box<ImportSet>,
    pub prefix: Identifier,
}

#[derive(Clone, Debug, DebugPls, PartialEq)]
pub struct RenameImportSet {
    pub set: Box<ImportSet>,
    pub old_new_pairs: Vec<(Identifier, Identifier)>,
}
