// SPDX-FileCopyrightText: 2022-2023 Gerry Agbobada <git@gagbo.net>
//
// SPDX-License-Identifier: LGPL-3.0-only

//! Helper types for numbers

mod cmp;
mod ops;
mod rational;

use std::fmt::Display;

use dbg_pls::DebugPls;
pub use rational::Rational;
pub(crate) use rational::ZERO_EXACT;

#[derive(Clone, Debug, DebugPls, Copy)]
pub enum Number {
    Exact(NumberExact),
    Inexact(NumberInexact),
}

impl Number {
    pub fn exact_int(integer: i128) -> Self {
        NumberExact::new(integer, 0).into()
    }

    pub fn exact_fraction<N, D>(numerator: N, denominator: D) -> Self
    where
        N: Into<i128>,
        D: Into<i128>,
    {
        NumberExact::new((numerator, denominator), 0).into()
    }

    pub fn exact_complex<Nr, Dr, Ni, Di>(real: (Nr, Dr), imag: (Ni, Di)) -> Self
    where
        Nr: Into<i128>,
        Dr: Into<i128>,
        Ni: Into<i128>,
        Di: Into<i128>,
    {
        NumberExact::new(real, imag).into()
    }

    pub fn inexact_complex<R, I>(real: R, imag: I) -> Self
    where
        R: Into<f64>,
        I: Into<f64>,
    {
        NumberInexact::new(real, imag).into()
    }

    pub fn inexact_real<R>(real: R) -> Self
    where
        R: Into<f64>,
    {
        NumberInexact::new(real, 0.0_f64).into()
    }

    pub fn is_nan(&self) -> bool {
        match &self {
            Self::Exact(e) => e.is_nan(),
            Self::Inexact(i) => i.is_nan(),
        }
    }
    pub fn is_finite(&self) -> bool {
        match &self {
            Self::Exact(e) => e.is_finite(),
            Self::Inexact(i) => i.is_finite(),
        }
    }
    pub fn is_exact(&self) -> bool {
        match &self {
            Self::Exact(e) => e.is_exact(),
            Self::Inexact(i) => i.is_exact(),
        }
    }
    pub fn is_real(&self) -> bool {
        match &self {
            Self::Exact(e) => e.is_real(),
            Self::Inexact(i) => i.is_real(),
        }
    }
}

impl From<NumberExact> for Number {
    fn from(v: NumberExact) -> Self {
        Self::Exact(v)
    }
}

impl From<NumberInexact> for Number {
    fn from(v: NumberInexact) -> Self {
        Self::Inexact(v)
    }
}

/// Representation of an exact number value
#[derive(Debug, DebugPls, Clone, Copy, Default, PartialEq)]
pub struct NumberExact {
    pub(crate) real: MaybeRational,
    pub(crate) imag: MaybeRational,
}

impl NumberExact {
    pub fn new<R, I>(real: R, imag: I) -> Self
    where
        R: Into<MaybeRational>,
        I: Into<MaybeRational>,
    {
        Self {
            real: real.into(),
            imag: imag.into(),
        }
    }
}

impl NumberExact {
    pub fn is_nan(&self) -> bool {
        matches!(self.real, MaybeRational::Nan) || matches!(self.imag, MaybeRational::Nan)
    }
    pub fn is_finite(&self) -> bool {
        matches!(self.real, MaybeRational::Rational(_))
            && matches!(self.imag, MaybeRational::Rational(_))
    }
    pub fn is_exact(&self) -> bool {
        true
    }
    pub fn is_real(&self) -> bool {
        (!matches!(self.real, MaybeRational::Nan))
            && matches!(self.imag, MaybeRational::Rational(val) if val == ZERO_EXACT)
    }
}

/// Representation of an inexact number value
#[derive(Debug, DebugPls, Clone, Copy, Default, PartialEq)]
pub struct NumberInexact {
    pub(crate) real: MaybeReal,
    pub(crate) imag: MaybeReal,
}

impl NumberInexact {
    pub fn new<R, I>(real: R, imag: I) -> Self
    where
        R: Into<MaybeReal>,
        I: Into<MaybeReal>,
    {
        Self {
            real: real.into(),
            imag: imag.into(),
        }
    }
}

impl NumberInexact {
    pub fn is_nan(&self) -> bool {
        matches!(self.real, MaybeReal::Nan) || matches!(self.imag, MaybeReal::Nan)
    }
    pub fn is_finite(&self) -> bool {
        matches!(self.real, MaybeReal::Real(_)) && matches!(self.imag, MaybeReal::Real(_))
    }
    pub fn is_exact(&self) -> bool {
        false
    }
    pub fn is_real(&self) -> bool {
        match self.real {
            MaybeReal::MinusInf | MaybeReal::PlusInf => {
                matches!(self.imag, MaybeReal::Real(val) if val.abs() < f64::EPSILON)
            }
            MaybeReal::Nan => false,
            MaybeReal::Real(r) => {
                if r == 0.0 {
                    matches!(self.imag, MaybeReal::Real(i) if i == 0.0)
                } else {
                    matches!(self.imag, MaybeReal::Real(i) if (i/r).abs() < f64::EPSILON)
                }
            }
        }
    }
}

#[derive(Debug, DebugPls, Clone, Copy, PartialEq)]
pub enum MaybeRational {
    MinusInf,
    PlusInf,
    Nan,
    Rational(Rational),
}

impl MaybeRational {
    // Return the int value if relevant
    pub fn int(&self) -> Option<i128> {
        match self {
            MaybeRational::Rational(rat) => {
                if rat.denominator() == 1 {
                    Some(rat.numerator())
                } else {
                    None
                }
            }
            _ => None,
        }
    }
}

impl From<i128> for MaybeRational {
    fn from(value: i128) -> Self {
        Self::Rational(Rational::int(value))
    }
}

impl<N, D> From<(N, D)> for MaybeRational
where
    N: Into<i128>,
    D: Into<i128>,
{
    fn from((numerator, denominator): (N, D)) -> Self {
        Self::Rational(Rational {
            numerator: numerator.into(),
            denominator: denominator.into(),
        })
    }
}

#[derive(Debug, DebugPls, Clone, Copy, PartialEq)]
pub enum MaybeReal {
    MinusInf,
    PlusInf,
    Nan,
    Real(f64),
}

impl Default for MaybeRational {
    fn default() -> Self {
        Self::Rational(Rational::default())
    }
}

impl Default for MaybeReal {
    fn default() -> Self {
        Self::Real(f64::default())
    }
}

impl From<Rational> for MaybeRational {
    fn from(val: Rational) -> Self {
        Self::Rational(val)
    }
}

impl<T> From<T> for MaybeReal
where
    T: Into<f64>,
{
    fn from(val: T) -> Self {
        Self::Real(val.into())
    }
}

impl std::fmt::Display for MaybeRational {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        match self {
            Self::MinusInf => {
                write!(f, "-inf.0")
            }
            Self::PlusInf => {
                write!(f, "+inf.0")
            }
            Self::Nan => {
                write!(f, "+nan.0")
            }
            Self::Rational(val) => {
                write!(f, "{}", val)
            }
        }
    }
}

impl std::fmt::Display for MaybeReal {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        match self {
            Self::MinusInf => {
                write!(f, "-inf.0")
            }
            Self::PlusInf => {
                write!(f, "+inf.0")
            }
            Self::Nan => {
                write!(f, "+nan.0")
            }
            Self::Real(val) => {
                write!(f, "{val}")
            }
        }
    }
}

impl MaybeReal {
    // Helper to correctly display numbers
    pub(crate) fn has_sign_in_repr(&self) -> bool {
        match self {
            Self::Real(val) => *val < 0.0,
            _ => true,
        }
    }

    pub fn inf(positive: bool) -> Self {
        if positive {
            Self::PlusInf
        } else {
            Self::MinusInf
        }
    }

    pub fn nan() -> Self {
        Self::Nan
    }

    pub fn zero(&self) -> bool {
        matches!(self, Self::Real(val) if val.abs() < f64::EPSILON)
    }
}

impl From<MaybeRational> for MaybeReal {
    fn from(rational: MaybeRational) -> Self {
        match rational {
            MaybeRational::MinusInf => Self::MinusInf,
            MaybeRational::PlusInf => Self::PlusInf,
            MaybeRational::Nan => Self::Nan,
            MaybeRational::Rational(rat) => {
                Self::Real(rat.numerator() as f64 / rat.denominator() as f64)
            }
        }
    }
}

impl MaybeRational {
    // Helper to correctly display numbers
    pub(crate) fn has_sign_in_repr(&self) -> bool {
        match self {
            Self::Rational(val) => *val < ZERO_EXACT,
            _ => true,
        }
    }

    pub fn inf(positive: bool) -> Self {
        if positive {
            Self::PlusInf
        } else {
            Self::MinusInf
        }
    }

    pub fn nan() -> Self {
        Self::Nan
    }

    pub fn zero(&self) -> bool {
        matches!(self, Self::Rational(val) if val.numerator() == 0 && val.denominator() != 0)
    }
}

impl Display for Number {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        match self {
            Self::Exact(num) => match &num.imag {
                MaybeRational::Rational(val) if *val == ZERO_EXACT => {
                    write!(f, "{}", num.real)
                }
                _ => {
                    if num.imag.has_sign_in_repr() {
                        write!(f, "{}{}i", num.real, num.imag)
                    } else {
                        write!(f, "{}+{}i", num.real, num.imag)
                    }
                }
            },
            Self::Inexact(num) => match &num.imag {
                MaybeReal::Real(val) if *val == 0.0 => {
                    write!(f, "{}", num.real)
                }
                _ => {
                    if num.imag.has_sign_in_repr() {
                        write!(f, "{}{}i", num.real, num.imag)
                    } else {
                        write!(f, "{}+{}i", num.real, num.imag)
                    }
                }
            },
        }
    }
}
