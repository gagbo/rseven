// SPDX-FileCopyrightText: 2022-2023 Gerry Agbobada <git@gagbo.net>
//
// SPDX-License-Identifier: LGPL-3.0-only

use dbg_pls::DebugPls;

use super::{Body, Expression, Formals, Identifier, ImportSet, Literal, TransformerSpec};

#[derive(Clone, Debug, DebugPls, PartialEq)]
pub struct Program {
    pub imports: Vec<ImportSet>,
    pub com_defs: Vec<CommandOrDef>,
}

#[derive(Clone, Debug, DebugPls, PartialEq)]
pub enum CommandOrDef {
    Command(Expression),
    Definition(Definition),
    BeginBlock(Vec<CommandOrDef>),
}

#[derive(Clone, Debug, DebugPls, PartialEq)]
pub enum Definition {
    Single(SingleDefinition),
    Lambda(LambdaDefinition),
    Syntax(SyntaxDefinition),
    Values(ValuesDefinition),
    Record(RecordDefinition),
    Block(Vec<Definition>),
}

#[derive(Clone, Debug, DebugPls, PartialEq)]
pub struct SingleDefinition {
    pub identifier: Identifier,
    pub value: Expression,
}

#[derive(Clone, Debug, DebugPls, PartialEq)]
pub struct LambdaDefinition {
    pub identifier: Identifier,
    pub formals: DefFormals,
    pub body: Body,
}

#[derive(Clone, Debug, DebugPls, PartialEq)]
pub struct SyntaxDefinition {
    pub keyword: Identifier,
    pub spec: TransformerSpec,
}

#[derive(Clone, Debug, DebugPls, PartialEq)]
pub struct ValuesDefinition {
    pub formals: Formals,
    pub body: Body,
}

#[derive(Clone, Debug, DebugPls, PartialEq)]
pub struct RecordDefinition {
    pub name: Identifier,
    pub constructor: (Identifier, Vec<Identifier>),
    pub pred: Identifier,
    pub fields: Vec<FieldSpec>,
}

#[derive(Clone, Debug, DebugPls, PartialEq)]
pub struct FieldSpec {
    pub name: Identifier,
    pub accessor: Identifier,
    pub mutator: Option<Identifier>,
}

#[derive(Clone, Debug, DebugPls, PartialEq)]
pub struct DefFormals {
    pub idents: Vec<Identifier>,
    /// Gets a value when the formals are defined with a `.` before the last identifier
    pub last_dotted_ident: Option<Identifier>,
}
