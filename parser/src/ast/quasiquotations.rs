// SPDX-FileCopyrightText: 2022-2023 Gerry Agbobada <git@gagbo.net>
//
// SPDX-License-Identifier: LGPL-3.0-only

use dbg_pls::DebugPls;

use super::{Expression, Literal};

#[derive(Clone, Debug, DebugPls, PartialEq)]
pub struct Quasiquotation {
    pub inner: QqTemplate,
}

#[derive(Clone, Debug, DebugPls, PartialEq)]
pub enum QqTemplate {
    /// Base case of the nesting: containing only an expression
    Zero(Box<Expression>),
    Datum(Literal),
    List(ListQqTemplate),
    Vector(Vec<QqTemplateOrSplice>),
    Unquote(
        /* This QqTemplate is one nesting layer off */ Box<QqTemplate>,
    ),
}

#[derive(Clone, Debug, DebugPls, PartialEq)]
pub enum QqTemplateOrSplice {
    Template(QqTemplate),
    SpliceUnquote(QqTemplate),
}

#[derive(Clone, Debug, DebugPls, PartialEq)]
pub enum ListQqTemplate {
    List(Vec<QqTemplateOrSplice>),
    DottedList {
        // Technically guaranteed to have 1 element. Do we care?
        head: Vec<QqTemplateOrSplice>,
        tail: Box<QqTemplate>,
    },
    Quoted(
        /* This QqTemplate is at the same level */ Box<QqTemplate>,
    ),
    Deeper(
        /* This QqTemplate is one nesting layer added */ Box<QqTemplate>,
    ),
}

// TODO(#B): Test that the ambiguity in templates is properly handled:
//
// In <quasiquotation>s, a <list qq template D> can some-
// times be confused with either an <unquotation D> or
// a <splicing unquotation D>. The interpretation as an
// <unquotation> or <splicing unquotation D> takes prece-
// dence.
