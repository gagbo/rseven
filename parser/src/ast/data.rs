// SPDX-FileCopyrightText: 2023 Gerry Agbobada <git@gagbo.net>
//
// SPDX-License-Identifier: LGPL-3.0-only

use dbg_pls::DebugPls;

use super::Literal;

#[derive(Clone, Debug, DebugPls, PartialEq)]
pub enum CompoundDatum {
    List {
        head: Vec<Literal>,
        tail: Option<Box<Literal>>,
    },
    /// ' datum
    Quote(Box<Literal>),
    /// ` datum
    Backquote(Box<Literal>),
    /// , datum
    Comma(Box<Literal>),
    /// ,@ datum
    CommaAt(Box<Literal>),
}
