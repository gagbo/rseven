<!--
SPDX-FileCopyrightText: 2022-2023 Gerry Agbobada <git@gagbo.net>

SPDX-License-Identifier: CC0-1.0
-->

# R7-parser

This crate provides a r7rs parser, following the spec currently found at
https://small.r7rs.org/attachment/r7rs.pdf

## Testing the Parser

The test library uses [Pest ASCII
tree](https://crates.io/crates/pest_ascii_tree) to print a friendly version of
the AST. You can also either use pesta or the `repl` test (`cargo test repl`) to
quickly run the parser and see the output.
