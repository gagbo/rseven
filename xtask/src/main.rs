use clap::Parser;

#[derive(clap::Parser)]
#[command(author, version, about, long_about = None)]
#[command(propagate_version = true)]
struct Args {
    #[command(subcommand)]
    command: Command,
}

#[derive(clap::Subcommand)]
enum Command {
    /// Run Coverage
    Coverage {
        /// Export as HTML locally instead of lcov
        #[arg(long)]
        dev: bool,
    },
    /// Install Cargo tools
    Install,
    /// Show crate build time
    #[command(name = "bloat-time")]
    BloatTime,
    /// Run CI tasks
    #[command(name = "ci")]
    CI,
    /// Run Cargo Docs in watch mode
    Docs,
}

fn main() -> Result<(), anyhow::Error> {
    let cli = Args::parse();

    match &cli.command {
        Command::Coverage { dev } => xtaskops::tasks::coverage(*dev),
        Command::Install => xtaskops::tasks::install(),
        Command::BloatTime => xtaskops::tasks::bloat_time("rseven-core"),
        Command::CI => xtaskops::tasks::ci(),
        Command::Docs => xtaskops::tasks::docs(),
    }
}
